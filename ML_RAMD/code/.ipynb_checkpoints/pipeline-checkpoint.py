"""
Python file containing the classe(s) of the ML pipeline developed by 
Tom Kaufmann and Daria Kokh.
"""

__author__ = "Tom Kaufmann, Daria Kohk, Lorenzo Fabbri"

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from collections import OrderedDict, Counter
from itertools import groupby
import os
import pprint

from sklearn import preprocessing
from sklearn.preprocessing import KBinsDiscretizer
from sklearn.cluster import KMeans
from sklearn.mixture import GaussianMixture
from sklearn.linear_model import LinearRegression, Ridge
from sklearn.kernel_ridge import KernelRidge
from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split, ShuffleSplit, StratifiedShuffleSplit
from sklearn.model_selection import RandomizedSearchCV, GridSearchCV
from sklearn.feature_selection import f_regression
from sklearn.metrics import mean_squared_error, mean_absolute_error
from sklearn.metrics import r2_score, explained_variance_score
from sklearn.metrics import classification_report, accuracy_score, average_precision_score
from sklearn.metrics import f1_score, precision_score, recall_score, roc_auc_score

from yellowbrick.regressor import ResidualsPlot

class mlramd:
    
    def __init__(self, all_ligands, outliers):
        """
        Function necessary to initialize mlramd class.

        Parameters
        ----------
        all_ligands : list
            List of strings representing the ligands/inhibitors
        outliers : list
            List of strings representing the ligands/inhibitors considered to be potential outliers

        Returns
        -------
        None
        """
        
        # All available ligands, outliers, ligands to keep and corrected labels (+16)
        self.all_ligands = all_ligands
        self.outliers    = outliers
        self.ligands     = [ligand for ligand in all_ligands if ligand not in outliers]
        self.labels_corrected = None

        # Input data, combined data, averaged data (frames), 
        # processed data for learning, unseen data for external test, 
        # metadata, names of the ligands and total number of unique ligands
        self.data           = None
        self.data_combined  = None # Grouped by fingerprints
        self.data_averaged  = None # Averaged frames
        self.data_processed = None
        self.data_unseen    = None
        self.metadata       = None
        self.ligand_names   = None # Ligands present in input data
        self.number_ligands = None

        # Number of frames/snapshots, number of unique trajectories, 
        # length of each trajectory
        self.frames              = None
        self.trajectories        = None
        self.length_trajectories = None

        # Clustering results
        self.kmeans_model = None
        self.gmm_model    = None

        # Scores for regression and classification (names)
        self.regr_scores = ['r2_train', 'mse_train', 'mae_train', 'evs_train', 
        'r2_val', 'mse_val', 'mae_val', 'evs_val']
        self.clas_scores = ['acc_train', 'f1_train', 'prec_train', 'rec_train', 
        'acc_val', 'f1_val', 'prec_val', 'rec_val']

        # Regression results
        self.regr_models        = [] # List containing all trained models (sort of ensemble method)
        self.regression_results = None
        self.hyper_best         = None

        # Classification results and parameters
        self.clas_models            = []
        self.classification_results = None
        self.avg                    = None # Type of average for computing metrics
    
    def load_data(self, path):
        """
        Function to load input dataset.

        The input dataset must have the following structure: rows must correspond to ligands/inhibitors, 
        and columns must correspond to molecular fingerprints. There is not limitation in the 
        number of trajectories for each ligand and the number of frames/snapshots for each 
        trajectory.
        The input dataset is stored as `data`.

        Parameters
        ----------
        path : string
            Absolute or relative path to the input dataset file. Currently supports only pickled files

        Returns
        -------
        None
        """
        
        # Check whether file exists in given path
        if os.path.isfile(path):
            data = pd.read_pickle(path)
            print(f'Shape of input data: {data.shape}.')
            self.data = data

            # Count number of unique ligands in input data
            self.ligand_names = set()
            for element in self.data.index:
                name = element[0].split(':')[0]
                self.ligand_names.add(name)
            print(f'Number of unique ligands: {len(self.ligand_names)}.')
            self.number_ligands = len(self.ligand_names)

            # Count number of unique trajectories
            tmp = set()
            for element in self.data.index:
                #name = ':'.join(element[0].split(':')[1:])
                name = element[0]
                tmp.add(name)
            self.trajectories = len(tmp)
            print(f'Number of trajectories: {self.trajectories}.')

            # Count number of total frames
            self.frames = self.data.shape[0]
            print(f'Number of frames: {self.frames}.')
        else:
            print('File not found.')

    def load_metadata(self, path, ligands_column, 
        sheet=0, ligand_names=None):
        """
        Function to load input metadata.

        The input metadata must have the following structure: rows must correspond to ligands/inhibitors, 
        and columns must correspond to molecular features. There is no limitation in the number of 
        ligands/inhibitors and molecular features.
        The input metadata is stored as `metadata`.

        Parameters
        ----------
        path : string
            Absolute or relative path to the input metadata file. Currently supports only Excel files
        ligands_column : int
            Integer corresponding to the column containing the ligands names
        sheet : int
            Integer corresponding to the sheet number in the input file
        ligand_names : set
            Set containing the unique ligands identifiers

        Returns
        -------
        None
        """

        # Check whether file exists
        if os.path.isfile(path):
            self.metadata = pd.read_excel(path, sheet)

            # Convert column containing ligands names to string
            self.metadata.iloc[:, ligands_column] = self.metadata.iloc[:, ligands_column].astype('str')

            # Check whether to sub-select ligands
            if (ligand_names is not None):
                self.metadata = self.metadata.loc[self.metadata.iloc[:, ligands_column].isin(ligand_names)]

            print(f'Shape of metadata: {self.metadata.shape}.')

            # Check whether all the ligands in input data have metadata entry
            ligands_metadata = list(self.metadata.iloc[:, ligands_column])
            for element in self.ligand_names:
                if (element not in ligands_metadata):
                    print(f'Ligand {element} is not present in metadata.')

    def plot_metadata(self, to_separate_by, features=None):
        """
        Function to plot metadata in a consistent way.

        It is possible to plot several features and separate them by several other features.

        Parameters
        ----------
        to_separate_by : list
            List of strings representing the molecular features used to separate by
        features : list
            List of strings representing the molecular features to plot

        Returns
        -------
        None
        """

        if (features is not None):
            # Iterate over features to plot
            for idx, feature in enumerate(features):

                # Iterate over features to plot by
                for separation in to_separate_by:
                    fig, ax = plt.subplots(idx)

                    # Iterate over levels of feature to separate by
                    for level in list(self.metadata[separation].unique()):
                        sns.distplot(self.metadata[self.metadata[separation]==level][feature], 
                            label=level, hist=True, kde=True, norm_hist=True)

                    plt.title(f'Separation of {feature} by {separation}.')
                    plt.legend()
                    plt.show()

    def combine_fingerprints(self):
        """
        """

        # Re-name all columns in order to just have name residues
        new_columns = []
        for column in self.data.columns:
            try:
                new_column = column.split('_')[1].split('X')[1]
                new_columns.append(new_column)
            except IndexError:
                continue
        # Make copy input data
        self.data_combined = self.data.copy()
        self.data_combined.columns = new_columns

        # Combine same fingerprints
        self.data_combined = self.data_combined.groupby(self.data_combined.columns, 
            axis=1).sum()

        print(f'Shape of grouped data: {self.data_combined.shape}.')

    def average_frames(self, which_data='data'):
        """
        Function that takes the average of the frames corresponding to each input trajectory, 
        with the respect to each molecular fingerprint.

        Since each rows in the input dataframe corresponds to a trajectory of a ligand/inhibitor, 
        this function performs the average, with respect to the molecular fingerprints, 
        of the frames/snapshots of each of these entries. The resulting dataframe should have 
        as many rows as the number of unique trajectories.
        The processed data is stored as `data_averaged`.

        Parameters
        ----------
        which_data : string
            String corresponding to the name of the dataframe to be used

        Returns
        -------
        None
        """

        # Create temporary dictionary containing averaged data
        tmp_dict = dict()

        # Create dictionary containing length of trajectories
        self.length_trajectories = dict()

        # Create set of complete ligand names
        tmp_names = set()
        for element in self.data.index:
            tmp_names.add(element[0])

        # Either use full data or grouped data
        if (which_data=='data'):
            for element in list(tmp_names):
                tmp_dict[element] = self.data.loc[element].mean(axis=0)

                # Store length trajectory
                self.length_trajectories[element] = self.data.loc[element].shape[0]
        elif (which_data=='grouped'):
            for element in list(tmp_names):
                tmp_dict[element] = self.data_combined.loc[element].mean(axis=0)

                # Store length trajectory
                self.length_trajectories[element] = self.data.loc[element].shape[0]
        else:
            print('Either complete or grouped data.')
        
        # Create new dataframe
        self.data_averaged = pd.DataFrame.from_dict(tmp_dict, 
            orient='index')

        print(f'Shape of averaged data (by frame): {self.data_averaged.shape}.')
        print(f'Total number of frames: {np.sum(list(self.length_trajectories.values()))}.')

    def average_trajectories(self):
        """
        Function that takes the average of the trajectories corresponding to each input 
        ligand/inhibitor, with the respect to each molecular fingerprint.

        Since each ligand/inhibitor can have multiple trajectories associated to it, this function 
        performs the average, with respect to the molecular fingerprints, of the of these trajectories 
        for each ligand/inhibitor.
        The processed data is stored as `data_averaged`.

        Parameters
        ----------
        None
            It is expected that the average with respect to the trajectories is performed 
            on the data averaged with respect to the frames

        Returns
        -------
        None
        """

        # Create temporary list with ligand names
        tmp_ligand_names = []

        # Add temporary column to averaged data containing ligand name only
        for idx, element in enumerate(self.data_averaged.index):
            self.data_averaged.loc[element, 'ID'] = element.split(':')[0]
            
            tmp_ligand_names.append(element.split(':')[0])

        # Group by same ligand and take mean of IFs
        self.data_averaged = self.data_averaged.groupby(['ID'], as_index=False).mean()

        # Drop column averaged data containing ligand name
        self.data_averaged.drop('ID', axis=1, inplace=True)

        # Insert new index containing ligand names
        tmp_ligand_names = list(OrderedDict.fromkeys(tmp_ligand_names))
        self.data_averaged.index = tmp_ligand_names

        print(f'Shape of averaged data (by trajectory): {self.data_averaged.shape}.')

    def threshold_IF(self, data, threshold1):
        """
        Function that applies threshold on input dataset.

        This function drops each column of the input dataset that does NOT contain at least 
        one entry being greater than the given threshold.
        This operation is performed in place.

        Parameters
        ----------
        data : dataframe
            Pandas dataframe containing the input data
        threshold1 : float

        Returns
        -------
        None
        """

        #to_drop = self.data_averaged.loc[:, list(self.data_averaged.mean() < threshold)]
        #self.data_averaged.drop(to_drop, axis=1, inplace=True)

        # Check for each column whether there is at least one element greater than threshold1
        to_keep = data.loc[:, (data > threshold1).any(axis=0)].columns
        to_drop = data.columns.difference(to_keep)
        data.drop(to_drop, axis=1, inplace=True)

        print(f'Shape of data after application of threshold1: {data.shape}.')

    def remove_correlated(self, data, threshold):
        """
        Function that drops correlated columns in the input dataset, given a 
        chosen threshold.
        This operation is performed in place.

        Parameters
        ----------
        data : dataframe
            Pandas dataframe containing the input data
        threshold : float

        Returns
        -------
        None
        """

        correlation_matrix = data.corr().abs()
        upper = correlation_matrix.where(np.triu(np.ones(correlation_matrix.shape), 
            k=1).astype(np.bool))

        to_drop = [column for column in upper.columns if any(upper[column] > threshold)]
        print(f'The following IFs ({len(to_drop)}) are going to be dropped: {to_drop}.')

        data.drop(data[to_drop], axis=1, inplace=True)

        print(f'Shape of averaged data without correlated IFs: {data.shape}.')

    def add_features(self, data, coln_metad_name, features='all'):
        """
        Function that adds the molecular features, all or a subset, present in the 
        metadata.
        This operation is performed in place.

        Parameters
        ----------
        data : dataframe
            Pandas dataframe containing the input data
        coln_metad_name : string
            String corresponding to the name of the column containing the names of the ligands
        features : list
            Default is `all`, meaning that all the molecular features are added. Pass a list
            of features to consider only them

        Returns
        -------
        dataframe
            Pandas dataframe with molecular features
        """

        # Set index of metadata as ligand name
        self.metadata.set_index(coln_metad_name, inplace=True)

        # Add temporary column to averaged data containing ligand name only.
        # Necessary only in case data is not averaged by trajectory.
        for idx, element in enumerate(data.index):
            data.loc[element, 'ID'] = element.split(':')[0]

        # Merge information from metadata and averaged data
        if (features=='all'):
            tmp_df = data.merge(self.metadata, left_on='ID', 
                right_on=self.metadata.index, right_index=True)
        else:
            # In this case a list of features is given as input

            # Loop over input features and check whether present in metadata
            for feature in features:
                if (feature not in self.metadata.columns):
                    print(f'{feature} is not present in metadata. It is going to be dropped.')
                    features.remove(feature)

            tmp_df = data.merge(self.metadata[features], 
                left_on='ID', right_on=self.metadata.index, right_index=True)

        print(f'Shape of averaged data with molecular features: {tmp_df.shape}.')

        # Drop column averaged data containing ligand name
        tmp_df.drop('ID', axis=1, inplace=True)

        return tmp_df

    def clustering(self, data, scale, method, params, columns):
        """
        Function that performs the clustering of the input data, based on the selected method. It is 
        possible to scale the data before running the clustering algorithm.

        Parameters
        ----------
        data : dataframe
            Pandas dataframe containing the input data
        scale : boolean
            If True, the input data is scaled using the selected method
        method : string
            Name of the method to be used to perform the clustering
        params : dictionary
            Dictionary containing as keys the hyper-parameters to be used for the clustering
        columns : int
            Number corresponding to the last column of the input data containing molecular 
            fingerprints. This is such that it is possible to pass dataframes containing also 
            molecular features

        Returns
        -------
        None
        """

        # Scale data
        if scale:
            data_scaled = data.values[:, :columns].copy()
            data_scaled = preprocessing.MinMaxScaler(feature_range=(0, 1), 
                copy=False).fit_transform(data_scaled)
        else:
            data_scaled = data.values[:, :columns]

        # Unpack parameters
        n_clusters      = params['n_clusters']
        n_components    = params['n_components']
        n_init          = params['n_init']
        max_iter        = params['max_iter']
        covariance_type = params['covariance_type']

        if (method=='kmeans'):
            kmeans = KMeans(n_clusters=n_clusters, 
                n_init=n_init, max_iter=max_iter, 
                random_state=0, n_jobs=-1).fit(data_scaled)
            self.kmeans_model = kmeans

            # Make predictions and summary
            self.summary_clustering(self.kmeans_model.predict(data_scaled), 
                'koff', columns)

        elif (method=='gmm'):
            # Iterate over input number of components to test
            gmm_models = []
            for component in n_components:
                gmm_models.append(GaussianMixture(n_components=component, 
                    covariance_type=covariance_type, 
                    max_iter=max_iter, n_init=n_init, 
                    random_state=0).fit(data_scaled))

            # Compute AIC scores
            aic = []
            for model in gmm_models:
                aic.append(model.aic(data_scaled))
            aic_min, aic_idx = np.min(aic), np.argmin(aic)
            self.gmm_model = gmm_models[aic_idx]
            print(f'Lowest AIC score: {aic_min}.')
            print(f'Best model has {n_components[aic_idx]} components.')

            plt.plot(n_components, aic, 'o', label='AIC')
            plt.legend()
            plt.xlabel('n_components')
            plt.ylabel('score')
            plt.title('AIC for the different number of Gaussian components.')
            plt.show()

            # Make predictions and summary
            self.summary_clustering(self.gmm_model.predict(data_scaled), 
                'koff', columns)

        else:
            print(f'{method} is currently not supported.')

        data_scaled = None

    def summary_clustering(self, predictions, feature, columns):
        """
        Function that performs some analyses of the obtained clusters. This function also prints 
        the ligands/inhibitors belonging to each obtained cluster.

        Parameters
        ----------
        predictions : array
            Array of labels, the index of the cluster each sample belongs to
        feature : string
            String corresponding to the molecular feature to be used for the scatter plot
        columns : int
            Number corresponding to the last column of the input data containing molecular 
            fingerprints. This is such that it is possible to pass dataframes containing also 
            molecular features

        Returns
        -------
        None
        """

        # Create list containing corrected features names and store them for later use
        tmp_names = self.data_averaged.columns[: columns]
        labels = []
        for name in tmp_names:
            number = int(name.split('X')[1][:-3])+16
            labels.append(name.split('_')[0] + ' ' + str(number) + name.split('X')[1][-3:])
        self.labels_corrected = labels

        # Heatmap clusters-features
        plt.figure(figsize=(len(np.unique(predictions)), 15))
        sns.set(font_scale=1.3)
        sns.heatmap(self.data_averaged.iloc[:, :columns].groupby(list(predictions)).agg('mean').T, 
            yticklabels=labels, cmap='YlGnBu')
        plt.xlabel('clusters')
        plt.ylabel('features')
        s = self.data_averaged.iloc[:, :columns].groupby(list(predictions)).agg('mean').T.shape
        plt.title(f'Heatmap of features by cluster: {s}.')
        plt.show()

        # Heatmap clusters-molecular features
        print('TODO!')

        # Scatter plot with error bars for feature-clusters
        feature_means = []
        feature_stds  = []
        for idx in range(len(np.unique(predictions))):
            tmp_mean = self.data_averaged.loc[predictions==idx][feature].mean()
            tmp_std  = self.data_averaged.loc[predictions==idx][feature].std()
            feature_means.append(tmp_mean)
            feature_stds.append(tmp_std)

        plt.figure()
        plt.errorbar(list(np.unique(predictions)), feature_means, 
            yerr=feature_stds, fmt='o')
        plt.xlabel('cluster')
        plt.ylabel(f'<{feature}>')
        plt.title(f'{feature} mean value for the found clusters.')
        plt.show()

        # Print information feature for each cluster
        for idx in range(len(np.unique(predictions))):
            print('============================================================')
            print(f'Cluster {idx}: mean value of {feature} is {np.round(feature_means[idx], 3)}.')
            print('\n')
            tmp_ligands = list(self.data_averaged.loc[predictions==idx].index)
            print(f'The ligands belonging to cluster {idx} are: {tmp_ligands}.')

    def transform_absolute_scale(self, data, feature, feature_exp, error=None):
        """
        Function that fits a linear regression model and transform some data to another scale.

        In the linear model, the independent variable corresponds to an experimentally measured 
        feature, while the dependent variable corresponds to the result of some simulations.
        The returned values are thus transformed to an absolute scale.

        Parameters
        ----------
        data : dataframe
            Pandas dataframe containing the input data
        feature : string
            String corresponding to the "simulated values"
        feature_exp : string
            String corresponding to the experimentally measured values
        error : string
            String corresponding to the error on the "simulated values"

        Returns
        -------
        feature_scaled : array
            NumPy array containing the scaled "simulated values"
        error_scaled : array
            NumPy array containing the scaled error on the "simulated values"
        """

        # Fit linear regression model experimental-simulation
        regressor = LinearRegression()
        regressor.fit(np.array(data[feature_exp]).reshape(-1, 1), np.array(data[feature]))
        intercept = regressor.intercept_
        slope     = regressor.coef_

        # Scale
        feature_scaled = (np.array(data[feature]) - intercept) / slope
        if (error is not None):
            error_scaled = (np.array(data[error])) / slope
        else:
            error_scaled = None

        return feature_scaled, error_scaled

    def plot_absolute_scale(self, data, feature_x, feature_y, error_x, error_y, by):
        """
        Function that generates the following plots: a scatter plot of two selected features, with 
        one scaled to an absolute scale, histograms of the selected features.

        Parameters
        ----------
        data : dataframe
            Pandas dataframe containing the input data
        feature_x : string
            String corresponding to the name of the column containing the x values
        feature_y : string
            String corresponding to the name of the column containing the y values
        error_x : string
            String corresponding to the name of the column containing the error on the x values
        error_y : string
            String corresponding to the name of the column containing the error on the y values
        by : list
            List of strings corresponding to the names of the columns to be used for grouping by

        Returns
        -------
        None
        """

        plt.figure(figsize=(8, 8))

        # Just plot line feature_x-feature_x
        plt.plot(data[feature_x], data[feature_x], color='black', linewidth=1)
        plt.xlabel(f'{feature_x}')
        plt.ylabel(f'{feature_y}')

        # Transform simulation results to absolute scale
        feature_scaled, error_scaled = self.transform_absolute_scale(data, feature_y, 
            feature_x, error_y)

        # Plot transformed data and eventually split by group(s)
        grouped = data.groupby(by)
        for key, group in grouped:
            idx = data.index.get_indexer(group.index)
            plt.errorbar(group[feature_x], feature_scaled[idx], 
                xerr=group[error_x], yerr=error_scaled[idx], 
                fmt='o', label=key)

        plt.title(f'Experimental and simulated values of {feature_x} grouped by {by}.')
        plt.legend(loc='center left', bbox_to_anchor=(1, 0.5), prop={'size': 10})
        plt.show()

        # Plot histograms of features
        plt.figure()
        sns.distplot(data[feature_x], hist=True, kde=True)
        plt.xlabel(f'{feature_x}')
        plt.title(f'Histogram of {feature_x}.')
        plt.show()

        plt.figure()
        sns.distplot(feature_scaled, hist=True, kde=True)
        plt.xlabel(f'{feature_y}')
        plt.title(f'Histogram of {feature_y}.')
        plt.show()

    def plot_mae(self, data, features):
        """
        Function that plots the histogram of the Mean Absolute Error between two selected features.
        The first feature in the input list is converted to an absolute scale, using the second 
        as independent variable.

        Parameters
        ----------
        data : dataframe
            Pandas dataframe containing the input data
        features : list
            List of strings corresponding to the names of the columns to be used as features

        Returns
        -------
        None
        """

        # Transform simulation results to absolute scale
        feature_scaled, _ = self.transform_absolute_scale(data, features[0], 
            features[1])

        plt.figure()
        sns.distplot(np.abs(feature_scaled - data[features[1]]), hist=True, kde=True)
        plt.gca().set_xlim(left=0.0)
        plt.xlabel('MAE')
        plt.show()

    def process_data(self, data, columns, method):
        """
        Function that performs some pre-processing on the input data for learning purposes.
        The processed data is stored as `data_processed`.

        Parameters
        ----------
        data : dataframe
            Pandas dataframe containing the input data
        columns : string or int
            If 'all', the complete dataframe is used. If an integer, this corresponds to the 
            number of columns to keep for pre-processing
        method : string
            Pre-processing method to be used for pre-processing

        Returns
        -------
        None
        """

        # Apply selected pre-processing method. Currently makes copy of data, so 
        # not very memory-efficient
        if (columns=='all'):
            data_processed = data.values.copy()
        else:
            data_processed = data.iloc[:, :columns].values.copy()

        if (method=='minmax'):
            scaler = preprocessing.MinMaxScaler()
            data_processed = scaler.fit_transform(data_processed)
        else:
            print(f'Method {method} is currently not supported.')

        print(f'Shape of processed data: {data_processed.shape}.')
        self.data_processed = data_processed

    def cross_validation(self, data, feature, folds, test_size=0.25, plot=False):
        """
        Function that performs cross-validation on the input data and plots the train/test split 
        based on input feature.

        Parameters
        ----------
        data : array
            Numpy array corresponding to the values of the molecular fingerprints
        feature : string
            String corresponding to the name of the column containing the selected feature
        folds : int
            Integer corresponding to the number of cross-validation folds
        test_size : float
            Float corresponding to the size of the test set
        plot : boolean
            If True, a plot of feature vs. ligands is generated

        Returns
        -------
        None
        """

        ss = ShuffleSplit(n_splits=folds, random_state=0, test_size=test_size)

        # Plot split data
        if plot:
            fig, axes = plt.subplots(int(folds/4), 4, figsize=(25, 17))
            i = 0
            j = 0
            for train_idx, test_idx in ss.split(data):
                #X_train, X_test = data[train_idx], data[test_idx]
                #y_train, y_test = self.data_averaged.sort_values(by=feature).iloc[train_idx][feature], \
                                  #self.data_averaged.sort_values(by=feature).iloc[test_idx][feature]
                y_train, y_test = self.data_averaged.iloc[train_idx][feature], \
                                  self.data_averaged.iloc[test_idx][feature]

                axes[i, j].plot(train_idx, y_train.values, 'o', markersize=10, label='train')
                axes[i, j].plot(test_idx, y_test.values, 'o', markersize=10, label='test')

                j+=1
                if (j>3):
                    axes[i, 0].set_ylabel(f'{feature}')
                    axes[i, 3].legend(loc='center left', bbox_to_anchor=(1, 0.5))
                    i+=1
                    j=0
            for j in range(4):
                axes[int(folds/4)-1, j].set_xlabel('ligands')
            plt.show()

        return ss

    def scores(self, method, model, data_train, y_train, data_test, y_test, print_res):
        """
        Function that computes and prints relevant scores for the problem being solved: either 
        regression or classification.

        Parameters
        ----------
        method : string
            Either 'regression' or 'classification'
        model : estimator
            Fitted model
        data_train : array
            NumPy array containing the input data for training. It must contain numerical values only
        y_train : array or series
            NumPy array or Pandas series containing the response variable for training. It must contain 
            numerical values only
        data_test : array
            NumPy array containing the input data for testing. It must contain numerical values only
        y_test : array or series
            NumPy array or Pandas series containing the response variable for testing. It must contain 
            numerical values only
        print_res : boolean
            If True, some information and the computed metrics are printed in-line

        Returns
        -------
        list
            List containing the values of the computed metrics for both training and 
            test
        """

        y_train_pred   = model.predict(data_train)
        y_true, y_pred = y_test, model.predict(data_test)

        # Compute scores for regression
        if (method=='regression'):
            r2_train  = r2_score(y_train, y_train_pred)
            mse_train = mean_squared_error(y_train, y_train_pred)
            mae_train = mean_absolute_error(y_train, y_train_pred)
            evs_train = explained_variance_score(y_train, y_train_pred)


            r2_test   = r2_score(y_true, y_pred)
            mse_test  = mean_squared_error(y_true, y_pred)
            mae_test  = mean_absolute_error(y_true, y_pred)
            evs_test  = explained_variance_score(y_true, y_pred)

            return [r2_train, mse_train, mae_train, evs_train, r2_test, mse_test, mae_test, evs_test]

        # Compute scores for classification
        if (method=='classification'):
            if print_res:
                print('Classification report for training results:')
                print(classification_report(y_train, y_train_pred))
                print('\n')

                print('Classification report for validation results:')
                print(classification_report(y_true, y_pred))

            # Scores training
            acc_train = accuracy_score(y_train, y_train_pred)

            #proba_train = model.predict_proba(data_train)

            f1_train   = f1_score(y_train, y_train_pred, average=self.avg)
            prec_train = precision_score(y_train, y_train_pred, average=self.avg)
            rec_train  = recall_score(y_train, y_train_pred, average=self.avg)

            # Scores validation
            acc_val = accuracy_score(y_true, y_pred)

            #proba_val = model.predict_proba(data_test)

            f1_val   = f1_score(y_true, y_pred, average=self.avg)
            prec_val = precision_score(y_true, y_pred, average=self.avg)
            rec_val  = recall_score(y_true, y_pred, average=self.avg)

            return [acc_train, f1_train, prec_train, rec_train, 
            acc_val, f1_val, prec_val, rec_val]

        else:
            print(f'{method} is currently not supported.')

    def regression(self, data_train, data_test, y_train, y_test, method, params, print_res):
        """
        Function that performs regression using the selected method and tuned hyper-parameters.
        Currently, a grid-search is performed to optimze the hyper-parameters: if these are too 
        many, a random-search is suggested.

        Parameters
        ----------
        data_train : array
            NumPy array containing the input data for training. It must contain numerical values only
        data_test : array
            NumPy array containing the input data for testing. It must contain numerical values only
        y_train : array or series
            NumPy array or Pandas series containing the response variable for training. It must contain 
            numerical values only
        y_test : array or series
            NumPy array or Pandas series containing the response variable for testing. It must contain 
            numerical values only
        method : string
            String corresponding to the method to be used for regression
        params : dictionary
            Dictionary containing as keys the parameters and hyper-parameters to be optimzed
        print_res : boolean
            If True, some information and the computed metrics are printed in-line

        Returns
        -------
        model
            Fitted regressor
        list
            List containing the values of the computed metrics for both training and 
            test
        """

        # Unpack parameters
        cv = params['cv']

        # Select method and unpack hyper-parameters
        if (method=='ridge'):
            alpha_ridge = params['alpha_ridge']
        elif (method=='rfr'):
            n_iter       = params['n_iter']
            n_estimators = params['n_estimators']
            max_depth    = params['max_depth']
            max_features = params['max_features']
        else:
            print(f'{method} is currently not supported.')

        # Ridge regression
        if (method=='ridge'):
            ridge = Ridge()

            hyperparameters = {'alpha': alpha_ridge}
            rscv = GridSearchCV(ridge, hyperparameters, cv=cv, n_jobs=-1)
            ridge_best = rscv.fit(data_train, y_train)
            self.regr_models.append(ridge_best)
            self.hyper_best = rscv.best_params_

            if print_res:
                print(f'\tBest parameters for Ridge regression:')
                print(f'\t\t{rscv.best_params_}')

            scores = self.scores('regression', ridge_best, data_train, y_train, 
                data_test, y_test, print_res)

            # Plot residuals
            #visualizer = ResidualsPlot(ridge_best)
            #visualizer.fit(data_train, y_train)
            #visualizer.score(data_test, y_test)
            #visualizer.poof()

            if print_res:
                print(f'... finished Ridge regression.')

        # Random Forest regression
        elif (method=='rfr'):
            regr = RandomForestRegressor()

            hyperparameters = {'n_estimators': n_estimators, 'max_depth': max_depth, 
                               'max_features': max_features}
            rscv = RandomizedSearchCV(regr, param_distributions=hyperparameters, n_iter=n_iter, 
                cv=cv, n_jobs=-1)
            regr_best = rscv.fit(data_train, y_train)
            self.regr_models.append(regr_best)
            self.hyper_best = rscv.best_params_

            if print_res:
                print(f'\tBest parameters for Random Forest regression:')
                print(f'\t\t{regr_best.best_params_}')

            scores = self.scores('regression', regr_best, data_train, y_train, 
                data_test, y_test, print_res)

            if print_res:
                print(f'... finished Random Forest regression.')

        else:
            print(f'{method} is currently not supported.')

        return self.regr_models[-1], scores

    def train_test_split_balanced(self, X, y, feature, test_size):
        """
        Function that performs a stratified split using an input feature to check 
        class imbalance. Probably useful only for regression tasks.

        Parameters
        ----------
        X : array
            NumPy array containing the input data. It must contain numerical values only
        y : array or series
            NumPy array or Pandas series containing the response variable. It must contain 
            numerical values only
        feature : series
            Pandas Series containing the feature to be used for class imbalance
        test_size : float
            Float corresponding to the size of the test set

        Returns
        -------
        list
            List containing training set, test set, training indeces and test indeces
        """

        # Create dictionary of unique elements for given feature and generate bar plot
        types = Counter(feature)
        #plt.figure()
        #plt.bar(range(len(types)), list(types.values()))
        #plt.xticks(range(len(types)), list(types.keys()))
        #plt.xlabel(f'{feature.name}')
        #plt.show()

        # Check whether minimum number of elements is at least 2
        if (np.min(list(types.values())) < 2):
            print('The number of elements for at least one type is less than 2.')

        # Use stratified split on the feature and then create train/test splits for y
        sss = StratifiedShuffleSplit(n_splits=1, test_size=test_size)

        for train_idx, test_idx in sss.split(X, feature):
            X_train, X_test = X[train_idx], X[test_idx]
            y_train, y_test = y[train_idx], y[test_idx]

        return [X_train, X_test, y_train, y_test, train_idx, test_idx]


    def perform_regression(self, data, response, feature, method, params, iterations, 
        test_size, print_res=False):
        """
        Function that sets up the data for performing regression.

        Parameters
        ----------
        data : array
            NumPy array containing the input data. It must contain numerical values only
        response : array or series
            NumPy array or Pandas series containing the response variable. It must contain 
            numerical values only
        feature : series
            Pandas Series containing the feature to be used for class imbalance
        method : string
            String corresponding to the method to be used for regression
        params : dictionary
            Dictionary containing as keys the parameters and hyper-parameters to be optimzed
        iterations : int
            Number of iteration for performing regression. For each iteration, the results are 
            stored
        test_size : float
            Float corresponding to the size of the test set
        print_res : boolean
            If True, some information and the computed metrics are printed in-line

        Returns
        -------
        None
        """

        # Store best values of hyper-parameters based on R2
        r2_test_tmp = -100
        hyper_tmp   = None

        # Store partial scores from `f_regression` procedure
        f_regression_scores = np.empty([1, data.shape[1]])

        # Create dictionary to store results
        self.regression_results = dict()
        for i in range(iterations):
            self.regression_results[str(i)] = dict()

        for i in range(iterations):

            # Split data into train and test
            X_train, X_test, y_train, y_test, _, _ = self.train_test_split_balanced(data, response, 
                feature, test_size=test_size)

            if (i==0):
                print(f'Number of training data points: {len(y_train)}.')
                print(f'Number of validation data points: {len(y_test)}.')

            # Run regression model and save results
            model, scores = self.regression(X_train, X_test, y_train, y_test, 
                method, params, print_res)
            for idx, score in enumerate(scores):
                self.regression_results[str(i)][self.regr_scores[idx]] = np.round(score, 3)

            # Selection based on R2 currently
            if (scores[4] > r2_test_tmp):
                r2_test_tmp = scores[4]
                hyper_tmp   = self.hyper_best

            # Store partial weights from `f_regression` procedure
            weights, pvalues = f_regression(X_train, y_train)
            f_regression_scores += weights

        print('Best hyper-parameters found:')
        print(hyper_tmp)

        # Bar-plot of features importance from `f_regression` procedure
        f_regression_scores = [float(i)/np.max(f_regression_scores[0]) for i in f_regression_scores[0]]
        fig, ax = plt.subplots(figsize=(20, 5))
        plt.bar(list(np.arange(data.shape[1])), f_regression_scores, color="coral")
        ax.set_xticks(np.arange(data.shape[1]))
        #ax.set_xticklabels(self.data_averaged.iloc[:, :data.shape[1]].columns, rotation = 80)
        ax.set_xticklabels(self.labels_corrected, rotation = 80)
        plt.xlabel('residues')
        plt.title(f'Feature importance using f_regression with {method}.')
        plt.show()
    
    def classification(self, data_train, data_val, y_train, y_val, method, params, print_res):
        """
        Function that performs classification using the selected method and tuned hyper-parameters.

        Parameters
        ----------
        data_train : array
            NumPy array containing the input data for training. It must contain numerical values only
        data_val : array
            NumPy array containing the input data for validation. It must contain numerical values only
        y_train : array
            NumPy array containing the binned response variable for training
        y_val : array
            NumPy array containing the binned response variable for validation
        method : string
            String corresponding to the method to be used for classification
        params : dictionary
            Dictionary containing as keys the parameters and hyper-parameters to be optimzed
        print_res : boolean
            If True, some information and the computed metrics are printed in-line

        Returns
        -------
        model
            Fitted regressor
        list
            List containing the values of the computed metrics for both training and 
            test
        """

        # Select method and unpack specific hyper-parameters
        if (method=='logistic'):
            a = 0
        else:
            print(f'{method} is currently not supported.')

        # Multinomial logistic regression
        if (method=='logistic'):
            clf = LogisticRegression(solver='lbfgs', multi_class='multinomial', 
                class_weight='balanced', n_jobs=-1)

            clf_fit = clf.fit(data_train, y_train)
            self.clas_models.append(clf_fit)

            scores = self.scores('classification', clf_fit, data_train, y_train, 
                data_val, y_val, print_res)

        else:
            print(f'{method} is currently not supported.')

        return self.clas_models[-1], scores

    def perform_classification(self, data, response, response_disc, method, params, iterations, 
        test_size, print_res=False):
        """
        Function that sets up the data for performing classification.

        Parameters
        ----------
        data : array
            NumPy array containing the input data. It must contain numerical values only
        response : array or series
            NumPy array or Pandas series containing the response variable. It must contain 
            numerical values only
        response_disc : array
            NumPy array containing binned response variable
        method : string
            String corresponding to the method to be used for classification
        params : dictionary
            Dictionary containing as keys the parameters and hyper-parameters to be optimzed
        iterations : int
            Number of iteration for performing classification. For each iteration, the results are 
            stored
        test_size : float
            Float corresponding to the size of the test set
        print_res : boolean
            If True, some information and the computed metrics are printed in-line

        Returns
        -------
        None
        """

        # Create dictionary to store results
        self.classification_results = dict()
        for i in range(iterations):
            self.classification_results[str(i)] = dict()

        # Perform classification for a number of iterations (at least once)
        for i in range(iterations):

            # Plot response variable and color by belonging bin
            if (i==0):
                cmap = plt.cm.jet
                cmap_list = [cmap(i) for i in range(cmap.N)]
                cmap = cmap.from_list('Custom map', cmap_list, cmap.N)
                plt.figure()
                sct = plt.scatter(range(len(response)), response, 
                    c=response_disc.reshape(-1), cmap=cmap)
                plt.colorbar(sct)
                plt.xlabel('ligands')
                plt.ylabel('response')
                plt.title('Scatter plot response variable colored by bin.')
                plt.show()

            # Split data into train and validation
            X_train, X_val, y_train, y_val = train_test_split(data, response_disc, 
                test_size=test_size, stratify=response_disc)

            if (i==0):
                print(f'Number of training data points: {len(y_train)}.')
                print(f'Number of validation data points: {len(y_val)}.')

            # Learn classifier and save results
            model, scores = self.classification(X_train, X_val, y_train, y_val, 
                method, params, print_res)
            for idx, score in enumerate(scores):
                self.classification_results[str(i)][self.clas_scores[idx]] = np.round(score, 3)

        # Feature importance (one possible method)
        coeff_all = np.empty_like(self.clas_models[0].coef_)
        for model in self.clas_models:
            coeff_all += model.coef_.sum()

        plt.figure(figsize=(20, 10))
        plt.plot(np.std(data, 0) * coeff_all.sum(), 'o', markersize=12)
        plt.xticks(range(data.shape[1]), self.labels_corrected, rotation=60)
        plt.tick_params(axis='x', which='major', labelsize=15)
        plt.show()

    def learning_summary(self, dict_scores, bins):
        """
        Function that generates a number of plots to test goodness of learned models. Works for both 
        regression and classification tasks.

        Parameters
        ----------
        dict_scores : dictionary
            Dictionary containing computed evaluation scores
        bins : int
            Number of bins for the histograms

        Returns
        -------
        None
        """

        # Aggregate results of different iterations
        number_scores = len(dict_scores[str(0)]) # Number of scores computed
        score_names = [name for name in dict_scores[str(0)]] # Names of the scores computed

        # Initialize figure for subplots
        fig, axes = plt.subplots(int(number_scores/2), 2, figsize=(12, 8))
        rows = [row for i in range(2) for row in range(int(number_scores/2))]

        # Iterate over the scores computed
        for i in range(number_scores):
            tmp_scores = []
            tmp_name = score_names[i]

            # Group results of same score from different iterations
            for k, v in groupby(dict_scores.items(), key=lambda i:i[1][tmp_name]):
                # List containing numerical results for specific score computed
                tmp_scores.extend([item[1][tmp_name] for item in list(v)])

            # Plot histogram
            j = rows[i]
            k = 0 if i<int(number_scores/2) else 1
            sns.distplot(tmp_scores, hist=True, kde=True, bins=bins, ax=axes[j, k])
            axes[j, k].set_xlim(left=-1.0, right=2.0)

            # Add vertical line corresponding to mean of considered score
            tmp_mean = np.round(np.mean(tmp_scores), 2)
            axes[j, k].axvline(tmp_mean, 0, 1, c='green', label=f'mean {tmp_mean}')
            axes[j, k].set_xlabel(f'{tmp_name}')

            axes[j, k].legend(prop={'size': 8})
        
        plt.tight_layout()
        plt.show()

    def evaluate_unseen(self, method, models, data_test, y_test):
        """
        Function that computes relevant (test) scores for the problem being solved: either 
        regression or classification.

        Parameters
        ----------
        method : string
            Either 'regression' or 'classification'
        models : list
            Fitted models
        data_test : array
            NumPy array containing the input data for testing. It must contain numerical values only
        y_test : array or series
            NumPy array or Pandas series containing the response variable for testing. It must contain 
            numerical values only

        Returns
        -------
        None
        """

        scores_regr_test = ['r2_test', 'mse_test', 'mae_test', 'evs_test']
        scores_clas_test = ['acc_test', 'f1_test', 'prec_test', 'rec_test']

        # Dictionary of dictionaries to store scores for all input models
        scores_all = dict()
        if (method=='regression'):
            for i in range(len(models)):
                scores_all[str(i)] = dict()
                for key in scores_regr_test:
                    scores_all[str(i)][key] = 0.0

        elif (method=='classification'):
            for i in range(len(models)):
                scores_all[str(i)] = dict()
                for key in scores_clas_test:
                    scores_all[str(i)][key] = 0.0

        else:
            print(f'{method} is currently not supported.')

        # Iterate over all input models and compute scores
        for idx, model in enumerate(models):
            y_true, y_pred = y_test, model.predict(data_test)

            # Compute scores for regression
            if (method=='regression'):
                scores_all[str(idx)]['r2_test']  = r2_score(y_true, y_pred)
                scores_all[str(idx)]['mse_test'] = mean_squared_error(y_true, y_pred)
                scores_all[str(idx)]['mae_test'] = mean_absolute_error(y_true, y_pred)
                scores_all[str(idx)]['evs_test'] = explained_variance_score(y_true, y_pred)

            # Compute scores for classification
            elif (method=='classification'):
                scores_all[str(idx)]['acc_test']  = accuracy_score(y_true, y_pred)
                scores_all[str(idx)]['f1_test']   = f1_score(y_true, y_pred, average=self.avg)
                scores_all[str(idx)]['prec_test'] = precision_score(y_true, y_pred, average=self.avg)
                scores_all[str(idx)]['rec_test']  = recall_score(y_true, y_pred, average=self.avg)

            else:
                print(f'{method} is currently not supported.')

        # Summary
        self.learning_summary(scores_all, bins=10)
