# Python2 script to use PyBioMed to compute protein molecular descriptors from protein sequence.
# It takes as input a file with one sequence per line and the path to the output file.

import sys
sys.path.insert(0, '/hits/fast/mcm/fabbrilo/applications/PyBioMed-1.0/')  # Must add path to library!
from PyBioMed import Pyprotein
import pandas as pd

def main():
    
    # List of methods to compute specific descriptors
    methods = ['GetAAComp', 'GetDPComp', 'GetTPComp', 'GetPAAC', 'GetAPAAC']

    # Read input file with all sequences
    sequences = sys.argv[1]

    with open(sequences) as f:
        text = f.readlines()

    pdb_ids = [seq.strip().split(' ')[0] for seq in text]
    sequences = [seq.strip().split(' ')[1] for seq in text]

    # Iterate over all sequences and compute descriptors
    df = pd.DataFrame()
    i = 0  # Keep track of PDB considered
    for seq in sequences:
        descriptors = Pyprotein.PyProtein(str(seq))
        
        # Compute specific descriptors
        df_tmp = pd.DataFrame()
        for method in methods:
            res = eval(f"descriptors.{method}()")
            
            df_tmp.concat([df_tmp, pd.DataFrame(res, index=[0])], 
                         axis=1, sort=False)
        df_tmp.insert(0, 'pdb', pdb_ids[i])  # Insert column with PDB ID
        i += 1

        # Append row to DataFrame of descriptors
        df = df.append(df_tmp, ignore_index=True)
    
    return df

if __name__ == '__main__':

    result = main()

    # Save result to `path_out`
    path_out = sys.argv[2]
    result.to_csv(path_out, index=False)
