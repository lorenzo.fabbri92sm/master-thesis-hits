"""
Python class which contains functions to predict kinetic rates from molecular descriptors
of receptor and ligand.

Input data must have the following columns (with these names): receptor, ligand,
rate, uniprot, cas.
"""

__author__ = "Lorenzo Fabbri"
__mantainer__ = "Lorenzo Fabbri"
__email__ = "lorenzo.fabbri@h-its.org"  # "lorenzo.fabbri-1@studenti.unitn.it"

import os
import subprocess
import itertools
import requests
import pprint

import warnings
warnings.filterwarnings('ignore', category=DeprecationWarning)

import numpy as np
import pandas as pd

from sklearn.ensemble import RandomForestRegressor
from sklearn.dummy import DummyRegressor
from sklearn.mixture import GaussianMixture
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV, cross_val_score, KFold
from sklearn import preprocessing
from sklearn.pipeline import Pipeline, make_pipeline
from sklearn.feature_selection import mutual_info_regression, SelectKBest, VarianceThreshold

import matplotlib.pyplot as plt
import seaborn as sns

import pubchempy as pcp
from rdkit import Chem
from rdkit.Chem import Descriptors, AllChem, rdmolops
from rdkit.Chem.Fingerprints import FingerprintMols
from rdkit.Chem.AllChem import ComputeMolVolume
from mordred import Calculator, descriptors

from Bio.SeqUtils.ProtParam import ProteinAnalysis
from Bio.PDB import PDBList, PDBParser, PPBuilder
from modlamp.descriptors import GlobalDescriptor

import igraph
import networkx as nx

# Dictionary mapping one-letter code to full amino acid name
map_aa = dict()
map_aa['A'] = ['alanine']
map_aa['R'] = ['arginine']
map_aa['N'] = ['asparagine']
map_aa['D'] = ['aspartic acid']
map_aa['B'] = ['asparagine']
map_aa['C'] = ['cysteine']
map_aa['E'] = ['glutamic acid']
map_aa['Q'] = ['glutamine']
map_aa['Z'] = ['glutamine']
map_aa['G'] = ['glycine']
map_aa['H'] = ['histidine']
map_aa['I'] = ['isoleucine']
map_aa['L'] = ['leucine']
map_aa['K'] = ['lysine']
map_aa['M'] = ['methionine']
map_aa['F'] = ['phenylalanine']
map_aa['P'] = ['proline']
map_aa['S'] = ['serine']
map_aa['T'] = ['threonine']
map_aa['W'] = ['tryptophan']
map_aa['Y'] = ['tyrosine']
map_aa['V'] = ['valine']

class mlmf:

    def __init__(self, path_data, path_res):

        # Read input data
        data = pd.read_csv(path_data)

        self.data = data  # Pandas DataFrame with receptors, ligands, IDs and rates
        self.pdb = dict()  # Dictionary containing pairs receptor UniProt ID-PDB IDs

        # Store best estimators from nested CV for each method (regression)
        self.best_estimators_regr = None
        # Store best estimators from nested CV for each method (classification)
        self.best_estimators_clas = None

        # Path to directory where results should be stores
        self.path_res = path_res

        # Mapping for amino acid residues with compounds and SMILES
        self.map_aa = map_aa
        # Iterate over all residues and retrieve compounds from PubChem and SMILES
        for key, value in self.map_aa.items():
            self.map_aa[key].append(pcp.get_compounds(value, 'name')[0])
            self.map_aa[key].append(self.map_aa[key][1].canonical_smiles)

    def data_augmentation(self, download_seq, download_pdb, path_out_pdb):
        """
        Add more information to input data: protein sequence of receptors, PDB ID for
        receptors, pubchempy molecule object for ligands and SMILES for ligands.

        Parameters
        ----------
        download_seq : bool
            If True, the (full) sequence for each receptor will be downloaded.
        download_pdb : bool
            If True, the PDB files for each receptor will be downloaded and stored.
        path_out_pdb : string
            Absolute or relative path to directory where PDB files will be stored.

        Returns
        -------
        None
        """

        # Add column with PDB ID for receptors
        self.retrieve_pdb(download=download_pdb, path_out=path_out_pdb)

        # Add column with protein sequence of receptors
        if download_seq:
            self.retrieve_seq()
        else:
            self.data['sequence'] = -1

        # Add column with puchempy molecule object for ligands.
        # Compounds must be re-computed every time since cannot be saved.
        self.add_compound()

        # Add column with SMILES for ligands
        if 'smiles' not in self.data.columns:
            self.add_smiles()

        # Check augmented data
        print('\n================================================================================')
        print(f'Number of NaN: {self.data.isna().sum().sum()}.')
        print(f'Shape of data: {self.data.shape}.')
        print('================================================================================\n')

    def clean_data(self, data, to_skip, threshold, to_print=False):
        """
        Clean up data in-place.

        Parameters
        ----------
        data : DataFrame
            Pandas DataFrame with rows being molecular complexes and columns being
            features and descriptors.
        to_skip : int
            Number of columns to skip when performing analyses. to_skip is included.
        threshold : float
            Drop columns with correlation greater than threshold.
        to_print : bool
            If True, information on what is being performed is printed on screen. Default
            is False.

        Returns
        -------
        list
            Python list containing highly correlated descriptors.
        """

        print(f'Shape of input data before cleaning: {data.shape}.')
        
        # Drop columns with the same name
        print('\nStarted checking duplicated columns...')
        data = data.loc[:, ~data.columns.duplicated()]

        # Iterate over the columns of data and store columns to drop
        to_drop = set()
        print('\nStarted checking and cleaning input data...')
        for coln in data.columns[to_skip:]:

            # Drop columns containing non-numerical values (descriptors)
            if data[coln].dtype == 'object' and coln not in to_drop:
                if to_print:
                    print(f'\tColumn {coln} is of type object.')
                to_drop.add(coln)

            # Drop columns containing NaNs (descriptors)
            if pd.isna(data[coln]).sum() > 0 and coln not in to_drop:
                if to_print:
                    print(f'\tColumn {coln} contains missing values.')
                to_drop.add(coln)

        # Drop stored columns
        data.drop(list(to_drop), axis=1, inplace=True)
        
        # Drop columns with zero variance (descriptors)
        print('\nStarted checking zero-variance columns...')
        selector = VarianceThreshold()
        selector.fit(data.iloc[:, to_skip:])
        
        colns_zeroVar = list(data.iloc[:, to_skip:].columns[~selector.get_support()].values)
        if to_print:
            print(f'\tZero-variance columns: {colns_zeroVar}.')
        data.drop(colns_zeroVar, axis=1, inplace=True)

        # Drop highly correlated columns (descriptors)
        if threshold is not None:
            print('\nStarted checking correlation...')
            corr_matrix = data.iloc[:, to_skip:].corr().abs()
            upper = corr_matrix.where(np.triu(np.ones(corr_matrix.shape), k=1).astype(np.bool))
            to_drop = [coln for coln in upper.columns if any(upper[coln] > threshold)]
            if to_print:
                print(f'\tNumber of highly correlated columns: {len(to_drop)}.')
                print(f'\tHighly correlated columns: {to_drop}.')
            data.drop(to_drop, axis=1, inplace=True)
        else:
            to_drop = []

        # Drop `bool` columns
        print('\nStarted checking type descriptors...')
        for column in data.columns[to_skip:]:
            if data[column].dtype=='bool':
                if to_print:
                    print(f'\tColumn {column} is of type bool.')
                data.drop([column], axis=1, inplace=True)

        # Reset index
        data.reset_index(drop=True, inplace=True)

        print(f'\nShape of input data after cleaning: {data.shape}.')
        
        return data, to_drop  # New DataFrame and correlated descriptors

    def feature_selection(self, data, to_skip, threshold):
        """
        Perform automatic feature selection using mutual information.

        Parameters
        ----------
        data : DataFrame
            Pandas DataFrame.
        to_skip : int
            Number of columns to skip when performing analyses. to_skip is included.
        threshold : int
            Number of descriptors to retain.

        Returns
        -------
        DataFrame
            Modified Pandas DataFrame with threshold number of molecular descriptors.
        """

        # Compute mutual information
        mi = mutual_info_regression(data.iloc[:, to_skip:], data['rate'])
        mi /= np.max(mi)

        # Assert input threshold is smaller than shape of data
        if threshold > data.shape[1]:
            print('The input threshold is too large. No feature selection performed.')
            return data

        # Select columns with larget mutual information (top `threshold`)
        idxs = np.argpartition(mi, -threshold)[-threshold:]

        # Plot results
        plt.figure(figsize=(40, 20))
        for i in range(len(idxs)):
            plt.subplot(int(threshold/10), 10, i+1)
            plt.scatter(data.iloc[:, idxs[i]+to_skip], data['rate'],
                    label=np.round(float(f'{mi[idxs[i]]}'), 2), s=20)
            plt.title(f'{data.columns[idxs[i]+to_skip]}')
            plt.legend()
        plt.tight_layout()
        plt.show()

        # Overwrite data
        return data.iloc[:, np.r_[:to_skip, idxs+to_skip]]

    def retrieve_pdb(self, download, path_out):
        """
        Retrieve PDB IDs of receptors using UniProt IDs and, if desired,
        download PDB files.

        Parameters
        ----------
        download : bool
            If True, the PDB files for each receptor will be downloaded and stored.
        path_out : string
            Absolute or relative path to directory where the PDB files will be stored.

        Returns
        -------
        None
        """

        # Download PDB files for receptors
        if download:
            print('----------------------------------------')

            pdbl = PDBList()
            for key, vals in self.pdb.items():
                for pdb_id in vals:
                    pdbl.retrieve_pdb_file(pdb_id, pdir=path_out,
                                           file_format='pdb')

        # Clean data by removing rows for which no PDB was found
        print('========================================')
        print(f'Number of NaN (PDB): {self.data.isna().sum().sum()}.')
        print('========================================')
        self.data.dropna(inplace=True)
        self.data.reset_index(inplace=True, drop=True)

    def retrieve_seq(self):
        """
        Retrieve sequence of receptors from UniProt.

        Parameters
        ----------
        None

        Returns
        -------
        None
        """

        print('----------------------------------------')
        print('Retrieving sequences...')

        # Create temporary set to avoid downloading same sequence multiple times
        seen = set()
        # Create dictionary to store sequences and avoid downloading same sequence multiple times
        seqs_dict = dict()

        for idx in self.data.index:
            receptor = self.data.loc[self.data.index[idx], 'uniprot']

            if receptor not in seen:
                seen.add(receptor)

                params = {'query': receptor, 'format': 'fasta', 'include': 'no'}
                response = requests.get('https://www.uniprot.org/uniprot/', params=params)

                # Store sequence in dictionary
                seq = ''.join(response.text.split('\n')[1:])
                seqs_dict[receptor] = seq

        # Add sequences to `data`
        for idx in self.data.index:
            receptor = self.data.loc[self.data.index[idx], 'uniprot']
            self.data.at[idx, 'sequence'] = seqs_dict[receptor]

        # Clean data by removing rows for which no sequence was found
        print('========================================')
        print(f'Number of NaN (SEQ): {self.data.isna().sum().sum()}.')
        print('========================================')
        self.data.dropna(inplace=True)
        self.data.reset_index(inplace=True, drop=True)

    def add_compound(self):
        """
        Retrieve compound objects using pubchempy.

        Parameters
        ----------
        None

        Returns
        -------
        None
        """
        
        # List containing ligands which were not found automatically
        self.comp_not_found = []

        print('----------------------------------------')
        print('Retrieving compounds...')
        
        # Loop over unique ligands and retrieve compound using PubChemPy
        ligands_unique = np.unique(self.data.ligand)
        
        for ligand in ligands_unique:
            # Get compound using CAS number
            c = pcp.get_compounds(ligand, 'name')
            
            try:
                # Add compound to data
                compound = c[0]
                idxs = self.data[self.data.ligand==ligand].index.values
                self.data.at[idxs, 'compound'] = compound
                
            except IndexError:
                print(f'Ligand {ligand} was not found.')
                
                # If no compound is found, put NaN
                idxs = self.data[self.data.ligand==ligand].index.values
                self.data.at[idxs, 'compound'] = np.nan

                # Add compound not found to list
                self.comp_not_found.append(ligand)

        # Clean data by removing rows for which no compound was found
        print('========================================')
        print(f'Number of NaN (COMP): {self.data.isna().sum().sum()}.')
        print('========================================')
        self.data.dropna(inplace=True)
        self.data.reset_index(inplace=True, drop=True)

    def add_smiles(self):
        """
        Compute SMILES for the ligands.

        Parameters
        ----------
        None

        Returns
        -------
        None
        """

        print('----------------------------------------')
        print('Retrieving SMILES...')

        for idx in self.data.index:
            ligand = self.data.loc[self.data.index[idx], 'compound']

            smiles = ligand.canonical_smiles
            # Check whether multiple small molecules. If this is the case, 
            # take the longest string
            if '.' in smiles:
                smiles_split = smiles.split('.')
                smiles = max(smiles_split, key=len)
                self.data.at[self.data.index[idx], 'smiles'] = smiles
            else:
                self.data.at[self.data.index[idx], 'smiles'] = smiles

        # Clean data by removing rows for which no SMILES were found
        print('========================================')
        print(f'Number of NaN (SMILES): {self.data.isna().sum().sum()}.')
        print('========================================')
        self.data.dropna(inplace=True)
        self.data.reset_index(inplace=True, drop=True)

    ##################################################################

    def find_pocket(self, path_bin, path_pdb, path_out, path_mapping):
        """
        Find residues in putative binding pocket(s). Calls external package: P2Rank.
        At the moment, only one PDB file for each receptor is used and only the first
        hit of P2Rank is used to retrieve protein sequence of the putative
        binding pocket.

        Parameters
        ----------
        path_bin : string
            Absolute or relative path to P2Rank program.
        path_pdb : string
            Absolute or relative path to directory where PDB files of receptors
            are stored.
        path_out : string
            Absolute or relative path where results P2Rank will be stored.
        path_mapping : string
            Absolute or relative path to CSV file containing mapping
            between UniProt IDs and PDB IDs.

        Returns
        -------
        None
        """

        # Mapping from UniProt to PDB IDs
        mapping = pd.read_csv(path_mapping, sep='\t')
        mapping = mapping.to_dict(orient='records')
        map_uniprot_pdb_dict = dict()

        for dic in mapping:
            k = dic['From']
            map_uniprot_pdb_dict[k] = []
        for k in map_uniprot_pdb_dict.keys():
            for dic in mapping:
                if dic['From']==k:
                    map_uniprot_pdb_dict[k].append(dic['To'])

        # Iterate over each kinase, retrieve ONE PDB file and run P2Rank
        for kinase in map_uniprot_pdb_dict.keys():

            # Keep looking for PDB file until "big enough"
            i = 0  # Iterator PDB for each kinase
            current_max = 0
            try:
                while True:
                    pdb_name = map_uniprot_pdb_dict[kinase][i].lower()
                    i += 1

                    parser = PDBParser().get_structure('tmp',
                            os.path.join(path_pdb, pdb_name+'.pdb'))
                    tmp_list = list(parser[0])[0]
                    residues_notHet = [residue for residue in tmp_list if residue.id[0]==' ']

                    if len(residues_notHet)>current_max:
                        current_max = len(residues_notHet)
                        current_max_name = pdb_name

                    if len(residues_notHet)>=300:  # More or less random threshold
                        break

            except IndexError:
                print(f'\nNo structure for {kinase} satisfies condition.')
                print(f'PDB: {current_max_name}. Length: {current_max}.')
                pdb_name = current_max_name

            # Add PDB ID to data as reference
            idx = self.data[self.data['uniprot']==kinase].index.values
            self.data.at[idx, 'pdb'] = pdb_name

            # Check whether result P2Rank is already available
            list_res = next(os.walk(path_out))[1]
            if pdb_name in list_res:
                continue

            for root, dirs, files in os.walk(path_pdb):
                files = [file.split('.')[0] for file in files]

                if pdb_name in files:
                    pdb_path = os.path.join(root, pdb_name)+'.pdb'

                    # Run P2Rank
                    print(f'Running P2Rank for {pdb_name}...\n')
                    subprocess.run(f'bash {path_bin}prank predict -threads 4 \
                            -o {os.path.join(path_out, pdb_name)} -f {pdb_path}', shell=True)

        # Call method to iterate over results of P2Rank and extract sequence
        seq_pocket_all = self.find_pocket_seq(path_out, path_pdb)

        print('\n')
        # Add sequence putative binding pocket to data
        for pdb, seq in seq_pocket_all.items():
            print(f'Adding sequence for {pdb}...')
            idxs = self.data[self.data['pdb']==pdb].index.values
            self.data.at[idxs, 'seq_pocket'] = seq

        # Check column containing sequence putative binding site.
        # It is possible that no sequence was found or that it is too short to be
        # used with BioPython and other libraries
        self.check_seq_pocket()

    def find_pocket_seq(self, path_res, path_pdb):
        """
        Retrieve protein sequence in found putative binding pocket.

        Parameters
        ----------
        path_res : string
            Absolute or relative path to directory where results of P2Rank are stored.
        path_pdb : string
            Absolute or relative path to directory where the PDB files are stored.

        Returns
        -------
        dictionary
            Python dictionary where keys are PDB IDs and values are the protein sequence
            of the found putative binding pocket.
        """

        # Store PDB ID and sequence putative binding pocket
        seq_pocket_all = dict()

        # Iterate over all P2Rank results
        for res in os.listdir(path_res):
            for files in os.listdir(os.path.join(path_res, res)):

                # Load file with residues
                if files.endswith('pdb_residues.csv'):

                    path_tmp = os.path.join(path_res, res)
                    with open(os.path.join(path_tmp, files)) as f:
                        text = f.read()
                        residues = []  # List for all found residues

                        for line in text.splitlines(True):
                            if not line.startswith('chain'):

                                info = line.split()
                                id_pocket = int(info[6].split(',')[0])
                                if id_pocket==1:  # Take just most probable pocket
                                    residues.append(info[2].split(',')[0])  # 3-letter residue

                        # Convert 3-letter code to 1-letter code and create protein sequence
                        seq_pocket = self.create_seq(residues)
                        seq_pocket_all[res] = seq_pocket

        return seq_pocket_all

    def create_seq(self, residues):
        """
        Create protein sequence given list of residues with 3-letter IDs.

        Parameters
        ----------
        residues : list
            List containing 3-letter code of aminoacids found in the putative
            binding pocket.

        Returns
        -------
        string
            Concatenated 1-letter coded protein sequence of putative binding pocket.
        """

        # Dictionary mapping 3-letter to 1-letter codes
        mapping_aa = {'CYS': 'C', 'ASP': 'D', 'SER': 'S', 'GLN': 'Q', 'LYS': 'K',
                'ILE': 'I', 'PRO': 'P', 'THR': 'T', 'PHE': 'F', 'ASN': 'N',
                'GLY': 'G', 'HIS': 'H', 'LEU': 'L', 'ARG': 'R', 'TRP': 'W',
                'ALA': 'A', 'VAL':'V', 'GLU': 'E', 'TYR': 'Y', 'MET': 'M',
                'MSE': 'M'}

        # Convert each 3-letter code to 1-letter code
        ret = []  # Converted sequence
        for aa in residues:
            ret.append(mapping_aa[aa])

        # Join list into sequence
        return ''.join(ret)

    def check_seq_pocket(self):
        """
        Check column dataset containing the sequence of the putative binding pocket:
        whether it is NaN or if it is too short.

        Parameters
        ----------
        None

        Returns
        -------
        None
        """

        # Check whether NaN
        nans = self.data.seq_pocket.isna().sum()
        print(f'\nNumber of NaNs: {nans}.')

        # Check minimum length
        lengths = [len(str(seq)) for seq in self.data.seq_pocket.values]
        print(f'Minimum length: {np.min(lengths)}.\n')

    #######################################################

    def compute_mf_2d(self, data, to_skip, coln_seq, dict_features):
        """
        Compute molecular descriptors without considering structure of receptor.

        Parameters
        ----------
        data : DataFrame
            Pandas DataFrame with features and molecular descriptors.
        to_skip : int
            Number of columns that should not be considered when 
            checking the dataset. Only used when checking for Inf.
        coln_seq : string
            Name of the column containing protein sequence: can be sequence of
            complete receptor or just binding pocket.
        dict_features : dictionary
            Python dictionary where keys are methods to compute the molecular
            descriptors and values are properties to be computed.

        Returns
        -------
        None
        """

        # Loop over keys of `dict_features` to compute molecular descriptors based on desired library
        for method, features in dict_features.items():

            # Generate input variables depending on method
            if method == 'rdkit':
                continue
            elif method == 'pcp':
                attrs = [data, features]
            elif method == 'biopython':
                attrs = [data, features, coln_seq]
            else:
                print(f'Method {method} is currently not supported.')
                continue

            getattr(self, f'{method}_mf')(*attrs)

        # Check data
        print('\n================================================================================')
        print(f'Number of NaN: {data.isna().sum().sum()}.')
        #print(f'Number of Inf: {np.isinf(data.iloc[:, to_skip:]).sum().sum()}.')
        print(f'Shape of data: {data.shape}.')
        print('================================================================================\n')

    def mol2graph(self, mol):
        """
        Convert RDKit molecule to graph object.

        Parameters
        ----------
        mol : molecules
            RDKit molecule.

        Returns
        -------
        graph
            iGraph object.
        """

        ad_matrix = rdmolops.GetAdjacencyMatrix(mol)
        bond_idxs = [(b.GetBeginAtomIdx(), b.GetEndAtomIdx()) for b in mol.GetBonds()]
        ad_list = np.ndarray.tolist(ad_matrix)

        graph = igraph.Graph()
        g = graph.Adjacency(ad_list).as_undirected()

        for idx in g.vs.indices:
            g.vs[idx]['AtomicNum'] = mol.GetAtomWithIdx(idx).GetAtomicNum()
            g.vs[idx]['AtomicSymbole'] = mol.GetAtomWithIdx(idx).GetSymbol()
        for bd in bond_idxs:
            b_type = mol.GetBondBetweenAtoms(bd[0], bd[1]).GetBondTypeAsDouble()
            g.es[g.get_eid(bd[0], bd[1])]['BondType'] = b_type

        return g

    def rdkit_mf(self, data, descriptors):
        """
        Compute molecular descriptors using RDKit.

        Parameters
        ----------
        data : DataFrame
            Pandas DataFrame with features and molecular descriptors.
        descriptors : list
            Python list containing molecular descriptors to be computed.

        Returns
        -------
        DataFrame
            Pandas DataFrame containing the computed molecular descriptors.
        """

        print('Computing molecular descriptors using RDKit...')
        
        # Get unique SMILES and create dictionary for results
        smiles_unique = list(np.unique(data.smiles))
        results = {smiles: dict() for smiles in smiles_unique}
        
        # Loop over unique SMILES and compute desired descriptors
        for smiles in smiles_unique:
            
            mol = Chem.MolFromSmiles(smiles)
            
            for descriptor in descriptors:
                
                # Number of halogens
                if descriptor=='fr_halogen':
                    res = eval(f'Chem.Fragments.{descriptor}(mol)')
                    
                    results[smiles][descriptor] = res
                
                # Fraction of C atoms SP3 hybridized
                elif descriptor=='CalcFractionCSP3':
                    res = eval(f'Chem.rdMolDescriptors.{descriptor}(mol)')
                    
                    results[smiles][descriptor] = res

                # Gasteiger charges
                elif descriptor=='GasteigerCharge':
                    mol_tmp = Chem.MolFromSmiles(smiles)
                    AllChem.ComputeGasteigerCharges(mol_tmp)
                    
                    vec_charges = [float(a.GetProp('_GasteigerCharge')) for a in mol_tmp.GetAtoms()]
                    
                    results[smiles][descriptor] = np.sum(vec_charges)

                # Volume
                elif descriptor=='MolVolume':
                    mol_tmp = Chem.AddHs(Chem.MolFromSmiles(smiles))
                    AllChem.EmbedMolecule(mol_tmp)
                    
                    results[smiles][descriptor] = ComputeMolVolume(mol_tmp)

                # Wiener index
                elif descriptor=='wiener_index':
                    g = self.mol2graph(mol)
                    g = nx.Graph([edge.tuple for edge in g.es])
                    
                    results[smiles][descriptor] = nx.wiener_index(g)
                
                # Flexibility (ratio number rotatable bonds and number bonds
                # between heavy atoms)
                elif descriptor=='flexibility':
                    tot_num_bonds = Chem.rdchem.Mol.GetNumBonds(mol, 
                                                               onlyHeavy=True)
                    num_rot_bonds = Descriptors.NumRotatableBonds(mol)
                    res = num_rot_bonds / tot_num_bonds
                    
                    results[smiles][descriptor] = res
                
                # Number of atoms
                elif descriptor=='GetNumAtoms':
                    
                    results[smiles][descriptor] = Chem.rdchem.Mol.GetNumAtoms(mol)

                # All the other descriptors that can be computed in the same way
                else:
                    res = eval(f'Descriptors.{descriptor}(mol)')
                    
                    results[smiles][descriptor] = res

        # Add results to data
        df_results = pd.DataFrame()
        for smiles, results_partial in results.items():
            df_results = df_results.append(pd.DataFrame(results_partial, index=[0]), 
                                           sort=False)
        df_results.insert(0, 'smiles', smiles_unique)
        df_results.reset_index(inplace=True, drop=True)
        
        new_colns = [df_results.columns[0]] + [coln+'_lig' for coln in df_results.columns[1:]]
        df_results.columns = new_colns
        
        data = pd.merge(data, df_results, on='smiles')
        data.reset_index(inplace=True, drop=True)
        
        # Check data
        print('\n================================================================================')
        print(f'Number of NaN: {data.isna().sum().sum()}.')
        print(f'Shape of data: {data.shape}.')
        print('================================================================================\n')
        
        return data

    def pcp_mf(self, data, features):
        """
        Compute molecular descriptors using PubChemPy.

        Parameters
        ----------
        data : DataFrame
            Pandas DataFrame with features and molecular descriptors.
        features : list
            Python list containing molecular descriptors to be computed.

        Returns
        -------
        None
        """

        print('\nComputing molecular descriptors using PubChemPy...')

        # Loop over desired molecular descriptors
        for feature in features:
            print(f'\tComputing descriptor {feature}...')

            for idx in data.index:
                mol = data.loc[data.index[idx], 'compound']

                data.at[data.index[idx], feature+'_lig'] = eval(f'mol.{feature}')

    def mordred_mf(self, data, ignore_3D=True):
        """
        Compute molecular descriptors using mordred. At the moment, `ignore_3D` 
        is set to True by default.
        For a description of the computed descriptors, see
        http://mordred-descriptor.github.io/documentation/master/descriptors.html.

        Parameters
        ----------
        data : DataFrame
            Pandas DataFrame with features and molecular descriptors.
        ignore_3D : bool
            Whether to compute 3D descriptors or not. Default to False (i.e.,
            3D descriptors are computed).

        Returns
        -------
        DataFrame
            Pandas DataFrame with features and new molecular descriptors.
        """

        print('Computing molecular descriptors using mordred...')

        # Get all ligands in data and convert to molecule
        ligands = [Chem.MolFromSmiles((ligand)) for ligand in data['smiles']]

        # Create DataFrame with molecular features
        calc = Calculator(descriptors, ignore_3D=ignore_3D)
        features = calc.pandas(ligands, quiet=True)

        # Rename columns to identify whether is ligand or receptor descriptors
        new_colns = [coln+'_lig' for coln in features.columns]
        features.columns = new_colns

        # Concatenate DataFrame with data
        data = pd.concat([data, features], axis=1, sort=False)

        # Check data
        print('\n================================================================================')
        print(f'Number of NaN: {data.isna().sum().sum()}.')
        print(f'Shape of data: {data.shape}.')
        print('================================================================================\n')

        return data

    def biopython_mf(self, data, features, coln_seq):
        """
        Compute molecular descriptors for receptor sequence using BioPython.

        Parameters
        ----------
        data : DataFrame
            Pandas DataFrame with features and molecular descriptors.
        features : list
            Python list containing molecular descriptors to be computed.
        coln_seq : string
            Name of the column containing protein sequence: can be protein sequence of
            complete receptor or just binding pocket.

        Returns
        -------
        None
        """

        print('\nComputing molecular descriptors using BioPython...')

        # Loop over desired molecular descriptors
        for feature in features:
            print(f'\tComputing descriptor {feature}...')

            for idx in data.index:
                # Get sequence
                seq = ProteinAnalysis(data.loc[data.index[idx], coln_seq].replace('X', ''))

                # Must deal with some descriptors separately
                if feature == 'get_amino_acids_percent':
                    aa_perc = seq.get_amino_acids_percent()
                    for aa, perc in aa_perc.items():
                        data.at[data.index[idx], aa+'_perc_rec'] = perc

                elif feature == 'flexibility':
                    data.at[data.index[idx], feature+'_rec'] = np.mean(seq.flexibility())

                else:
                    # Compute feature
                    res = eval(f'seq.{feature}()')

                    # If the result is a list of features, iterate over them
                    if isinstance(res, list) or isinstance(res, tuple):
                        for i in range(len(res)):
                            data.at[data.index[idx], feature + f'_{i}_rec'] = res[i]
                    else:
                        data.at[data.index[idx], feature+'_rec'] = res
                        
    def analyse_sequence(self, data, which_seq):
        """
        Generate additional descriptors starting from the protein sequence.
        
        Parameters
        ----------
        data : DataFrame
            Pandas DataFrame with features and molecular descriptors.
        which_seq : string
            Name of the column in the DataFrame containing the sequences for which the
            descriptors will be computed.
        
        Returns
        -------
        None
        """
        
        # Retrieve unique sequences from data
        seq_unique = data[which_seq].unique()
        
        # Iterate over sequences and compute descriptors
        for seq in seq_unique:

            # Create dictionary to store all generated descriptors
            aa_desc = dict()
            # Get indices in data corresponding to this specific sequence
            idxs = data[data[which_seq]==seq].index.values

            seq = seq.replace('X', '')
            
            # Create list of single amino acids
            aa = list(seq)
            # Create list with PubChemPy objects for considered sequence
            pcp_mol = {a: self.map_aa[a][1] for a in aa}
            # Create list containing RDKit molecules from SMILES
            rdkit_mol = {a: Chem.MolFromSmiles(self.map_aa[a][2]) for a in aa}
            
            # Compute descriptors from modlAMP
            descriptors_modl = ['aliphatic_index', 'boman_index', 
                               'hydrophobic_ratio']
            global_desc = GlobalDescriptor(seq)
            
            for desc in descriptors_modl:
                result = eval(f'global_desc.{desc}()')
                result = global_desc.descriptor.flatten()[0]
                aa_desc[global_desc.featurenames[0]+'_rec'] = result

            # Compute descriptors from PubChemPy
            descriptors_pcp = ['tpsa', 
                               'complexity', 'molecular_weight', 'rotatable_bond_count', 
                               'heavy_atom_count', 'charge']
            for desc in descriptors_pcp:
                result = sum([eval(f'mol.{desc}') for mol in pcp_mol.values()])
                aa_desc[desc+'_rec'] = result

            # Compute descriptors from RDKit
            descriptors_rdkit = ['MolLogP', 'MolMR', 
            'NHOHCount', 'NOCount', 'FractionCSP3']
            for desc in descriptors_rdkit:
                result = sum([eval(f'Descriptors.{desc}(mol)') for mol in rdkit_mol.values()])
                aa_desc[desc+'_rec'] = result

            # Add computed descriptors to data
            for desc in aa_desc.keys():
                data.at[idxs, desc] = aa_desc[desc]

    def pbm_mf(self, path_receptors, path_res, path_script, which_seq):
        """
        Compute molecular descriptors for receptors starting from the protein sequences
        using PyBioMed (requires Python2).

        Parameters
        ----------
        path_receptors : string
            Absolute or relative path to directory where TXT file with sequences of
            receptors will be stored.
        path_res : string
            Absolute or relative path to directory where results PyBioMed will be stored.
        path_script : string
            Absolute or relative path to Python script with commands to run PyBioMed (provided).
        which_seq : string
            Name of the column in the DataFrame containing the sequences for which the
            descriptors will be computed.

        Returns
        -------
        None
        """

        # Save unique sequences of receptors to file (one per line)
        self.data[['pdb', which_seq]].drop_duplicates().to_csv(path_receptors, index=False,
                header=False, sep=' ')

        # Call PyBioMed from Shell
        command = f'python2 {path_script} {path_receptors} {path_res}'
        process = subprocess.Popen(command, shell=True,
                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output, error = process.communicate()

        # Read file with results PyBioMed
        pbm_res = pd.read_csv(path_res)

        # Rename columns to identify whether is ligand or receptor descriptors
        new_colns = [pbm_res.columns[0]] + [coln+'_rec' for coln in pbm_res.columns[1:]]  # Rename all except `pdb`
        pbm_res.columns = new_colns

        # Check data
        print('\n================================================================================')
        print(f'Number of NaN: {pbm_res.isna().sum().sum()}.')
        print(f'Shape of data: {pbm_res.shape}.')
        print('================================================================================\n')

        # Merge results with DataFrame
        self.data = pd.merge(self.data, pbm_res, on='pdb')
        self.data.reset_index(inplace=True, drop=True)

        # Check merged data
        print('\n================================================================================')
        print(f'Number of NaN: {self.data.isna().sum().sum()}.')
        print(f'Shape of data: {self.data.shape}.')
        print('================================================================================\n')

    ###############################################################################################

    def q2f3(self, y_train, y_test, y_pred):
        """
        Compute Q2F3 metric for model evaluation. For details, see:
        https://pubs.acs.org/doi/abs/10.1021/acs.jcim.6b00277?src=recsys&journalCode=jcisd8.

        Parameters
        ----------
        y_train : array
            NumPy array containing values of dependent variable for training.
        y_test : array
            NumPy array containing values of dependent variable for test.
        y_pred : array
            NumPy array containing the predicted values of dependent variable.

        Returns
        -------
        float
            Q2F3 score.
        """

        # Reshape otherwise this idiot does stupid things
        y_train = y_train.reshape(-1, 1)
        y_test = y_test.reshape(-1, 1)
        y_pred = y_pred.reshape(-1, 1)

        num = np.sum((y_test-y_pred)**2)
        den = np.sum((y_train-np.mean(y_train))**2)

        num = num/len(y_test)
        den = den/len(y_train)
        frac = num/den

        return (1-frac)

    def regression(self, X, y, scaling, group, methods, hyper_params, num_trials, 
        cv, search, n_iter,  test_size, num_run):
        """
        Perform regression using desired methods, number of iterations for the outer
        loop of CV, number of folds for the inner loop of CV and size of test set.

        Parameters
        ----------
        X : DataFrame
            Pandas DataFrame with only molecular descriptors and features like sequences and
            SMILES.
        y : array
            NumPy array containing the dependent variable.
        scaling : string or None
            If None, no scaling will be applied to the molecular descriptors. Otherwise, it
            must be a string representing one of the available scaling methods in Scikit-learn.
        group : string or None
            If None, no stratification will be applied when splitting data for training and test.
            Otherwise, it must be the name of a column present in the input dataset.
        methods : list
            Python list containing Scikit-learn methods (e.g., RandomForestRegressor()).
        hyper_params : list
            Python list containing as many dictionaries as the number of methods. Each dictionary
            contains the hyper-parameters that will be used in RandomizedSearchCV.
        num_trials : int
            Number of times the input data are split into training/validation and test sets.
        cv : int
            Number of folds to be used for Cross-Validation. It is the same for both the CVs.
        search : string
            Either randomizedsearchcv or gridsearchcv.
        n_iter : int
            Number of vectors sampled by RandomizedSearchCV.
        test_size : float
            Size of the test set.
        num_run : int
            Number representing how many times the cell (from the Jupyter notebook) was run.

        Returns
        -------
        None
        """

        # Assert certain columns are not passed to this function
        to_avoid = ['receptor', 'ligand', 'rate', 'uniprot', 'cas', 'count_lig', 'sequence',
                'compound', 'smiles', 'pdb', 'seq_pocket']
        for column in X.columns:
            if column in to_avoid:
                print(f'{column} should not be passed to this function.')
                return

        # Split data: training/validation and test sets.
        # Stratify based on column containing classes/types.
        if group is not None:
            X_trainVal, X_test, y_trainVal, y_test = train_test_split(X, y, test_size=test_size,
                    shuffle=True, stratify=self.data[group])
        else:
            print('Data splitting is not stratified.')
            X_trainVal, X_test, y_trainVal, y_test = train_test_split(X, y, test_size=test_size,
                    shuffle=True)
        print(f'\nShape of training and validation sets: {X_trainVal.shape}.')
        print(f'Shape of test set: {X_test.shape}.')
        y_trainVal = y_trainVal.values.reshape(-1, 1)
        y_test = y_test.values.reshape(-1, 1)

        # Store scores for each method
        scores = dict()
        # Store best predictor for each method and for each trial
        best_estimators = {type(method).__name__: [] for method in methods}

        # Specify scoring method
        scoring = 'neg_mean_squared_error'

        # Loop over methods
        i = 0
        for method in methods:
            method_name = type(method).__name__
            print(f'\nStarting method {method_name}...')

            # Store scores from both loops
            non_nested_scores = np.zeros(num_trials)
            nested_scores = np.zeros(num_trials)

            # Unpack parameters for specific method (dictionary for Grid or Random search)
            params = hyper_params[i]
            i += 1

            # Outer loop for CV
            j = 0  # To check whether to generate report outliers
            for trial in range(num_trials):
                print(f'\tStarting trial number {trial+1}...')

                # Define splitting strategy for both CVs
                inner_cv = KFold(n_splits=cv, shuffle=True, random_state=trial)
                outer_cv = KFold(n_splits=cv, shuffle=True, random_state=trial)

                # Non-nested CV (hyper-parameters search and fitting)
                scaler = eval(f'preprocessing.{scaling}()') if scaling else IdentityTransformer()

                if search=='randomizedsearchcv':
                    clf = make_pipeline(scaler,
                            RandomizedSearchCV(estimator=method, param_distributions=params,
                                cv=inner_cv, scoring=scoring, n_jobs=-1, n_iter=n_iter))
                elif search=='gridsearchcv':
                    clf = make_pipeline(scaler,
                            GridSearchCV(estimator=method, param_grid=params,
                                cv=inner_cv, scoring=scoring, n_jobs=-1))

                clf.fit(X_trainVal, y_trainVal.ravel(),
                        **{f'{search}__sample_weight': np.ones(len(y_trainVal))})

                non_nested_scores[trial] = np.sqrt(-clf.named_steps[search].best_score_)
                best_estimator = clf.named_steps[search].best_estimator_

                # Nested CV.
                # Use estimator found on previous step
                scaler = eval(f'preprocessing.{scaling}()') if scaling else IdentityTransformer()
                pipeline = make_pipeline(scaler, best_estimator)
                nested_score = cross_val_score(pipeline,
                        X=X_trainVal, y=y_trainVal.ravel(),
                        cv=outer_cv, scoring=scoring, n_jobs=-1)
                nested_scores[trial] = np.sqrt(-nested_score.mean())

                # Plot performance current trial
                scaler = eval(f'preprocessing.{scaling}()') if scaling else IdentityTransformer()
                scaler.fit(X_trainVal)

                self.plot_trial_CV(best_estimator, X_trainVal, X_test, y_trainVal, y_test, scaler, j, num_run)
                j += 1

                # Store best estimator for current method and current trial
                best_estimators[method_name].append(best_estimator)

            # Store results for current method
            scores[method_name] = {'non_nested': np.around(non_nested_scores, 2),
                    'nested': np.around(nested_scores, 2)}
            """print('\n')
            pprint.pprint(scores[method_name])
            print('\n')"""

        # Difference nested loops
        score_difference = np.round(non_nested_scores-nested_scores, 2)
        #print(f'Average difference (std): {score_difference.mean()} ({score_difference.std()}).\n')

        """# Plot scores
        for method, score in scores.items():
            self.plot_CV(method, score['non_nested'], score['nested'], scoring)
            plt.show()"""

        # Evaluate models on training and test (unseen) sets
        scaler = eval(f'preprocessing.{scaling}()') if scaling else IdentityTransformer()
        scaler.fit(X_trainVal)
        self.evaluate_trainTest(best_estimators, scaler.transform(X_trainVal), y_trainVal, 
                           scaler.transform(X_test), y_test)

        # Store best estimators (regression)
        self.best_estimators_regr = best_estimators

    def search_statistics(self, estimators):
        """
        Visualize statistics about parameters of best estimators.

        Parameters
        ----------
        estimators : dictionary
            Python dictionary containing fitted learning models.

        Returns
        -------
        None
        """

        # Store parameters of interest
        params_all = dict()
        list_params = ['max_depth', 'max_features', 'min_samples_leaf', 
        'min_samples_split', 'n_estimators']

        # Iterate over methods
        for method in estimators.keys():
            print(f'Method: {method}.')
            params_all[method] = {param: [] for param in list_params}

            # Iterate over fitted models
            i = 0
            for model in estimators[method]:
                print(f'\tModel: {i+1}.')
                i += 1

                params = model.get_params()
                params_all[method]['n_estimators'].append(params['n_estimators'])
                params_all[method]['max_depth'].append(params['max_depth'])
                params_all[method]['max_features'].append(params['max_features'])
                params_all[method]['min_samples_split'].append(params['min_samples_split'])
                params_all[method]['min_samples_leaf'].append(params['min_samples_leaf'])

        # Display DataFrames
        print('\n')
        for method in params_all.keys():
            print(f'Method: {method}.')
            display(pd.DataFrame(params_all[method]))

    def plot_trial_CV(self, estimator, X_trainVal, X_test, y_trainVal, y_test, scaler, 
        iteration, num_run):
        """
        Generate plots to evaluate performance for a single trial of CV.

        Parameters
        ----------
        estimator : Scikit-learn object
            Scikit-learn fitted estimator.
        X_trainVal : array
            NumPy array containing values of molecular descriptors for training and validation.
        X_test : array
            NumPy array containing values of molecular descriptors for test.
        y_trainVal : array
            NumPy array containing values of dependent variable for training and validation
        y_test : array
            NumPy array containing values of dependent variable for test.
        scaler : Scikit-learn object
            Scikit-learn fitted scaler.
        iteration : int
            Counter for the outer CV loop in the function `self.regression(...)`.
        num_run : int
            Counter for the number of times the cell (Jupyter notebook) was run.

        Returns
        -------
        None
        """

        fig, axes = plt.subplots(1, 2, figsize=(20, 10))

        # Store indeces of training and test sets
        idx_trainVal = X_trainVal.index.values
        idx_test = X_test.index.values

        # Predictions and scores for training
        y_pred_trainVal = estimator.predict(scaler.transform(X_trainVal)).reshape(-1, 1)
        q2f3 = np.round(self.q2f3(y_trainVal, y_trainVal, y_pred_trainVal), 2)
        r2 = np.round(r2_score(y_trainVal, y_pred_trainVal), 2)

        sns.scatterplot(x=[el[0] for el in y_trainVal],
            y=[el[0] for el in y_pred_trainVal],
            hue=self.data.iloc[idx_trainVal, 0].values.tolist(),
            ax=axes[0])  # Color by receptor
        axes[0].set_xlim(0.0)
        axes[0].set_ylim(0.0, axes[0].get_xlim()[1])
        axes[0].plot([0, axes[0].get_xlim()[1]], [0, axes[0].get_ylim()[1]], '--')
        axes[0].set_xlabel('true')
        axes[0].set_ylabel('predicted')
        axes[0].legend()
        axes[0].set_title(f'train (Q2, R2): {q2f3, r2}')

        ## Predictions and scores for test
        y_pred_test = estimator.predict(scaler.transform(X_test)).reshape(-1, 1)

        # Threshold for outliers based on residuals
        threshold_out = 0.5  # Arbitrary
        residuals_test = y_pred_test - y_test

        col_test = []  # Color based on whether outlier or not
        for el in residuals_test:
            col_test.append(1) if np.abs(el)>=threshold_out else col_test.append(0)
        col_test = np.array(col_test).reshape(-1, 1)

        q2f3 = np.round(self.q2f3(y_trainVal, y_test, y_pred_test), 2)
        r2 = np.round(r2_score(y_test, y_pred_test), 2)

        sns.scatterplot(x=[el[0] for el in y_test],
            y=[el[0] for el in y_pred_test],
            hue=self.data.iloc[idx_test, 0].values.tolist(),
            ax=axes[1])  # Color by receptor
        axes[1].set_xlim(0.0)
        axes[1].set_ylim(0.0, axes[1].get_xlim()[1])
        axes[1].plot([0, axes[1].get_xlim()[1]], [0, axes[1].get_ylim()[1]], '--')
        axes[1].set_xlabel('true')
        axes[1].set_ylabel('predicted')
        axes[1].legend()
        axes[1].set_title(f'test (Q2, R2): {q2f3, r2}')

        """# Plot of residuals
        axes[1, 0].scatter(y_pred_test, residuals_test, marker='o',
                c=col_test, label='residuals test')
        axes[1, 0].axhline(y=0.0)
        axes[1, 0].set_xlabel('predicted')
        axes[1, 0].set_ylabel('residuals')
        axes[1, 0].legend()"""

        """# Distribution of residuals. Pick should be at 0.0. No `abs()`
        sns.distplot(residuals_test, kde=False, norm_hist=False,
                ax=axes[1, 1], label='residuals test')
        axes[1, 1].axvline(x=0.0)
        axes[1, 1].set_xlabel('residuals')
        axes[1, 1].legend()"""

        """# Plot of predicted and true values (test) by index
        axes[2, 0].plot(y_pred_test, 'ro', label='predictions test')
        axes[2, 0].plot(y_test, 'go', label='true test')
        axes[2, 0].axhline(np.mean(y_test), label='average test')
        axes[2, 0].set_xlabel('index')
        axes[2, 0].set_ylabel('rate')
        axes[2, 0].legend()"""

        """# Bar-plot of count of receptor families in test set
        cp = sns.countplot(x=self.data.iloc[idx_test, 0], ax=axes[2, 1])
        cp.set_xticklabels(cp.get_xticklabels(), rotation=30)"""

        plt.tight_layout()
        plt.show()
        ##

        # Report on found outliers
        if iteration==0:
            which_rows = list(map(bool, col_test))
            idx_outliers = X_test[which_rows].index  # Indices of outliers in test set
            #print(f'\nIndex of outliers (threshold={threshold_out}):\n{list(idx_outliers)}.\n')
            #self.report_outliers(idx_outliers, num_run)

    def report_outliers(self, idx_outliers, num_run):
        """
        Generate nicely formatted report based on indices of 'outliers'.

        Parameters
        ----------
        idx_outliers : index
            Pandas index containing indices of elements in test set considered outliers.
        num_run : int
            Counter for the number of times the cell (Jupyter notebook) was run.

        Returns
        -------
        None
        """

        print('\n######################################################')
        print('Report on found outliers...')

        # Sub-select complete dataset based on input indices
        data_out = self.data.iloc[idx_outliers, :]
        # Add column to count number of runs
        data_out['num_runs'] = num_run

        # Save outliers to file to compare different runs of fitting
        file_name = 'outliers.csv'
        file_dir = self.path_res
        if os.path.exists(os.path.join(file_dir, file_name)):
            aw = 'a'
        else:
            aw = 'w'
        data_out.to_csv(path_or_buf=os.path.join(file_dir, file_name),
                columns=['receptor', 'ligand', 'num_runs'], mode=aw,
                header=False, index=False)

        # Extract some information to display. Convert rate to log10 scale
        data_out = data_out[['receptor', 'ligand', 'rate', 'num_runs']]
        data_out['rate'] = np.log10(1 / data_out['rate'])
        #display(data_out.head())

        # Barplots of receptors and ligands names across multiple runs.
        # Normalize by the number of runs (how many times you run the
        # cell containing the call to self.regression(...))
        data_tmp = pd.read_csv(os.path.join(file_dir, file_name),
                names=['receptor', 'ligand', 'num_runs'])
        max_num_runs = np.max(data_tmp['num_runs'])
        counts_rec = data_tmp['receptor'].value_counts() / max_num_runs
        counts_lig = data_tmp['ligand'].value_counts() / max_num_runs
        fig, axes = plt.subplots(1, 2, figsize=(40, 15))
        ax0 = sns.barplot(counts_rec.index, counts_rec.values, ax=axes[0])
        ax0.set_xticklabels(ax0.get_xticklabels(), rotation=45)
        axes[0].set_title('Barplot for receptors')
        ax1 = sns.barplot(counts_lig.index, counts_lig.values, ax=axes[1])
        ax1.set_xticklabels(ax1.get_xticklabels(), rotation=45)
        axes[1].set_title('Barplot for ligands')
        plt.show()

        # Plot summary on log10 scale
        plt.figure(figsize=(25, 10))
        sns.distplot(np.log10(1 / self.data['rate']), label='full data')
        for value in data_out['rate']:
            plt.axvline(x=value, color='r', linewidth=0.5)
        plt.legend()
        plt.title('Histogram of rate values with outliers (log10 scale).')
        plt.show()

        print('\n######################################################')

    def plot_fi(self, estimators, data, how_many):
        """
        Create DataFrame with feature importances and plot result,
        for each method and each estimator. Probably works only
        with Random Forests objects from Scikit-learn.

        Parameters
        ----------
        estimators : dictionary
            Python dictionary with keys being the names of the used methods (e.g.,
            RandomForestRegressor), and values being fitted Scikit-learn estimators.
        data : DataFrame
            Pandas DataFrame containing only the used molecular descriptors, and no
            features (e.g., sequences, SMILES). This must match the DataFrame used
            to fit the models.
        how_many : int
            Number of features to display in plot of FI.

        Returns
        -------
        None
        """

        # Iterate over methods
        for method in estimators.keys():
            i = 0
            # Iterate over estimators
            for estimator in estimators[method]:

                # Create DataFrame with feature importances
                df = pd.DataFrame({'cols': data.columns,
                                  'imp': estimator.feature_importances_})
                df.sort_values('imp', ascending=False, inplace=True)

                # Plot feature importances
                df.iloc[:how_many, :].plot('cols', 'imp', 'barh',
                        figsize=(20, 10), legend=False, title=f'FI for {method} #{i}.')
                i += 1

    def evaluate_trainTest(self, best_estimators, X_trainVal, y_trainVal, X_test, y_test):
        """
        Evaluate best estimators from nested CV on unseen data.

        Parameters
        ----------

        Returns
        -------
        """

        # Store metrics scores for each method
        scores = {method: [] for method in best_estimators.keys()}

        # Loop over methods
        for method, estimators in best_estimators.items():

            # Loop over best estimators
            for estimator in estimators:
                y_pred = estimator.predict(X_test)
                #print(f'SD of test predictions: {np.round(np.std(y_pred), 2)}.')
                
                # Dummy regressor training (mean)
                dr = DummyRegressor(strategy='mean')
                dr.fit(X_trainVal, y_trainVal)
                y_trainVal_predDummy = dr.predict(X_trainVal)

                # Dummy regressor test (mean)
                y_test_predDummy = dr.predict(X_test)

                # Currently MAE, RMSE, R2 and dummy MAE on training and test sets. Order is important
                scores[method].append(np.round(mean_absolute_error(y_test, y_pred), 2))
                scores[method].append(np.round(np.sqrt(mean_squared_error(y_test, y_pred)), 2))
                scores[method].append(np.round(r2_score(y_test, y_pred), 2))
                scores[method].append(np.round(mean_absolute_error(y_trainVal, y_trainVal_predDummy), 2))
                scores[method].append(np.round(mean_absolute_error(y_test, y_test_predDummy), 2))

        # Plot scores for each method and for each trial
        self.plot_scores(scores)
        plt.show()

    def plot_scores(self, scores):
        """
        Plot evaluation metrics for different methods and for each trial.

        Parameters
        ----------

        Returns
        -------
        """

        # Corresponds to number of different metrics to be computed
        by = 5

        # Loop over methods and create one figure for each
        for method, metrics in scores.items():

            # Plot all available metrics in same figure
            plt.figure(figsize=(10, 7))
            plt.plot(metrics[::by], 'o--', label='MAE test')
            plt.plot(metrics[1::by], 'o--', label='RMSE test')
            plt.plot(metrics[2::by], 'o--', label='R2 test')
            plt.plot(metrics[3::by], 'o--', label='MAE dummy training')
            plt.plot(metrics[4::by], 'o--', label='MAE dummy test')
            plt.xlabel('trials')
            plt.xticks(np.arange(len(metrics)/by))
            plt.ylabel('scores')
            plt.yticks(metrics)
            plt.legend()
            plt.title(f'Evaluation of {method}.')

    def plot_CV(self, method, non_nested_scores, nested_scores, scoring):
        """
        Plot scores from nested CV set-up.

        Parameters
        ----------

        Returns
        -------
        """

        # Plot scores on each trial for non-nested and nested (most important one) CV
        plt.figure(figsize=(10, 7))
        non_nested_line, = plt.plot(non_nested_scores, 'ro--', label='Non-Nested CV')
        nested_line, = plt.plot(nested_scores, 'bo--', label='Nested CV')
        plt.xlabel('trials')
        plt.ylabel('scores')
        plt.legend()
        plt.title(f'Method: {method}. Scoring: {scoring}.')

    #######################################################

    def clustering(self, data, to_skip, scaler, params):
        """
        Perform clustering using a GMM.

        Parameters
        ----------
        data : DataFrame
            Pandas DataFrame containing features and molecular descriptors.
        to_skip : int
            Number indicating the columns to be skipped.
        scaler : string
            Either a string indicating the scaler from Scikit-learn or None.
        params : dictionary
            Python dictionary containing the parameters for the GMM.

        Returns
        -------
        Scikit-learn fitted model
            Best GMM model.
        """

        # Scale input data
        if scaler is not None:
            scaler = eval(f'preprocessing.{scaler}()')
            data_scaled = scaler.fit_transform(data.iloc[:, to_skip:])
        else:
            data_scaled = data

        # Perform GMM clustering
        gmm_models = []
        n_components = params['n_components']
        for n_component in n_components:
            print(f'Fitting GMM with {n_component} components.')
            gmm_models.append(GaussianMixture(n_components=n_component, 
                covariance_type=params['covariance_type'], 
                max_iter=params['max_iter'], 
                n_init=params['n_init'], 
                random_state=n_component).fit(data_scaled))

        # Compute AIC scores to select best model
        aic = []
        for model in gmm_models:
            aic.append(model.aic(data_scaled))
        best_gmm_model = gmm_models[np.argmin(aic)]
        print(f'\nBest model has {n_components[np.argmin(aic)]} components.\n')

        # Plot AIC scores
        plt.figure(figsize=(15, 10))
        plt.plot(n_components, aic, 'o', label='AIC')
        plt.xlabel('number components')
        plt.ylabel('AIC score')
        plt.legend()
        plt.show()

        # Plot summary
        self.summary_clustering(pd.DataFrame(data_scaled, columns=self.data.columns[to_skip:]), 
            best_gmm_model.predict(data_scaled), 'rate')

        return best_gmm_model

    def summary_clustering(self, data, predictions, feature):
        """
        Perform analyses on obtained clusters.

        Parameters
        ----------
        data : DataFrame
            Pandas DataFrame containing molecular descriptors (scaled data).
        predictions : array
            Array resulting from the application of `predict` using the best GMM model.
        feature : string
            Name of the column if the dataset containing the feature of interest.

        Returns
        -------
        None
        """

        # Heat-map clusters-descriptors
        plt.figure(figsize=(data.shape[1]/3, 10))
        sns.heatmap(data.groupby(list(predictions)).agg('mean').T, 
            cmap='YlGnBu')
        plt.xlabel('clusters')
        plt.ylabel('descriptors')
        plt.show()

        # Scatter plot for clusters-descriptors
        descriptors_means = []
        descriptors_stds  = []
        for idx in range(len(np.unique(predictions))):
            tmp_mean = self.data.loc[predictions==idx][feature].mean()
            tmp_std  = self.data.loc[predictions==idx][feature].std()
            descriptors_means.append(tmp_mean)
            descriptors_stds.append(tmp_std)

        plt.figure(figsize=(15, 10))
        plt.errorbar(list(np.unique(predictions)), descriptors_means, 
            yerr=descriptors_stds, fmt='o')
        plt.xlabel('cluster')
        plt.ylabel(f'<{feature}>')
        plt.title(f'{feature} mean value for the found clusters.')
        plt.show()

########################################################

from sklearn.base import BaseEstimator, TransformerMixin

class IdentityTransformer(BaseEstimator, TransformerMixin):

    def __init__(self):
        pass

    def fit(self, input_array, y=None):
        return self

    def transform(self, input_array, y=None):
        return input_array*1
