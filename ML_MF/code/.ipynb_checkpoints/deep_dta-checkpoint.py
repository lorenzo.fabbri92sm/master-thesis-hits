"""Keras (TensorFlow as back-end) implementation of DeepDTA: https://arxiv.org/abs/1801.10193.
To run this script, check the cell on the associated Jupyter notebook (dl.ipynb).
It takes as input (1) the path to the dataset and (2) the name to store the fitted model.
Before running this code, you must change the path to where the trained models will be saved."""

import numpy as np
import pandas as pd
import random
import matplotlib.pyplot as plt

from sklearn.utils import shuffle
from sklearn.metrics import r2_score
import tensorflow as tf
import keras
from keras import backend as K
from keras import optimizers
from keras.models import Model, Sequential
from keras.layers import Dense, Dropout, Embedding, Conv1D
from keras.layers import GlobalMaxPooling1D, Concatenate, Input

from rdkit import Chem

import sys


##################################################
def q2f3(y_train, y_test, y_pred):
    # Evaluation metric for regression problems.

    # Reshape otherwise this idiot does stupid things
    y_train = y_train.reshape(-1, 1)
    y_test = y_test.reshape(-1, 1)
    y_pred = y_pred.reshape(-1, 1)

    num = np.sum((y_test-y_pred)**2)
    den = np.sum((y_train-np.mean(y_train))**2)

    num = num/len(y_test)
    den = den/len(y_train)
    frac = num/den

    return (1-frac)
##################################################


##################################################
#path = '../data/kiba/kiba.csv'
path = str(sys.argv[1])
data = pd.read_csv(path)
data = shuffle(data)
data.reset_index(inplace=True, drop=True)

name_model = str(sys.argv[2])

# Training parameters
batch_size = 128
epochs = 200
train = 0.8  # Percentage for training set
use_desc = False  # Whether to also use additional descriptors
##################################################


##################################################
# The maximum lengths allowed for protein sequences and compounds, respectively.
# This means that even if a protein sequences is longer than MAX_LEN_P,
# it will be truncated. Check the DeepDTA paper for more details
MAX_LEN_P = 1000
MAX_LEN_C = 100

# Lists to store protein sequences, compound SMILES, 
# molecular descriptors and the target variable we want to predict
XP = []
XC = []
desc = []
Y = []

# Dictionaries mapping each character in protein sequences and compound SMILES
# to integers. Check the DeepDTA paper for more details
dict_p = { "A": 1, "C": 2, "B": 3, "E": 4, "D": 5, "G": 6,
	"F": 7, "I": 8, "H": 9, "K": 10, "M": 11, "L": 12,
	"O": 13, "N": 14, "Q": 15, "P": 16, "S": 17, "R": 18,
	"U": 19, "T": 20, "W": 21,
	"V": 22, "Y": 23, "X": 24,
	"Z": 25 }
dict_c = {"#": 29, "%": 30, ")": 31, "(": 1, "+": 32, "-": 33, "/": 34, ".": 2,
	"1": 35, "0": 3, "3": 36, "2": 4, "5": 37, "4": 5, "7": 38, "6": 6,
	"9": 39, "8": 7, "=": 40, "A": 41, "@": 8, "C": 42, "B": 9, "E": 43,
	"D": 10, "G": 44, "F": 11, "I": 45, "H": 12, "K": 46, "M": 47, "L": 13,
	"O": 48, "N": 14, "P": 15, "S": 49, "R": 16, "U": 50, "T": 17, "W": 51,
	"V": 18, "Y": 52, "[": 53, "Z": 19, "]": 54, "\\": 20, "a": 55, "c": 56,
	"b": 21, "e": 57, "d": 22, "g": 58, "f": 23, "i": 59, "h": 24, "m": 60,
	"l": 25, "o": 61, "n": 26, "s": 62, "r": 27, "u": 63, "t": 28, "y": 64}

def enc_seq(seq, dict_p):
    # Function to encode protein sequences
    
	res = np.zeros((MAX_LEN_P))

	for i, aa in enumerate(seq[:MAX_LEN_P]):
		res[i] = dict_p[aa]

	return res

def enc_comp(smi, dict_c):
    # Function to encode compound SMILES
    
	res = np.zeros((MAX_LEN_C))

	for i, aa in enumerate(smi[:MAX_LEN_C]):
		res[i] = dict_c[aa]

	return res

# Loop over all the complexes in dataset. Encode sequences and SMILES,
# append results to lists
for idx in data.index:
    
    try:
        seq = enc_seq(data.loc[data.index[idx], 'seq'], dict_p)

        smiles = data.loc[data.index[idx], 'smiles']
        smiles = Chem.MolToSmiles(Chem.MolFromSmiles(smiles), canonical=True)
        comp = enc_comp(smiles, dict_c)

        XP.append(seq)
        XC.append(comp)
        desc.append(np.asarray(data.iloc[idx, 5:]))
        Y.append(data.loc[data.index[idx], 'val'])
        
    except:
        continue

XP = np.asarray(XP)
XC = np.asarray(XC)
desc = np.asarray(desc)
Y = np.asarray(Y)
##################################################


##################################################
# Initialize TensorFlow
session_conf = tf.ConfigProto(intra_op_parallelism_threads=1,
	inter_op_parallelism_threads=1)
sess = tf.Session(graph=tf.get_default_graph(),
	config=session_conf)
K.set_session(sess)

# Variables that are inputs to the network
XP_input = Input(shape=(MAX_LEN_P,), dtype='int32')
XC_input = Input(shape=(MAX_LEN_C,), dtype='int32')
DESC = Input(shape=(512,), dtype='float32')

NUM_FILTERS = 16  # Number of filters for the CNNs
reg = 0.001  # Regularization value for L2 regularization

# Whether to use a kernel regularizer (in which case which one) or not (None)
kernel_regularizer = keras.regularizers.l2(reg)
#kernel_regularizer = None

# CNN for proteins
K_SIZE_P = 8  # Window size for convolutional filter
seq = Embedding(input_dim=26, output_dim=128, input_length=1000)(XP_input)
seq = Conv1D(filters=NUM_FILTERS, kernel_size=K_SIZE_P, activation='relu',
	padding='valid', strides=1, kernel_regularizer=kernel_regularizer)(seq)
seq = Conv1D(filters=NUM_FILTERS*2, kernel_size=K_SIZE_P, activation='relu',
	padding='valid', strides=1, kernel_regularizer=kernel_regularizer)(seq)
seq = Conv1D(filters=NUM_FILTERS*3, kernel_size=K_SIZE_P, activation='relu',
	padding='valid', strides=1, kernel_regularizer=kernel_regularizer)(seq)
seq = GlobalMaxPooling1D()(seq)

# CNN for compounds
K_SIZE_C = 8  # Window size for convolutional filter
comp = Embedding(input_dim=65, output_dim=128, input_length=100)(XC_input)
comp = Conv1D(filters=NUM_FILTERS, kernel_size=K_SIZE_C, activation='relu',
	padding='valid', strides=1, kernel_regularizer=kernel_regularizer)(comp)
comp = Conv1D(filters=NUM_FILTERS*2, kernel_size=K_SIZE_C, activation='relu',
	padding='valid', strides=1, kernel_regularizer=kernel_regularizer)(comp)
comp = Conv1D(filters=NUM_FILTERS*3, kernel_size=K_SIZE_C, activation='relu',
	padding='valid', strides=1, kernel_regularizer=kernel_regularizer)(comp)
comp = GlobalMaxPooling1D()(comp)

# If using also additional descriptors, must add input to model
if use_desc:
    interaction = keras.layers.concatenate([seq, comp, DESC], axis=-1)
else:
    interaction = keras.layers.concatenate([seq, comp], axis=-1)

# FC layers
drop = 0.2  # Dropout probability
nodes1 = 256  # Number of nodes for FC1 and FC@
nodes2 = int(nodes1 / 2)  # Number of nodes for FC3
FC1 = Dense(nodes1, activation='relu', 
           kernel_regularizer=kernel_regularizer)(interaction)
FC2 = Dropout(drop)(FC1)
FC2 = Dense(nodes1, activation='relu', 
           kernel_regularizer=kernel_regularizer)(FC2)
FC3 = Dropout(drop)(FC2)
FC3 = Dense(nodes2, activation='relu', 
           kernel_regularizer=kernel_regularizer)(FC3)
FC3 = Dropout(drop)(FC3)

#predictions = Dense(1, kernel_initializer='normal')(FC3)
predictions = Dense(1)(FC3)

# If using also additional descriptors, must add input to model
if use_desc:
    model = Model(inputs=[XP_input, XC_input, DESC], outputs=[predictions])
else:
    model = Model(inputs=[XP_input, XC_input], outputs=[predictions])
model.compile(optimizer='adam', loss='mean_squared_error')
print(model.summary())
##################################################


##################################################
# Define training set size
train_size = int(len(Y) * train)

# If using also additional descriptors, must add input to model.
# While training, it outputs also information about the validation set (if provided)
if use_desc:
    res = model.fit(([XP[:train_size], XC[:train_size], desc[:train_size]]), 
                    Y[:train_size], batch_size=batch_size, epochs=epochs, 
                   validation_data=([XP[train_size:], XC[train_size:], desc[train_size:]], 
                                   Y[train_size:]))
else:
    res = model.fit(([XP[:train_size], XC[:train_size]]), 
                    Y[:train_size], batch_size=batch_size, epochs=epochs, 
                   validation_data=([XP[train_size:], XC[train_size:]], 
                                   Y[train_size:]))

# Plot training and validation losses to check for overfitting
training_loss = res.history['loss']
validation_loss = res.history['val_loss']
epochs_tr = range(1, len(training_loss)+1)
epochs_val = range(1, len(validation_loss)+1)

fig, ax = plt.subplots(1, 1, figsize=(15, 7))
plt.plot(epochs_tr, training_loss, '--', 
        label='training')
plt.plot(epochs_val, validation_loss, '--', 
        label='validation')
plt.xlabel('epochs')
plt.ylabel('loss')
plt.legend()
plt.show()

# Save the model for transfer learning
model.save(f'/hits/fast/mcm/fabbrilo/ML_MF/code/models/{name_model}.h5')

# If using also additional descriptors, must add input to model.
if use_desc:
    preds = model.predict(([XP[train_size:], XC[train_size:], desc[train_size:]]))
else:
    preds = model.predict(([XP[train_size:], XC[train_size:]]))
preds = preds.flatten()

# Model evaluation: R2, Q2, MSE and plot of true vs. predicted values
mse = 1/len(preds) * np.sum((preds-Y[train_size:])**2)
r2 = r2_score(Y[train_size:], preds)
q2 = q2f3(Y[:train_size], Y[train_size:], preds)
print(f'\nMSE test set: {mse}.')
print(f'R2 test set: {r2}.')
print(f'Q2 test set: {np.round(q2, 2)}.')

fig, ax = plt.subplots(1, 1, figsize=(15, 7))
plt.plot(Y[train_size:], preds, 'o')
plt.plot(list(range(19)), list(range(19)), '--')
plt.show()
##################################################
