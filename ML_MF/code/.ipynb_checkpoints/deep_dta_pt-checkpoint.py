# PyTorch implementation of DeepDTA. Cannot guarantee it works properly, though

##################################################
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

from rdkit import Chem

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import DataLoader, TensorDataset, Dataset

from sklearn.model_selection import KFold, train_test_split
from sklearn.utils import shuffle

import random
##################################################


##################################################
sample = False
sample_size = 5000
path = '../data/kiba/kiba.csv'

data = pd.read_csv(path)
data = shuffle(data)
data.reset_index(inplace=True, drop=True)

if sample:
    data = data.sample(n=sample_size)
    data.reset_index(inplace=True, drop=True)
##################################################


##################################################
MAX_LEN_P = 1000
MAX_LEN_C = 100
XP = []
XC = []
Y = []

dict_p = { "A": 1, "C": 2, "B": 3, "E": 4, "D": 5, "G": 6,
	"F": 7, "I": 8, "H": 9, "K": 10, "M": 11, "L": 12,
	"O": 13, "N": 14, "Q": 15, "P": 16, "S": 17, "R": 18,
	"U": 19, "T": 20, "W": 21,
	"V": 22, "Y": 23, "X": 24,
	"Z": 25 }
dict_c = {"#": 29, "%": 30, ")": 31, "(": 1, "+": 32, "-": 33, "/": 34, ".": 2,
	"1": 35, "0": 3, "3": 36, "2": 4, "5": 37, "4": 5, "7": 38, "6": 6,
	"9": 39, "8": 7, "=": 40, "A": 41, "@": 8, "C": 42, "B": 9, "E": 43,
	"D": 10, "G": 44, "F": 11, "I": 45, "H": 12, "K": 46, "M": 47, "L": 13,
	"O": 48, "N": 14, "P": 15, "S": 49, "R": 16, "U": 50, "T": 17, "W": 51,
	"V": 18, "Y": 52, "[": 53, "Z": 19, "]": 54, "\\": 20, "a": 55, "c": 56,
	"b": 21, "e": 57, "d": 22, "g": 58, "f": 23, "i": 59, "h": 24, "m": 60,
	"l": 25, "o": 61, "n": 26, "s": 62, "r": 27, "u": 63, "t": 28, "y": 64}

def enc_seq(seq, dict_p):
	res = np.zeros((MAX_LEN_P))

	for i, aa in enumerate(seq[:MAX_LEN_P]):
		res[i] = dict_p[aa]

	return res

def enc_comp(smi, dict_c):
	res = np.zeros((MAX_LEN_C))

	for i, aa in enumerate(smi[:MAX_LEN_C]):
		res[i] = dict_c[aa]

	return res

for idx in data.index:
	Y.append(np.array(data.loc[data.index[idx], 'val']))

	seq = enc_seq(data.loc[data.index[idx], 'seq'], dict_p)
	XP.append(seq)

	smiles = data.loc[data.index[idx], 'smiles']
	smiles = Chem.MolToSmiles(Chem.MolFromSmiles(smiles), canonical=True)
	comp = enc_comp(smiles, dict_c)
	XC.append(comp)
##################################################


##################################################
Xp_train, Xp_test, yp_train, yp_test = train_test_split(XP, Y, 
                                                        test_size=0.3, shuffle=False)
Xc_train, Xc_test, yc_train, yc_test = train_test_split(XC, Y, 
                                                        test_size=0.3, shuffle=False)

class Data(Dataset):
    
    def __init__(self, dataP, targetP, dataC, targetC, transform=None):
        self.dataP = [torch.from_numpy(X).int() for X in dataP]
        self.targetP = [torch.from_numpy(y).float() for y in targetP]
        
        self.dataC = [torch.from_numpy(X).int() for X in dataC]
        self.targetC = [torch.from_numpy(y).float() for y in targetC]
        
        self.transform = transform
    
    def __getitem__(self, index):
        Xp = self.dataP[index]
        yp = self.targetP[index]
        
        Xc = self.dataC[index]
        yc = self.targetC[index]
        
        if self.transform:
            Xp = self.transform(Xp)
            Xc = self.transform(Xc)
            
        return Xp, yp, Xc, yc
    
    def __len__(self):
        return len(self.dataP)

device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
batch_size = 256

# Training
dataset_train = Data(Xp_train, yp_train, 
                    Xc_train, yc_train)
loader_train = DataLoader(
    dataset_train, 
    batch_size=batch_size, 
    shuffle=True, 
    num_workers=2, 
    pin_memory=device)

# Test
dataset_test = Data(Xp_test, yp_test, 
                    Xc_test, yc_test)
loader_test = DataLoader(
    dataset_test, 
    batch_size=batch_size, 
    shuffle=True, 
    num_workers=2, 
    pin_memory=device)
##################################################


##################################################
class ConvNet(nn.Module):
    
    def __init__(self):
        super(ConvNet, self).__init__()
        
        n_filters = 32
        k_size = 8
        
        # Embedding
        emb_size = 128
        self.embeddingP = nn.Embedding(25, emb_size)
        self.embeddingC = nn.Embedding(64, emb_size)
        
        # Convolutional layer 1
        self.conv1 = nn.Conv1d(in_channels=emb_size, out_channels=n_filters, 
                               kernel_size=k_size, stride=1, padding=0)
        nn.init.xavier_uniform_(self.conv1.weight)
        # Convolutional layer 2
        self.conv2 = nn.Conv1d(in_channels=n_filters, out_channels=n_filters*2, 
                               kernel_size=k_size, stride=1, padding=0)
        nn.init.xavier_uniform_(self.conv2.weight)
        # Convolutional layer 3
        self.conv3 = nn.Conv1d(in_channels=n_filters*2, out_channels=n_filters*3, 
                               kernel_size=k_size, stride=1, padding=0)
        nn.init.xavier_uniform_(self.conv3.weight)
        # Pooling layer
        self.poolP = nn.AdaptiveMaxPool1d(986)
        self.poolC = nn.AdaptiveMaxPool1d(86)
        # Activation
        self.act = nn.ReLU()
        
        # Fully-connected layers
        self.drop_out = nn.Dropout(p=0.1)
        self.fc1 = nn.Linear(in_features=102912, out_features=1024)
        nn.init.xavier_uniform_(self.fc1.weight)
        self.fc2 = nn.Linear(in_features=1024, out_features=512)
        nn.init.xavier_uniform_(self.fc2.weight)
        self.fc3 = nn.Linear(in_features=512, out_features=1)
        nn.init.xavier_uniform_(self.fc3.weight)
    
    def forward(self, prot, mol):
        
        out_prot = self.embeddingP(prot)
        out_prot = out_prot.view(out_prot.size(0), -1, out_prot.size(2))
        out_prot = self.act(self.conv1(out_prot))
        out_prot = self.act(self.conv2(out_prot))
        out_prot = self.poolP(self.act(self.conv3(out_prot)))
        
        out_mol = self.embeddingC(mol)
        out_mol = out_mol.view(out_mol.size(0), -1, out_mol.size(2))
        out_mol = self.act(self.conv1(out_mol))
        out_mol = self.act(self.conv2(out_mol))
        out_mol = self.poolC(self.act(self.conv3(out_mol)))
        
        out = torch.cat((out_prot.view(out_prot.size(0), -1), 
                        out_mol.view(out_mol.size(0), -1)), dim=1)
        #print(out.size())
        out = out.view(-1, 102912)
        
        out = self.drop_out(out)
        out = F.relu(self.fc1(out))
        out = self.drop_out(out)
        out = F.relu(self.fc2(out))
        out = self.drop_out(out)
        out = self.fc3(out)
        
        return out

model = ConvNet().to(device)
print('--------------------------------------------------')
print(model)
print('--------------------------------------------------')
print('\n')

criterion = nn.MSELoss()

lr = 0.001
optimizer = torch.optim.Adam(model.parameters(), lr=lr)
##################################################


##################################################
num_epochs = 50

true = []
preds = []

print('--------------------------------------------------')
print('TRAINING...')
print('--------------------------------------------------')

for epoch in range(num_epochs):
    
    model.train()
    for batch, data in enumerate(loader_train):
    
        Xp, yp = data[0], data[1]
        Xc, yc = data[2], data[3]
        
        # Proteins
        Xp = Xp.view(Xp.size(0), -1, Xp.size(1))
        Xp = Xp.to(device, dtype=torch.long)
        
        # Compounds
        Xc = Xc.view(Xc.size(0), -1, Xc.size(1))
        Xc = Xc.to(device, dtype=torch.long)
        
        # Target (same for protein and compound)
        y = yc.view(-1, 1)
        y = y.to(device, dtype=torch.float)
        
        # Forward
        output = model(Xp, Xc)
        loss = criterion(output, y)
        
        # Backward and optimization
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        
        # Store results
        true.extend(y.cpu().detach().numpy().flatten())
        preds.extend(output.cpu().detach().numpy().flatten())
        
        if (batch+1)%100 == 0:
            print(f'Epoch {epoch+1}/{num_epochs}; Loss {loss.item()}.')

torch.save(model.state_dict(), './models/deepDTA_p.pth')
##################################################


##################################################
true = []
preds = []

print('\n')
print('--------------------------------------------------')
print('TEST...')
print('--------------------------------------------------')

model.eval()
with torch.no_grad():
    
    #for data in zip(loader_p_test, loader_c_test):
    for batch, data in enumerate(loader_test):
        
        Xp, yp = data[0], data[1]
        Xc, yc = data[2], data[3]
        
        # Proteins
        Xp = Xp.view(Xp.size(0), -1, Xp.size(1))
        Xp = Xp.to(device, dtype=torch.long)
        
        # Compounds
        Xc = Xc.view(Xc.size(0), -1, Xc.size(1))
        Xc = Xc.to(device, dtype=torch.long)
        
        # Target (same for protein and compound)
        y = yc.view(-1, 1)
        y = y.to(device, dtype=torch.float)
        
        # Forward
        output = model(Xp, Xc)
        loss = criterion(output, y)
        
        # Store results
        true.extend(y.cpu().numpy().flatten())
        preds.extend(output.cpu().numpy().flatten())
        
        if (batch+1)%100 == 0:
            print(f'Epoch {epoch+1}/{num_epochs}; Loss {loss.item()}.')
##################################################


##################################################
fig, ax = plt.subplots(1, 1, figsize=(15, 7))
sns.scatterplot(true, preds, ax=ax)
plt.plot(list(range(18)), list(range(18)), '--')
plt.xlabel('true')
plt.ylabel('predictions')
plt.show()
##################################################
