"""
This file contains a script that generates a number of molecular features for a number of given 
compounds.
"""

__author__    = "Lorenzo Fabbri"
__mantainer__ = "Lorenzo Fabbri"
__email__     = "lorenzo.fabbri@h-its.org"


import os
import numpy as np

import pandas as pd
import tabula

import pubchempy as pcp
from rdkit import Chem
from rdkit.Chem import Descriptors

from Bio.SeqUtils.ProtParam import ProteinAnalysis


class Features:
	"""
	Main class containing functions to read input files, generate matrix of features 
	and generate output files.
	"""

	def __init__(self):

		self.data = None # Input data
		self.identifiers = [] # List containing identifiers compounds
		self.features = pd.DataFrame() # Pandas DataFrame containing features
		self.compounds = [] # List containing compounds

		print('If you do not need to get data from a pdf file, you must add self.data and self.identifiers yourself.')

	def read_pdf(self, path, pages, coln_identifiers, guess=False, coln_names=None):
		"""
		Function to read in pdf file with compound identifiers and/or compound features. To be used 
		with a lot of care. If data is already available, user must add `data_pdf` and `identifiers` 
		to class instantiation.

		Parameters
		----------
		path : string
			Path to the input file in pdf format
		pages : string
			Pages containing the desired table(s)
		coln_identifiers : string
			Name of the column containing the desired identifiers
		guess : boolean
			Whether to guess structure table. Default to False
		coln_names : list
			List of strings to use as column names

		Returns
		-------
		None
		"""

		# Read in pdf file
		df = tabula.read_pdf(path, pages=pages, guess=guess)

		# If table is in multiple pages, remove header if present inside table
		for name in df.columns:
			df = df[df[name] != name]

		if (coln_names is not None):
			df.columns = coln_names

		self.data = df
		# Drop NaNs and duplicated rows (by identifier)
		self.data.dropna(inplace=True)
		self.data.drop_duplicates(subset=[coln_identifiers], keep='first', inplace=True)
		self.data.reset_index(inplace=True, drop=True)

		self.identifiers = list(self.data[coln_identifiers])

		print(f'Data has shape {self.data.shape}.\n')
		print(df.head())
		print('\n')
		print(f'There are {len(np.unique(self.identifiers))} unique identifiers in the input file.')
		print(f'There are {len(self.identifiers)} identifiers in the input file.')

	def get_compounds(self, identifier):
		"""
		Function to acquire compounds given some form of identifier.

		Parameters
		----------
		identifier : string
			Identifier to use for the PubChemPy library function `get_compounds`

		Returns
		-------
		None
		"""

		# Add column to `data` for compounds
		self.data['compound'] = np.nan

		# Iterate over compounds identifiers and interrogate PubChem
		for idx, compound in enumerate(self.identifiers):
			c = pcp.get_compounds(compound, identifier)
			try:
				self.data.loc[self.data.index[idx], 'compound'] = c[0]

			# It is possible that a compound is not found. In this case it is dropped
			except IndexError:
				print(f'Compound {compound} was not found and it is going to be dropped.')

		# Remove identifiers not found, clean up data and create list with all compounds found
		idx_nan = list(self.data['compound'].index[self.data['compound'].apply(pd.isnull)])
		self.identifiers = [self.identifiers[i] for i in self.data.index if i not in idx_nan]
		self.data.dropna(inplace=True)
		self.data.reset_index(inplace=True, drop=True)
		self.compounds = list(self.data['compound'])

		print(f'\nData has shape {self.data.shape}.\n')
		print(f'There are {len(self.identifiers)} identifiers in the input file.\n')
		print(f'There are {len(self.compounds)} compounds in the input file.')

	def generate_mf(self, methods_features):
		"""
		Function to call other functions to generate molecular features from a given 
		list of libraries.

		Parameters
		----------
		methods_features : dictionary
			Dictionary with keys being the methods to be used to compute the MFs and 
			values being the features to be computed

		Returns
		-------
		None
		"""

		if ('mf_pcp' not in methods_features.keys()):
			print('PCP must be used in order to generate the Smiles.\n')
			return

		# Add compounds names to `features` DataFrame
		self.features['identifier'] = self.identifiers
		self.features['ligand'] = self.data['ligand']
		self.features['ligand_obj']   = self.compounds # PCP objects for ligands

		for method, features in methods_features.items():
			getattr(self, method)(features)

	def mf_pcp(self, features):
		"""
		Function to compute given features using the Python library `pubchempy`.

		Parameters
		----------
		features : list
			List of strings representing the features to be computed

		Returns
		-------
		None
		"""

		# Loop over input features and compute them
		for feature in features:
			print(f'Computing {feature}...')
			for idx, compound in enumerate(self.compounds):
				self.features.loc[self.features.index[idx], feature+'_lig'] = eval(f'compound.{feature}')

	def mf_rdkit(self, features):
		"""
		Function to compute given features using the Python library `RDKit`.

		Parameters
		----------
		features : list
			List of strings representing the features to be computed

		Returns
		-------
		None
		"""

		# Loop over input features and compute them
		for feature in features:
			print(f'Computing {feature}...')
			for idx, compound in enumerate(self.compounds):
				# Get Smiles molecules
				mol = Chem.MolFromSmiles(self.features.iloc[idx]['canonical_smiles_lig'])

				self.features.loc[self.features.index[idx], feature+'_lig'] = eval(f'Descriptors.{feature}(mol)')

	def prot_mf_bp(self, df, coln):
		"""
		Function to generate molecular features from protein sequences using BioPython's module 
		`ProteinAnalysis`.

		Parameters
		----------
		df : DataFrame
			Pandas DataFrame containing, at least, a column with the protein sequences
		coln : string
			Name of the column in df containing protein sequences to use for features generation

		Returns
		-------
		None
		"""

		# Iterate over all sequences and generate features
		for idx in df.index:
			seq = ProteinAnalysis(df.loc[df.index[idx], coln].replace('X', ''))

			# Compute features
			df.at[idx, 'aromaticity_mol']                    = seq.aromaticity()
			df.at[idx, 'isoelectric_point_mol']              = seq.isoelectric_point()
			df.at[idx, 'mean_flexibility_mol']               = np.mean(seq.flexibility())
			df.at[idx, 'gravy_mol']                          = seq.gravy()
			df.at[idx, 'instability_index_mol']              = seq.instability_index()
			df.at[idx, 'molar_extinction_coefficient_0_mol'] = seq.molar_extinction_coefficient()[0]
			df.at[idx, 'molar_extinction_coefficient_1_mol'] = seq.molar_extinction_coefficient()[1]
			df.at[idx, 'molecular_weight_mol']               = seq.molecular_weight()
			df.at[idx, 'secondary_structure_fraction_0_mol'] = seq.secondary_structure_fraction()[0]
			df.at[idx, 'secondary_structure_fraction_1_mol'] = seq.secondary_structure_fraction()[1]
			df.at[idx, 'secondary_structure_fraction_2_mol'] = seq.secondary_structure_fraction()[2]

			# aa_perc = seq.get_amino_acids_percent()
			# for aa, perc in zip(aa_perc.keys(), aa_perc.values()):
			# 	df.at[idx, aa] = perc

	def clean_features(self):
		"""
		Function to clean up matrix of features. At the moment it simply drops columns 
		containing identical values (e.g., all 1's).

		Parameters
		----------
		None

		Returns
		-------
		None
		"""

		# First two columns always to be kept
		idx = [True, True]

		print(f'Matrix of features has shape {self.features.shape}.\n')

		# Remove columns containing just 0's
		colns = idx+list((np.array((self.features.iloc[:, 2:] != 0).any(axis=0))))
		self.features = self.features.loc[:, colns]

		# Remove columns containing same value
		colns = list((self.features != self.features.iloc[0]).any(axis=0))
		self.features = self.features.loc[:, colns]

		print(f'\nMatrix of features now has shape {self.features.shape}.')

	def save_features(self, path):
		"""
		Function to save to file computed features.

		Parameters
		----------
		path : string
			Path with the name of the file to be saved

		Returns
		-------
		None
		"""

		self.features.to_csv(path)
