from pymol import cmd,stored

set depth_cue, 1
set fog_start, 0.4

set_color b_col, [36,36,85]
set_color t_col, [10,10,10]
set bg_rgb_bottom, b_col
set bg_rgb_top, t_col      
set bg_gradient

set  spec_power  =  200
set  spec_refl   =  0

load data/3txo.pdb, protein
create ligands, protein and organic
select xlig, protein and organic
delete xlig

hide everything, all

color white, elem c
color bluewhite, protein
#show_as cartoon, protein
show surface, protein
#set transparency, 0.15

show sticks, ligands
set stick_color, magenta

load data/3txo.pdb_points.pdb.gz, points
hide nonbonded, points
show nb_spheres, points
set sphere_scale, 0.2, points
cmd.spectrum("b", "green_red", selection="points", minimum=0, maximum=0.7)


stored.list=[]
cmd.iterate("(resn STP)","stored.list.append(resi)")    # read info about residues STP
lastSTP=stored.list[-1] # get the index of the last residue
hide lines, resn STP

cmd.select("rest", "resn STP and resi 0")

for my_index in range(1,int(lastSTP)+1): cmd.select("pocket"+str(my_index), "resn STP and resi "+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.show("spheres","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_scale","0.4","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_transparency","0.1","pocket"+str(my_index))



set_color pcol1 = [0.361,0.576,0.902]
select surf_pocket1, protein and id [1070,708,1094,1097,656,1096,1174,657,133,134,236,684,681,519,661,678,2300,2296,2298,2299,77,78,1057,1058,1081,89,111,113,115,1182,83,2332,2304,2306,114,135,112,250,1175,1179,1176,1180,721,1071,1072,1073,722,762,707,725,726,729,731] 
set surface_color,  pcol1, surf_pocket1 
set_color pcol2 = [0.278,0.322,0.702]
select surf_pocket2, protein and id [1439,1045,1051,1054,1464,1494,1495,1496,1440,1397,1398,1549,1552,1537,1548,1650,1652,1459,1457,1458,1426,1427,1895,1428,1429,1430,1431,1651] 
set surface_color,  pcol2, surf_pocket2 
set_color pcol3 = [0.467,0.361,0.902]
select surf_pocket3, protein and id [949,939,940,945,946,941,943,944,1965,2114,922,486,921,923,492,2112,2113,2115,2111,951,953,484,493,1966,2076,1986,1987] 
set surface_color,  pcol3, surf_pocket3 
set_color pcol4 = [0.490,0.278,0.702]
select surf_pocket4, protein and id [370,348,349,1205,371,306,345,346,347,333,1181,1195,1196,1197,266,377,1204,106,109,267,101,102,105,1182,103] 
set surface_color,  pcol4, surf_pocket4 
set_color pcol5 = [0.792,0.361,0.902]
select surf_pocket5, protein and id [174,16,173,198,1,2,27,15,26,228,546,552,553,2580,545,2585,547,2581,564,565,567] 
set surface_color,  pcol5, surf_pocket5 
set_color pcol6 = [0.702,0.278,0.659]
select surf_pocket6, protein and id [629,7,8,570,24,47,242,627,46,2389,586,631] 
set surface_color,  pcol6, surf_pocket6 
set_color pcol7 = [0.902,0.361,0.682]
select surf_pocket7, protein and id [967,1244,1245,1360,1369,1953,1954,1955,1956,966,968,974,1388,1392,1385,1386,1387,1909,1948,1945,1916,1371,1911,1914,1915,1921,1923,1373] 
set surface_color,  pcol7, surf_pocket7 
set_color pcol8 = [0.702,0.278,0.408]
select surf_pocket8, protein and id [258,609,610,2390,275,2384,2385,128,117,118,120,124,93,94,126,256,276] 
set surface_color,  pcol8, surf_pocket8 
set_color pcol9 = [0.902,0.361,0.361]
select surf_pocket9, protein and id [734,735,770,771,744,2288,2213,2214,742,2206,2210,2212,767,768,769,2187,2278,2280] 
set surface_color,  pcol9, surf_pocket9 
set_color pcol10 = [0.702,0.408,0.278]
select surf_pocket10, protein and id [679,1113,1131,2134,1110,1121,1123,665,670,525,480,1161,488,489,490,2132,2133,491] 
set surface_color,  pcol10, surf_pocket10 
set_color pcol11 = [0.902,0.682,0.361]
select surf_pocket11, protein and id [1821,2008,2015,1820,1899,1904,2006,1970,1898,1935,1934,1971,1936] 
set surface_color,  pcol11, surf_pocket11 
set_color pcol12 = [0.702,0.659,0.278]
select surf_pocket12, protein and id [2061,2063,2050,2153,2152,2054,860,861,864,854,855,831,866,2106] 
set surface_color,  pcol12, surf_pocket12 


deselect

orient
