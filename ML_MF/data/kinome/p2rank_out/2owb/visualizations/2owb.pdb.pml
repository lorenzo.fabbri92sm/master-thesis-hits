from pymol import cmd,stored

set depth_cue, 1
set fog_start, 0.4

set_color b_col, [36,36,85]
set_color t_col, [10,10,10]
set bg_rgb_bottom, b_col
set bg_rgb_top, t_col      
set bg_gradient

set  spec_power  =  200
set  spec_refl   =  0

load data/2owb.pdb, protein
create ligands, protein and organic
select xlig, protein and organic
delete xlig

hide everything, all

color white, elem c
color bluewhite, protein
#show_as cartoon, protein
show surface, protein
#set transparency, 0.15

show sticks, ligands
set stick_color, magenta

load data/2owb.pdb_points.pdb.gz, points
hide nonbonded, points
show nb_spheres, points
set sphere_scale, 0.2, points
cmd.spectrum("b", "green_red", selection="points", minimum=0, maximum=0.7)


stored.list=[]
cmd.iterate("(resn STP)","stored.list.append(resi)")    # read info about residues STP
lastSTP=stored.list[-1] # get the index of the last residue
hide lines, resn STP

cmd.select("rest", "resn STP and resi 0")

for my_index in range(1,int(lastSTP)+1): cmd.select("pocket"+str(my_index), "resn STP and resi "+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.show("spheres","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_scale","0.4","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_transparency","0.1","pocket"+str(my_index))



set_color pcol1 = [0.361,0.576,0.902]
select surf_pocket1, protein and id [1314,1315,1316,1329,1330,1331,491,493,229,461,465,495,1222,1223,1225,827,828,1199,1206,1224,1226,1310,1313,206,205,212,213,214,215,216,217,218,220,224,225,226,227,228,230,816,817,851,852,853,813,819,1186,1187,1169,1464,1467,1337,1338,1339,1457,1461,1197,844,1332,1221,765,249,250,363,364,365,366,231,235,362,380,502,418,200,201,202,197,196,198,193,194,192,195,788,789,784,787,1306,1309,555,557,630,525,526,490,503,489,496,497,500,457,631,353,763,770] 
set surface_color,  pcol1, surf_pocket1 
set_color pcol2 = [0.302,0.278,0.702]
select surf_pocket2, protein and id [1196,1197,1194,1484,1185,1186,1187,1485,843,844,1700,1741,1742,871,1770,1796,1494,1665,1750,1496,1487,1492,1481,1861,1751,1753,1762,1763,1821,1822,1767,1755,1758] 
set surface_color,  pcol2, surf_pocket2 
set_color pcol3 = [0.631,0.361,0.902]
select surf_pocket3, protein and id [1590,1591,1593,1601,1602,1603,1605,2107,2110,2111,2112,2113,2102,1089,1090,1091,1097,1098,1596,1595,1395,2119,2126,2127,1598,1627,1093] 
set surface_color,  pcol3, surf_pocket3 
set_color pcol4 = [0.678,0.278,0.702]
select surf_pocket4, protein and id [119,120,650,100,101,77,651,658,329,348,144,145,758,679,681,291] 
set surface_color,  pcol4, surf_pocket4 
set_color pcol5 = [0.902,0.361,0.682]
select surf_pocket5, protein and id [1343,1160,1349,1350,1331,492,515,516,481,488,480,514] 
set surface_color,  pcol5, surf_pocket5 
set_color pcol6 = [0.702,0.278,0.341]
select surf_pocket6, protein and id [596,598,1043,1067,1068,1071,1073,597,615,617,606,1060,1062,1046,1047,1061,1103,1066] 
set surface_color,  pcol6, surf_pocket6 
set_color pcol7 = [0.902,0.522,0.361]
select surf_pocket7, protein and id [1263,655,775,773,774,656,772,1296,604,1241,1279,607] 
set surface_color,  pcol7, surf_pocket7 
set_color pcol8 = [0.702,0.596,0.278]
select surf_pocket8, protein and id [1160,450,452,1454,1458,1444,1442,492,453,1331,1457] 
set surface_color,  pcol8, surf_pocket8 


deselect

orient
