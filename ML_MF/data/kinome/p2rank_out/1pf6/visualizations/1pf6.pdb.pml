from pymol import cmd,stored

set depth_cue, 1
set fog_start, 0.4

set_color b_col, [36,36,85]
set_color t_col, [10,10,10]
set bg_rgb_bottom, b_col
set bg_rgb_top, t_col      
set bg_gradient

set  spec_power  =  200
set  spec_refl   =  0

load data/1pf6.pdb, protein
create ligands, protein and organic
select xlig, protein and organic
delete xlig

hide everything, all

color white, elem c
color bluewhite, protein
#show_as cartoon, protein
show surface, protein
#set transparency, 0.15

show sticks, ligands
set stick_color, magenta

load data/1pf6.pdb_points.pdb.gz, points
hide nonbonded, points
show nb_spheres, points
set sphere_scale, 0.2, points
cmd.spectrum("b", "green_red", selection="points", minimum=0, maximum=0.7)


stored.list=[]
cmd.iterate("(resn STP)","stored.list.append(resi)")    # read info about residues STP
lastSTP=stored.list[-1] # get the index of the last residue
hide lines, resn STP

cmd.select("rest", "resn STP and resi 0")

for my_index in range(1,int(lastSTP)+1): cmd.select("pocket"+str(my_index), "resn STP and resi "+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.show("spheres","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_scale","0.4","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_transparency","0.1","pocket"+str(my_index))



set_color pcol1 = [0.361,0.576,0.902]
select surf_pocket1, protein and id [1237,2662,2645,2646,2647,2656,1336,1337,1338,1341,1343,527,635,381,1245,1342,1344,219,225,231,210,217,492,493,494,1259,1260,633,634,1261,379,202,255,256,257,364,204,2649,207,205,825,828,823,824,826,377,241,854,833,277,853,2650,1238,1228,1534,1211,1209,1210,1359,1365,1366,1367,1512,1518] 
set surface_color,  pcol1, surf_pocket1 
set_color pcol2 = [0.278,0.380,0.702]
select surf_pocket2, protein and id [681,796,797,798,800,801,802,698,700,116,118,119,120,267,388,389,268,762,719,720,743,763,764,699,727,726,668,682,818,290,140,141,142,291,146,163,289,160,161,162,133,371,372,263,265,266] 
set surface_color,  pcol2, surf_pocket2 
set_color pcol3 = [0.361,0.408,0.902]
select surf_pocket3, protein and id [299,301,302,303,658,350,351,352,298,300,6,8,319,321,323,7,342,328,329,330,37,38,40,42,28,30,63,659,666,668,667,138,140,137] 
set surface_color,  pcol3, surf_pocket3 
set_color pcol4 = [0.310,0.278,0.702]
select surf_pocket4, protein and id [1552,1553,1555,1556,1224,1226,1238,1567,1228,1534,1535,891,893,894,911,1775,1813,1836,1772,1814,1229,1233,2641,2655,2656,892,895,896] 
set surface_color,  pcol4, surf_pocket4 
set_color pcol5 = [0.482,0.361,0.902]
select surf_pocket5, protein and id [1818,1819,1820,1823,1824,1828,1829,1821,1822,2277,1809,1767,1804,2279,2295,2296,2289,2292,2267,2272,2293,1746,1725,1962,1826,1908,1904,1906,1830,1833,1900,1902,1825,1827,1932,1951,1952,1955,1953,1954,2268,2270,1922,1923,1896,1924,1742] 
set surface_color,  pcol5, surf_pocket5 
set_color pcol6 = [0.439,0.278,0.702]
select surf_pocket6, protein and id [1162,1388,1387,1404,1405,1153,1163,2759,2760,2788,2761,1171,1384,1411,1412,1413,1151,1154,2752,1142,1145,1146,2753,538,539,550,552,575,577,1157,573,574,549,551,2789,2790,2791,2792,2810,2811,540,1117,1119] 
set surface_color,  pcol6, surf_pocket6 
set_color pcol7 = [0.651,0.361,0.902]
select surf_pocket7, protein and id [1663,1672,1673,1689,2423,1599,1681,1682,2701,2702,1447,1656,1659,1456,1457,1655,1652,1688,2422,2699,2697,2703,2704,2707,2709,1639,2424,1600,1636,2417,2426,2427,2429,2430,2026,2043,2044,2045,2046,1396,1392,1403] 
set surface_color,  pcol7, surf_pocket7 
set_color pcol8 = [0.573,0.278,0.702]
select surf_pocket8, protein and id [1080,1104,1082,1083,1084,2477,2501,2509,1113,1115,1106,1145,1147,1105,1107,601,1117,589,603,604,620] 
set surface_color,  pcol8, surf_pocket8 
set_color pcol9 = [0.820,0.361,0.902]
select surf_pocket9, protein and id [1371,515,1381,487,1379,1466,1469,1484,1465,1409,1412,1415,1419,1423,1424,1425,1454,1418,1388,1387,1374,1384,513,485,550,552,1474,1428] 
set surface_color,  pcol9, surf_pocket9 
set_color pcol10 = [0.702,0.278,0.698]
select surf_pocket10, protein and id [1629,1630,1631,2087,2088,2090,1615,2089,2085,1490,1493,1494,1495,1881,1883,1470,1477,1646,1608,1521,1605,1606,1607,1576,1578,1882,1879,1611,1635,1642,1644,1479,1480,1489,1670,1668] 
set surface_color,  pcol10, surf_pocket10 
set_color pcol11 = [0.902,0.361,0.812]
select surf_pocket11, protein and id [1937,1938,2198,2199,2200,1892,1919,1890,1891,1941,1945,1939,2150,2154,2160,2140,2147,2153,1920,1914,1915,1921,1923,1924,1887] 
set surface_color,  pcol11, surf_pocket11 
set_color pcol12 = [0.702,0.278,0.565]
select surf_pocket12, protein and id [2831,2848,2996,2932,2933,2874,2833,2834,2832,2864,2865,2987,2973,2980,2985,2986,2898] 
set surface_color,  pcol12, surf_pocket12 
set_color pcol13 = [0.902,0.361,0.643]
select surf_pocket13, protein and id [1274,1275,854,841,842,834,837,1323,343,347,1322,1276] 
set surface_color,  pcol13, surf_pocket13 
set_color pcol14 = [0.702,0.278,0.435]
select surf_pocket14, protein and id [398,406,407,408,402,404,773,397,230,239,248,249,250,390,456,223] 
set surface_color,  pcol14, surf_pocket14 
set_color pcol15 = [0.902,0.361,0.475]
select surf_pocket15, protein and id [2629,920,968,2630,1000,2622,2626,2609,2601,2604,2621,2597,2600,2602,925,926,928,961,966,971,976,954,922,949,950] 
set surface_color,  pcol15, surf_pocket15 
set_color pcol16 = [0.702,0.278,0.302]
select surf_pocket16, protein and id [2227,2247,2251,2260,2370,2367,2368,2243,2244,2242,2240,2241,2252] 
set surface_color,  pcol16, surf_pocket16 
set_color pcol17 = [0.902,0.416,0.361]
select surf_pocket17, protein and id [1237,2641,2645,2646,2647,2656,2643,2640,210,204,2649] 
set surface_color,  pcol17, surf_pocket17 
set_color pcol18 = [0.702,0.388,0.278]
select surf_pocket18, protein and id [1593,1929,1626,2030,1592,1960,2391,2409,2000,1964,1940,1726] 
set surface_color,  pcol18, surf_pocket18 
set_color pcol19 = [0.902,0.584,0.361]
select surf_pocket19, protein and id [848,847,858,862,871,873,861,863,872,201,2637,2632,2633,2635,2636,2638] 
set surface_color,  pcol19, surf_pocket19 
set_color pcol20 = [0.702,0.522,0.278]
select surf_pocket20, protein and id [983,984,985,2318,2304,2306,2305,2319,2321,963,978,955,947] 
set surface_color,  pcol20, surf_pocket20 
set_color pcol21 = [0.902,0.753,0.361]
select surf_pocket21, protein and id [935,943,958,959,2306,1801,1810,932,2295,2296,2293,2294,2291] 
set surface_color,  pcol21, surf_pocket21 
set_color pcol22 = [0.702,0.651,0.278]
select surf_pocket22, protein and id [1273,2621,1297,2598,1266,1268,905,1298,1300,1023,1303,1034,1022] 
set surface_color,  pcol22, surf_pocket22 
set_color pcol23 = [0.878,0.902,0.361]
select surf_pocket23, protein and id [2561,2387,2493,1041,1043,2386,2351,2354] 
set surface_color,  pcol23, surf_pocket23 


deselect

orient
