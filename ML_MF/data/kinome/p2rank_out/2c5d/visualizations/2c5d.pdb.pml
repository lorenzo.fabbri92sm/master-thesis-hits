from pymol import cmd,stored

set depth_cue, 1
set fog_start, 0.4

set_color b_col, [36,36,85]
set_color t_col, [10,10,10]
set bg_rgb_bottom, b_col
set bg_rgb_top, t_col      
set bg_gradient

set  spec_power  =  200
set  spec_refl   =  0

load data/2c5d.pdb, protein
create ligands, protein and organic
select xlig, protein and organic
delete xlig

hide everything, all

color white, elem c
color bluewhite, protein
#show_as cartoon, protein
show surface, protein
#set transparency, 0.15

show sticks, ligands
set stick_color, magenta

load data/2c5d.pdb_points.pdb.gz, points
hide nonbonded, points
show nb_spheres, points
set sphere_scale, 0.2, points
cmd.spectrum("b", "green_red", selection="points", minimum=0, maximum=0.7)


stored.list=[]
cmd.iterate("(resn STP)","stored.list.append(resi)")    # read info about residues STP
lastSTP=stored.list[-1] # get the index of the last residue
hide lines, resn STP

cmd.select("rest", "resn STP and resi 0")

for my_index in range(1,int(lastSTP)+1): cmd.select("pocket"+str(my_index), "resn STP and resi "+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.show("spheres","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_scale","0.4","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_transparency","0.1","pocket"+str(my_index))



set_color pcol1 = [0.361,0.576,0.902]
select surf_pocket1, protein and id [4470,3086,3088,3042,3043,4479,4480,3089,5837,4474,4241,4476,4500,5857,5858,4540,4558,5818,5819,5816,5817,4468,3338,3342,4247,4251,5812,4238,3028,3029,5637,4431,4433,4436,4434,4437,3109,4439,4441,4443,4444,5820,5821] 
set surface_color,  pcol1, surf_pocket1 
set_color pcol2 = [0.278,0.392,0.702]
select surf_pocket2, protein and id [976,863,977,913,914,7478,7462,7474,896,953,933,7458,7459,7463,7464,7467,8185,8164,8168,8175,8167,7480,7457,947,7465] 
set surface_color,  pcol2, surf_pocket2 
set_color pcol3 = [0.361,0.431,0.902]
select surf_pocket3, protein and id [1396,1416,1418,6292,236,237,256,258,259,6275,6276,6277,6428,6430,6427,1332,1352,1350,1349,1379,1364,1391,1392,1389,1390,1393,1382,1363,1371,277,6445,6447] 
set surface_color,  pcol3, surf_pocket3 
set_color pcol4 = [0.278,0.278,0.702]
select surf_pocket4, protein and id [3907,6706,6707,6709,6713,6714,3930,3931,6712,6008,6009,6730,6010,6012,6728,3816,3817,3818,3849,3850,3851,3814,3815,3823,3867,3868,3906,3887,6007,7269,3901,6003,6004,6705] 
set surface_color,  pcol4, surf_pocket4 
set_color pcol5 = [0.431,0.361,0.902]
select surf_pocket5, protein and id [1525,74,1526,1584,1586,2883,1604,2864,2865,1489,1490,2866,2868,75,2862,2863,2858,2867,1603,2683,89,135,151,1483,1487,155,153,1511,88,1479,1480,1516,1522] 
set surface_color,  pcol5, surf_pocket5 
set_color pcol6 = [0.392,0.278,0.702]
select surf_pocket6, protein and id [2831,1618,1621,1613,1614,2833,1630,1631,2573,2574,2576,2579,2662,1646,1647,2839,2850,2852,2854,176,178,2828,2829,2830,2832,2834,6333,6345] 
set surface_color,  pcol6, surf_pocket6 
set_color pcol7 = [0.576,0.361,0.902]
select surf_pocket7, protein and id [975,976,863,977,869,880,881,882,990,8747,8226,8227,7462,864,7463,8180,8185,8175,8197,8191,8194,8195,7465,8187] 
set surface_color,  pcol7, surf_pocket7 
set_color pcol8 = [0.506,0.278,0.702]
select surf_pocket8, protein and id [4343,4344,4347,4350,7884,7730,7732,7882,7883,7885,7902,7898,7900,7901,7731,3212,3213,3231,3190,3191,7747,4346,4300,4303,4317,4325,4333,4284,4306,4372,4370] 
set surface_color,  pcol8, surf_pocket8 
set_color pcol9 = [0.722,0.361,0.902]
select surf_pocket9, protein and id [6008,6730,6732,6010,3929,3930,3931,6720,3817,3851,3814,3815,3823,6007,6004,3836,6725,6736,6742,7287] 
set surface_color,  pcol9, surf_pocket9 
set_color pcol10 = [0.616,0.278,0.702]
select surf_pocket10, protein and id [3199,4069,3438,3440,3441,3444,3992,4047,4048,4008,4050,4052,3991,3435,3437,3471,3472,3993,3200,3221,3222,7601] 
set surface_color,  pcol10, surf_pocket10 
set_color pcol11 = [0.863,0.361,0.902]
select surf_pocket11, protein and id [402,423,2896,2898,2710,2771,2772,2776,2770,1560,1567,1568,2709,403,2890,1576,2887,2782,2897,2894,2893] 
set surface_color,  pcol11, surf_pocket11 
set_color pcol12 = [0.702,0.278,0.671]
select surf_pocket12, protein and id [266,267,268,6147,6163,1098,6145,6146,6161,6151,6156,6159,6165,6270,6230,6238,6243,1076,1059,1061] 
set surface_color,  pcol12, surf_pocket12 
set_color pcol13 = [0.902,0.361,0.792]
select surf_pocket13, protein and id [1095,267,6147,1096,1098,243,244,245,246,483,484,486,487,1038,1039,1114,1115,1093,1094,518,490,1037,1054] 
set surface_color,  pcol13, surf_pocket13 
set_color pcol14 = [0.702,0.278,0.561]
select surf_pocket14, protein and id [5725,5726,5730,5850,5852,5841,5743,5844,4530,5847,5848,5851,5664,3356,3377,3378,4521,4522,5663,3357,5846] 
set surface_color,  pcol14, surf_pocket14 
set_color pcol15 = [0.902,0.361,0.647]
select surf_pocket15, protein and id [1514,1287,2904,89,132,134,1511,1516,1522,1291,2883] 
set surface_color,  pcol15, surf_pocket15 
set_color pcol16 = [0.702,0.278,0.447]
select surf_pocket16, protein and id [1551,1553,1555,1559,1560,1568,2695,2709,1791,1793,2179,2694,1770,2697,1771,2698,2704,1554,2931,61,2914,2909,2696] 
set surface_color,  pcol16, surf_pocket16 
set_color pcol17 = [0.902,0.361,0.506]
select surf_pocket17, protein and id [1645,1646,1647,2531,2550,1883,2831,2833,1631,2573,2576,2834] 
set surface_color,  pcol17, surf_pocket17 
set_color pcol18 = [0.702,0.278,0.337]
select surf_pocket18, protein and id [3834,3836,3835,3944,6725,6736,6740,6742,7287,6771,6772,3929,6720] 
set surface_color,  pcol18, surf_pocket18 
set_color pcol19 = [0.902,0.361,0.361]
select surf_pocket19, protein and id [7897,7708,7688,7689,7711,7892,7964,7702,7906,7911,7962,7963,7910,7893,7994,7995,7650] 
set surface_color,  pcol19, surf_pocket19 
set_color pcol20 = [0.702,0.337,0.278]
select surf_pocket20, protein and id [4512,4513,4522,5649,5663,5133,4733,4505,4510,4514,4507,4509,5868,4743,4745,4747] 
set surface_color,  pcol20, surf_pocket20 
set_color pcol21 = [0.902,0.506,0.361]
select surf_pocket21, protein and id [799,1311,1313,796,1315,111,107,109,95,105,106,112,1528,1530,389,1519,794] 
set surface_color,  pcol21, surf_pocket21 
set_color pcol22 = [0.702,0.447,0.278]
select surf_pocket22, protein and id [4572,4583,4568,5806,5808,4584,4585,5616,5521,5527,5528,5533,5787,5788] 
set surface_color,  pcol22, surf_pocket22 
set_color pcol23 = [0.902,0.647,0.361]
select surf_pocket23, protein and id [471,1112,1113,1114,1115,485,223,226,227,1130,1159,6301,1158] 
set surface_color,  pcol23, surf_pocket23 
set_color pcol24 = [0.702,0.561,0.278]
select surf_pocket24, protein and id [1374,845,846,847,335,348,349,350,830,831,914,912] 
set surface_color,  pcol24, surf_pocket24 
set_color pcol25 = [0.902,0.792,0.361]
select surf_pocket25, protein and id [5783,5785,5784,5786,5787,5788,4837,5530,4583,4585,4600,4601,5485] 
set surface_color,  pcol25, surf_pocket25 
set_color pcol26 = [0.702,0.671,0.278]
select surf_pocket26, protein and id [1598,2699,2717,2777,2779,2794,2795,2796,1575,1576,2776,2711] 
set surface_color,  pcol26, surf_pocket26 
set_color pcol27 = [0.863,0.902,0.361]
select surf_pocket27, protein and id [3101,3103,3118,4221,4239,5798,5757] 
set surface_color,  pcol27, surf_pocket27 


deselect

orient
