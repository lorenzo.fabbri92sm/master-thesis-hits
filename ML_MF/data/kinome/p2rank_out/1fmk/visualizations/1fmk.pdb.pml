from pymol import cmd,stored

set depth_cue, 1
set fog_start, 0.4

set_color b_col, [36,36,85]
set_color t_col, [10,10,10]
set bg_rgb_bottom, b_col
set bg_rgb_top, t_col      
set bg_gradient

set  spec_power  =  200
set  spec_refl   =  0

load data/1fmk.pdb, protein
create ligands, protein and organic
select xlig, protein and organic
delete xlig

hide everything, all

color white, elem c
color bluewhite, protein
#show_as cartoon, protein
show surface, protein
#set transparency, 0.15

show sticks, ligands
set stick_color, magenta

load data/1fmk.pdb_points.pdb.gz, points
hide nonbonded, points
show nb_spheres, points
set sphere_scale, 0.2, points
cmd.spectrum("b", "green_red", selection="points", minimum=0, maximum=0.7)


stored.list=[]
cmd.iterate("(resn STP)","stored.list.append(resi)")    # read info about residues STP
lastSTP=stored.list[-1] # get the index of the last residue
hide lines, resn STP

cmd.select("rest", "resn STP and resi 0")

for my_index in range(1,int(lastSTP)+1): cmd.select("pocket"+str(my_index), "resn STP and resi "+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.show("spheres","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_scale","0.4","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_transparency","0.1","pocket"+str(my_index))



set_color pcol1 = [0.361,0.576,0.902]
select surf_pocket1, protein and id [2475,2562,2563,2564,2567,2568,2569,2570,2566,1710,1711,1712,1598,1693,1696,1698,1704,1708,1553,2467,1560,1546,1547,1548,1550,1559,1599,1597,1583,2456,2468,2056,2055,2574,2583,2584,2042,2439,1563,1568,1569,1574,1576,1578,1579,1573,1580,2594,2596,2604,2598,1577,2081,2102,2492,2105,1544,2072,2089,1540,1541,2109,2134,1542,1545,2110,2078,2491,1695] 
set surface_color,  pcol1, surf_pocket1 
set_color pcol2 = [0.278,0.333,0.702]
select surf_pocket2, protein and id [1339,1345,1352,1355,1356,1335,1336,1340,71,431,57,69,59,519,56,60,617,618,621,655,656,584,1334,554,555,556,581,582,558,560,562,563,544,521,561,512,1911,1908,1909,1910,1919,1363,1365,2065,2083,2085,2549,2550,2079,2082,2511,2069,2501,2504,2507,2508,2535,2520,644,1920,2533,2534,2086] 
set surface_color,  pcol2, surf_pocket2 
set_color pcol3 = [0.443,0.361,0.902]
select surf_pocket3, protein and id [1894,1898,1900,1901,1902,2319,2317,2318,3343,3344,3345,3377,2343,2372,2373,619,607,608,622,3378,589,590,591,609,791,1907,1908,1919,1903,2289,2290,2292,2293] 
set surface_color,  pcol3, surf_pocket3 
set_color pcol4 = [0.463,0.278,0.702]
select surf_pocket4, protein and id [2058,2061,2069,1962,1963,2066,1676,1672,1662,1677,101,1373,115,116,82,87,89,446,1349,93,96,97,113,98,1354,1350,99,1372,1362,1363,1366,1359,1367] 
set surface_color,  pcol4, surf_pocket4 
set_color pcol5 = [0.741,0.361,0.902]
select surf_pocket5, protein and id [604,605,606,631,633,638,635,636,637,3437,3438,942,632,939,935,962,3450,3373,3413,3421,3422,610,672,611,2238,3365,3402,3399,3403,3406,2270,2271,2272] 
set surface_color,  pcol5, surf_pocket5 
set_color pcol6 = [0.694,0.278,0.702]
select surf_pocket6, protein and id [2456,2126,2468,2856,2909,2454,2650,2908,2616,2619,2638,2651,2636] 
set surface_color,  pcol6, surf_pocket6 
set_color pcol7 = [0.902,0.361,0.757]
select surf_pocket7, protein and id [599,600,3433,958,842,783,977,981,982,983,984,3440,3446,3447,3448,3471,985,766,798,797,799,789,792] 
set surface_color,  pcol7, surf_pocket7 
set_color pcol8 = [0.702,0.278,0.475]
select surf_pocket8, protein and id [1384,1956,1398,1420,289,1415,285,1381,1889,1890,1944,1948,292,290,1859,1863,1861,1864,1377,1383,1380,1376,1378] 
set surface_color,  pcol8, surf_pocket8 
set_color pcol9 = [0.902,0.361,0.459]
select surf_pocket9, protein and id [1853,1949,2041,2056,2055,1932,1854,1855,2577,2559,2562,2563,2564,2569,2565,2571,1711,2575,2591,2592,2574] 
set surface_color,  pcol9, surf_pocket9 
set_color pcol10 = [0.702,0.318,0.278]
select surf_pocket10, protein and id [1214,1215,1181,1182,1298,1315,742,533,535,1179,1151,522,525] 
set surface_color,  pcol10, surf_pocket10 
set_color pcol11 = [0.902,0.561,0.361]
select surf_pocket11, protein and id [3188,3072,3157,3158,3061,3077,3078,3081,3187,3062,3063,3155,3165] 
set surface_color,  pcol11, surf_pocket11 
set_color pcol12 = [0.702,0.553,0.278]
select surf_pocket12, protein and id [1417,1418,1421,1403,1411,1412,123,124,125,1654,1399,1644,126,140,1446,1451,1641,1484,1485,1458,1459,1653] 
set surface_color,  pcol12, surf_pocket12 
set_color pcol13 = [0.902,0.859,0.361]
select surf_pocket13, protein and id [2574,2042,2598,2594,2591,2592,2590,1711,1712,2564,2565,2571] 
set surface_color,  pcol13, surf_pocket13 


deselect

orient
