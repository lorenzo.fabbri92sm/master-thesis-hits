from pymol import cmd,stored

set depth_cue, 1
set fog_start, 0.4

set_color b_col, [36,36,85]
set_color t_col, [10,10,10]
set bg_rgb_bottom, b_col
set bg_rgb_top, t_col      
set bg_gradient

set  spec_power  =  200
set  spec_refl   =  0

load data/4dee.pdb, protein
create ligands, protein and organic
select xlig, protein and organic
delete xlig

hide everything, all

color white, elem c
color bluewhite, protein
#show_as cartoon, protein
show surface, protein
#set transparency, 0.15

show sticks, ligands
set stick_color, magenta

load data/4dee.pdb_points.pdb.gz, points
hide nonbonded, points
show nb_spheres, points
set sphere_scale, 0.2, points
cmd.spectrum("b", "green_red", selection="points", minimum=0, maximum=0.7)


stored.list=[]
cmd.iterate("(resn STP)","stored.list.append(resi)")    # read info about residues STP
lastSTP=stored.list[-1] # get the index of the last residue
hide lines, resn STP

cmd.select("rest", "resn STP and resi 0")

for my_index in range(1,int(lastSTP)+1): cmd.select("pocket"+str(my_index), "resn STP and resi "+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.show("spheres","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_scale","0.4","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_transparency","0.1","pocket"+str(my_index))



set_color pcol1 = [0.361,0.576,0.902]
select surf_pocket1, protein and id [1141,1153,1154,1236,1237,1241,1238,1242,1243,1244,481,1257,480,380,411,173,175,414,513,443,444,447,1112,1113,1266,1267,1268,1269,438,441,1271,413,1169,1170,784,194,195,196,321,322,323,327,336,338,169,171,514,590,736,731,137,139,140,306,757,778,775,754,1146,162,164,165,166,1142,1143,1130,1372,1373,1379,1370,1371,1366,1368,1369,141,142,143,145,147,148,155,156,158,159,181,160,167,178,135,136,783,810,1418,1416,1417,1363,1354,1362,1098] 
set surface_color,  pcol1, surf_pocket1 
set_color pcol2 = [0.365,0.278,0.702]
select surf_pocket2, protein and id [282,283,284,285,301,723,25,26,27,50,47,48,279,280,265,92,237,238,240,615,617,619,620,616,618,614,613] 
set surface_color,  pcol2, surf_pocket2 
set_color pcol3 = [0.792,0.361,0.902]
select surf_pocket3, protein and id [800,802,1144,1145,1136,798,840,831,828,829,1126,1408,1398,1409,1622,1667,1668,1654,1128,1382,1695,1652] 
set surface_color,  pcol3, surf_pocket3 
set_color pcol4 = [0.702,0.278,0.533]
select surf_pocket4, protein and id [329,685,349,687,706,203,205,206,207,330,109,108,110,64,65] 
set surface_color,  pcol4, surf_pocket4 
set_color pcol5 = [0.902,0.361,0.361]
select surf_pocket5, protein and id [1547,2062,2056,2060,2061,1015,1040,1512,1037,1038,2213,2064,2068,2071,2212,2049,2052,2053,2054,1523,1524,1525] 
set surface_color,  pcol5, surf_pocket5 
set_color pcol6 = [0.702,0.533,0.278]
select surf_pocket6, protein and id [1177,1196,1197,1195,2187,769,770,772,1192,948,950,937,939,2178,822,917,918,821,823,914,915] 
set surface_color,  pcol6, surf_pocket6 


deselect

orient
