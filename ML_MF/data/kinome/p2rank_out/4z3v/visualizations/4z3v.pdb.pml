from pymol import cmd,stored

set depth_cue, 1
set fog_start, 0.4

set_color b_col, [36,36,85]
set_color t_col, [10,10,10]
set bg_rgb_bottom, b_col
set bg_rgb_top, t_col      
set bg_gradient

set  spec_power  =  200
set  spec_refl   =  0

load data/4z3v.pdb, protein
create ligands, protein and organic
select xlig, protein and organic
delete xlig

hide everything, all

color white, elem c
color bluewhite, protein
#show_as cartoon, protein
show surface, protein
#set transparency, 0.15

show sticks, ligands
set stick_color, magenta

load data/4z3v.pdb_points.pdb.gz, points
hide nonbonded, points
show nb_spheres, points
set sphere_scale, 0.2, points
cmd.spectrum("b", "green_red", selection="points", minimum=0, maximum=0.7)


stored.list=[]
cmd.iterate("(resn STP)","stored.list.append(resi)")    # read info about residues STP
lastSTP=stored.list[-1] # get the index of the last residue
hide lines, resn STP

cmd.select("rest", "resn STP and resi 0")

for my_index in range(1,int(lastSTP)+1): cmd.select("pocket"+str(my_index), "resn STP and resi "+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.show("spheres","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_scale","0.4","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_transparency","0.1","pocket"+str(my_index))



set_color pcol1 = [0.361,0.576,0.902]
select surf_pocket1, protein and id [1183,1257,1259,1264,1265,581,1184,1276,1260,1288,504,1277,761,717,718,334,349,350,740,744,743,751,598,351,1266,228,229,230,348,763,765,769,164,165,166,168,169,206,198,756,762,174,177,794,171,176,184,185,186,216,188,194,1155,1156,1158,1159,1169,1170,196,200,1133,1365,1367,770,791] 
set surface_color,  pcol1, surf_pocket1 
set_color pcol2 = [0.329,0.278,0.702]
select surf_pocket2, protein and id [1796,1802,1804,1806,1779,1782,1784,1786,1801,1803,1805,1921,1924,1928,1930,1931,1945,1951,1955,1956,1957,1958,1959,1960,1763,1920,1905,1914,1866,1867,1929,1736,1737,1738,1774,1775,1777,1757,1758,1759,1769,1778,1761,1677,1680,1717,1703,1713,1948,1716,1714,1715] 
set surface_color,  pcol2, surf_pocket2 
set_color pcol3 = [0.698,0.361,0.902]
select surf_pocket3, protein and id [1323,1294,1362,1293,1372,1373,1374,1375,1377,1133,1432,1433,1363,1365,1325,1119,1120,1295,1118,1121,1122,1301,1424,1563,1560,1562,1564,1565,1542,1544,1471,1384] 
set surface_color,  pcol3, surf_pocket3 
set_color pcol4 = [0.702,0.278,0.639]
select surf_pocket4, protein and id [760,805,758,759,803,863,1220,1210,1211,1217,1218,883,861,857,920,921,924,980,917,919,957,945,946,1203,851] 
set surface_color,  pcol4, surf_pocket4 
set_color pcol5 = [0.902,0.361,0.545]
select surf_pocket5, protein and id [328,606,605,72,74,272,274,275,277,276,318,307,308,309,311,312,313,607,608,609,610,611,612] 
set surface_color,  pcol5, surf_pocket5 
set_color pcol6 = [0.702,0.353,0.278]
select surf_pocket6, protein and id [1410,1404,1367,1413,1156,1158,1159,1160,1431,785,786,1690,823,1464,1447,1448,1449,1428,1419,1821,1820,1746,1748,1744,1463] 
set surface_color,  pcol6, surf_pocket6 
set_color pcol7 = [0.902,0.729,0.361]
select surf_pocket7, protein and id [731,1206,1213,724,725,727,1199,1196,551,1226,1242,1243,556,1197,1225,1227,552] 
set surface_color,  pcol7, surf_pocket7 


deselect

orient
