from pymol import cmd,stored

set depth_cue, 1
set fog_start, 0.4

set_color b_col, [36,36,85]
set_color t_col, [10,10,10]
set bg_rgb_bottom, b_col
set bg_rgb_top, t_col      
set bg_gradient

set  spec_power  =  200
set  spec_refl   =  0

load data/2i0v.pdb, protein
create ligands, protein and organic
select xlig, protein and organic
delete xlig

hide everything, all

color white, elem c
color bluewhite, protein
#show_as cartoon, protein
show surface, protein
#set transparency, 0.15

show sticks, ligands
set stick_color, magenta

load data/2i0v.pdb_points.pdb.gz, points
hide nonbonded, points
show nb_spheres, points
set sphere_scale, 0.2, points
cmd.spectrum("b", "green_red", selection="points", minimum=0, maximum=0.7)


stored.list=[]
cmd.iterate("(resn STP)","stored.list.append(resi)")    # read info about residues STP
lastSTP=stored.list[-1] # get the index of the last residue
hide lines, resn STP

cmd.select("rest", "resn STP and resi 0")

for my_index in range(1,int(lastSTP)+1): cmd.select("pocket"+str(my_index), "resn STP and resi "+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.show("spheres","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_scale","0.4","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_transparency","0.1","pocket"+str(my_index))



set_color pcol1 = [0.361,0.576,0.902]
select surf_pocket1, protein and id [939,957,1362,985,1360,1361,960,961,981,982,983,958,935,934,443,393,570,390,388,391,394,959,966,953,955,1467,1468,1476,1477,396,1442,1446,1447,1448,1450,1332,989,1436,585,1460,1464,442,583,991,826] 
set surface_color,  pcol1, surf_pocket1 
set_color pcol2 = [0.278,0.322,0.702]
select surf_pocket2, protein and id [1734,1243,1244,1245,1733,1735,1262,1267,92,94,1744,108,109,132,2285,2287,2288,2286,1220,2303,2299] 
set surface_color,  pcol2, surf_pocket2 
set_color pcol3 = [0.467,0.361,0.902]
select surf_pocket3, protein and id [1438,1439,1440,50,52,53,1310,1544,1493,1495,32,39,45,24,26,20,417,419,421,1444,1455,1473,1474,1346,1443,1452,714,715,1453,1454,1456,1457,1463] 
set surface_color,  pcol3, surf_pocket3 
set_color pcol4 = [0.490,0.278,0.702]
select surf_pocket4, protein and id [1632,1531,1532,1533,1901,1898,1335,1519,1333,1008,1885,1043,1044,1847] 
set surface_color,  pcol4, surf_pocket4 
set_color pcol5 = [0.792,0.361,0.902]
select surf_pocket5, protein and id [1597,1567,1568,1569,1566,1595,1596,1578,51,52,53,1604,37,36,41,1718,1702,1297,1299,1300,1301,1719,1720,1642,1641,1296] 
set surface_color,  pcol5, surf_pocket5 
set_color pcol6 = [0.702,0.278,0.659]
select surf_pocket6, protein and id [67,1430,1433,824,825,826,68,747,748,61,60,1437,1436,586,712,713,714,1448,1450,935,934,1440] 
set surface_color,  pcol6, surf_pocket6 
set_color pcol7 = [0.902,0.361,0.682]
select surf_pocket7, protein and id [2164,2168,2172,2196,2165,2193,2275,2278,2279,2280,2322,2312,2311,2318,2344,2341,2342,2343,2346,2345,2347,2310,2317] 
set surface_color,  pcol7, surf_pocket7 
set_color pcol8 = [0.702,0.278,0.408]
select surf_pocket8, protein and id [1568,1566,1594,1596,1610,1611,1999,1549,1552,1557,1997,1676,1667,1673] 
set surface_color,  pcol8, surf_pocket8 
set_color pcol9 = [0.902,0.361,0.361]
select surf_pocket9, protein and id [1492,1493,1495,1494,1488,1501,408,412,1502,1583,1584,5,1582,1579,419,421,11,20,1455] 
set surface_color,  pcol9, surf_pocket9 
set_color pcol10 = [0.702,0.408,0.278]
select surf_pocket10, protein and id [740,206,207,208,59,75,76,77,705,706,678,681,42,44] 
set surface_color,  pcol10, surf_pocket10 
set_color pcol11 = [0.902,0.682,0.361]
select surf_pocket11, protein and id [1919,1777,1778,1917,2059,2049,2212,1809,2210,2213,2067,2206,2207] 
set surface_color,  pcol11, surf_pocket11 
set_color pcol12 = [0.702,0.659,0.278]
select surf_pocket12, protein and id [2141,2146,2148,2121,2122,1141,2137,2138,2140,2379,2380,2381,2382,2378] 
set surface_color,  pcol12, surf_pocket12 


deselect

orient
