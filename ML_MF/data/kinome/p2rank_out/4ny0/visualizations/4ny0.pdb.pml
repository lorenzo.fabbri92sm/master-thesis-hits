from pymol import cmd,stored

set depth_cue, 1
set fog_start, 0.4

set_color b_col, [36,36,85]
set_color t_col, [10,10,10]
set bg_rgb_bottom, b_col
set bg_rgb_top, t_col      
set bg_gradient

set  spec_power  =  200
set  spec_refl   =  0

load data/4ny0.pdb, protein
create ligands, protein and organic
select xlig, protein and organic
delete xlig

hide everything, all

color white, elem c
color bluewhite, protein
#show_as cartoon, protein
show surface, protein
#set transparency, 0.15

show sticks, ligands
set stick_color, magenta

load data/4ny0.pdb_points.pdb.gz, points
hide nonbonded, points
show nb_spheres, points
set sphere_scale, 0.2, points
cmd.spectrum("b", "green_red", selection="points", minimum=0, maximum=0.7)


stored.list=[]
cmd.iterate("(resn STP)","stored.list.append(resi)")    # read info about residues STP
lastSTP=stored.list[-1] # get the index of the last residue
hide lines, resn STP

cmd.select("rest", "resn STP and resi 0")

for my_index in range(1,int(lastSTP)+1): cmd.select("pocket"+str(my_index), "resn STP and resi "+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.show("spheres","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_scale","0.4","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_transparency","0.1","pocket"+str(my_index))



set_color pcol1 = [0.361,0.576,0.902]
select surf_pocket1, protein and id [9825,9827,9061,9063,9023,9024,9025,9026,9054,9014,9053,9017,9018,9862,9873,9874,9875,9876,8996,8086,8088,8090,8756,8772,8773,8436,8073,8063,8066,8529,8531,8534,8789,8792,8145,8154,8144,8143,8149,8152,8511,8510,8512,8452,8755,8735,8451,9062,8734,10467,10497,9880,9890,9892,10474,10473,10475,9841,9844,9843,9003,8779,8812,8813,8785,8786,8787,8814,8986,8174,8172,9826,9832] 
set surface_color,  pcol1, surf_pocket1 
set_color pcol2 = [0.278,0.408,0.702]
select surf_pocket2, protein and id [1054,1055,1056,1855,1866,1867,1017,996,1836,1019,991,1883,1885,1873,1868,505,524,427,429,1869,527,1018,778,782,765,766,772,989,502,503,504,2466,2467,2468,2487,2488,2490,1825,1834,1837,1818,1820] 
set surface_color,  pcol2, surf_pocket2 
set_color pcol3 = [0.361,0.467,0.902]
select surf_pocket3, protein and id [2607,295,2608,5724,5725,5728,7913,293,294,7933,7936,7937,561,248,261,262,263,264,5701,5703,5721,5722,5633,389,2604,2606,2598,2599,2603,387,390,2595,7918,7908,7910,7924,7925,7930,7931,7899,7932,7934,553,2565,7942,7945,286,382,400,384,5715,5716,6198,6201,6227,5714,5704,5705,5706,6150,6151] 
set surface_color,  pcol3, surf_pocket3 
set_color pcol4 = [0.278,0.322,0.702]
select surf_pocket4, protein and id [6334,7172,7174,7175,6357,7158,6327,6104,6355,6356,6393,6394,7190,7192,7193,7204,7163,6295,7215,7225,7211,6110,6116,6118,6120,5765,5860,5862,5865,6103,5767,7205,7206,7224,7207,5822,5843,5841,5842,5837,7218,7221,7223,7804,7805,7806,6117,7828,5840,7798,7825] 
set surface_color,  pcol4, surf_pocket4 
set_color pcol5 = [0.361,0.361,0.902]
select surf_pocket5, protein and id [3172,3173,3174,3193,3096,5159,3191,3098,3724,3725,3153,5129,3171,4552,4537,4542,4554,5156,5135,5136,5137,3434,3626,3658,3688,3435,3687,3665,3686,4487,4489,3441,3447,3194,3196,4536,4538,4522,4503,4521,4523,4524,4535,4494,3449,3451,4505,4506] 
set surface_color,  pcol5, surf_pocket5 
set_color pcol6 = [0.322,0.278,0.702]
select surf_pocket6, protein and id [1929,1930,5057,5058,5059,5060,1937,2390,4599,2388,4598,4588,5076,4593,5064,1909,1910,1915,1916,1917,1934,1919,1925,1931,2405,4604,4584,4585,4627,4603,1935,1958,1924,2389,2391,2395,2407] 
set surface_color,  pcol6, surf_pocket6 
set_color pcol7 = [0.467,0.361,0.902]
select surf_pocket7, protein and id [3416,3418,3687,2752,2750,3679,3417,3396,3397,3680,3716,2811,2812,2813,2814,2816,3715,2725,2728,2735,2807,2806,2810,2827,3132,3395,3724,3113,3114,3174,3098] 
set surface_color,  pcol7, surf_pocket7 
set_color pcol8 = [0.408,0.278,0.702]
select surf_pocket8, protein and id [2735,3648,2807,3674,4093,4084,4092,2810,2808,2809,4091,4094,4095,4096,4097,2806,3680,2811,2814,2816,3715,3705,3706,3708,3644,3679,3675,3676,3673,2750,2835,2772,3888,3889,4109,4111,3900,2777,2786,2791,3649,3850,3853,3887,3899] 
set surface_color,  pcol8, surf_pocket8 
set_color pcol9 = [0.576,0.361,0.902]
select surf_pocket9, protein and id [6522,6778,6569,6780,6762,6735,6736,6763,6765,6737,6764,6766,5504,6317,6318,6343,5476,5477,5478,5450,5455,5460,5471,5470,5446,6558,6557,6567,5441] 
set surface_color,  pcol9, surf_pocket9 
set_color pcol10 = [0.490,0.278,0.702]
select surf_pocket10, protein and id [8383,8370,8377,8386,8393,8397,8390,8392,2964,10600,10601,10602,10568,10577,10605,10606,10610,10611,8394,8391,2963,8302,10614,8819,8820,8385,10582,5264,10581,10579,2931,2932,2933,8870,5277,5272,5276] 
set surface_color,  pcol10, surf_pocket10 
set_color pcol11 = [0.682,0.361,0.902]
select surf_pocket11, protein and id [2964,3053,10593,10599,10600,10601,10602,8370,8397,2963,10587,10579,10582,2930,2931,2932,2933,3222,3051,3069,5231,5230,5268,5272,3056,3059,5234,5273,5276,5277,3058] 
set surface_color,  pcol11, surf_pocket11 
set_color pcol12 = [0.573,0.278,0.702]
select surf_pocket12, protein and id [1426,139,101,102,103,104,1218,1220,1229,1398,1427,1428,1429,107,108,117,122,132,140,166,1422,1424,1425,1440,1184,1216,1212,1005] 
set surface_color,  pcol12, surf_pocket12 
set_color pcol13 = [0.792,0.361,0.902]
select surf_pocket13, protein and id [1046,1047,138,167,979,1018,81,975,1010,1011,79,83,59,144,145,147,728,158,136,137,141,142,143,745,747,748,749,727,56,63,66,1036,1006,1007,1037,1005] 
set surface_color,  pcol13, surf_pocket13 
set_color pcol14 = [0.659,0.278,0.702]
select surf_pocket14, protein and id [4518,3503,3505,4520,4682,3507,4425,4426,4463,4464,4512,4427,4498,4688,4690,4691,4692,4499,3493,3494,3497,3508,3509,3512,3516,3513,3514,4434,4435] 
set surface_color,  pcol14, surf_pocket14 
set_color pcol15 = [0.902,0.361,0.902]
select surf_pocket15, protein and id [7132,7133,7181,6172,6174,7189,6176,6183,7179,6181,7187,7357,7359,7360,7168] 
set surface_color,  pcol15, surf_pocket15 
set_color pcol16 = [0.702,0.278,0.659]
select surf_pocket16, protein and id [5421,5420,6349,6356,5475,5480,6066,5483,5485,6393,6385,6085,6086,6087,5404,5394,5397,5767,5783] 
set surface_color,  pcol16, surf_pocket16 
set_color pcol17 = [0.902,0.361,0.792]
select surf_pocket17, protein and id [9012,8111,9191,8986,8987,9435,9431,9447,9449,9238,9436,9227,9236,8124,8125,8129,8173,8146,8147,9432,9433,9434] 
set surface_color,  pcol17, surf_pocket17 
set_color pcol18 = [0.702,0.278,0.573]
select surf_pocket18, protein and id [1046,1426,138,139,979,975,1010,1011,133,147,136,137,141,1036,1002,1006,1007,1037,1003,1005,1422,1424,1425,1184,1415,1039] 
set surface_color,  pcol18, surf_pocket18 
set_color pcol19 = [0.902,0.361,0.682]
select surf_pocket19, protein and id [9764,9772,9773,9763,9858,9837,9801,9802,9836,9856,8852,9765,8850,8851,8845,10028,10029,10030,8843,9848,9850,9855] 
set surface_color,  pcol19, surf_pocket19 
set_color pcol20 = [0.702,0.278,0.490]
select surf_pocket20, protein and id [6159,6161,7503,7505,6153,6157,7349,7884,6141,2586,2592,7494,7493,7495,2585,2580,7507,7513,2593,2595,7921,7914] 
set surface_color,  pcol20, surf_pocket20 
set_color pcol21 = [0.902,0.361,0.576]
select surf_pocket21, protein and id [5308,5310,5311,4902,4904,5326,5307,5328,5177,5146,3239,3237,5292,3212,3214,3213,3211,3232,3228] 
set surface_color,  pcol21, surf_pocket21 
set_color pcol22 = [0.702,0.278,0.408]
select surf_pocket22, protein and id [1830,1794,1795,1757,1765,1766,2020,838,845,1849,1851,1848,2021,2022,2023] 
set surface_color,  pcol22, surf_pocket22 
set_color pcol23 = [0.902,0.361,0.467]
select surf_pocket23, protein and id [568,570,544,559,562,563,2642,2508,542,543,545,546,2235,2477,2479,519,2639,2641,2622,2623,2638,2233,2658,2659] 
set surface_color,  pcol23, surf_pocket23 
set_color pcol24 = [0.702,0.278,0.322]
select surf_pocket24, protein and id [9046,9422,9429,9006,9013,9007,9014,9043,9044,9017,9018,8145,8144,8146,8086,8088,9012,8986,8174,9009,9011,9431,8172] 
set surface_color,  pcol24, surf_pocket24 
set_color pcol25 = [0.902,0.361,0.361]
select surf_pocket25, protein and id [5906,5908,5884,5882,5897,7979,7820,7846,5880,5881,7815,7842,7817,7980,7571,7573] 
set surface_color,  pcol25, surf_pocket25 
set_color pcol26 = [0.702,0.322,0.278]
select surf_pocket26, protein and id [5724,5725,5726,5728,7937,5720,5727,5738,5891,7903,7904,5721,5722,5633,7942,7945,5601,5602] 
set surface_color,  pcol26, surf_pocket26 
set_color pcol27 = [0.902,0.467,0.361]
select surf_pocket27, protein and id [5261,8826,8828,10018,10590,8822,8816,5249,10182,10040,10172,10016,10583,8810,10163,10041,10161,10165,10169] 
set surface_color,  pcol27, surf_pocket27 
set_color pcol28 = [0.702,0.408,0.278]
select surf_pocket28, protein and id [627,629,640,569,570,600,512,2481,514,2482,639,537] 
set surface_color,  pcol28, surf_pocket28 
set_color pcol29 = [0.902,0.576,0.361]
select surf_pocket29, protein and id [5303,5257,5259,5270,4864,4857,5300,5302,5282,5283,5290,5271,8830,8829,8826,8827,8824,8825,8831,8832,4841] 
set surface_color,  pcol29, surf_pocket29 
set_color pcol30 = [0.702,0.490,0.278]
select surf_pocket30, protein and id [3896,3921,4026,4025,3958,3999,3995,3996,3997,3998,3895,3897,3899,3922,4035,4033,3860,4034,3898] 
set surface_color,  pcol30, surf_pocket30 
set_color pcol31 = [0.902,0.682,0.361]
select surf_pocket31, protein and id [2572,7511,7513,2574,2575,7510,7928,7923,7918,396,374,376,378,7512,812,813,861,862] 
set surface_color,  pcol31, surf_pocket31 
set_color pcol32 = [0.702,0.573,0.278]
select surf_pocket32, protein and id [913,348,351,352,7997,367,347,7953,7955,7959,7976,885,368,892,914,7987] 
set surface_color,  pcol32, surf_pocket32 
set_color pcol33 = [0.902,0.792,0.361]
select surf_pocket33, protein and id [6530,6703,6676,6749,6678,6923,6924] 
set surface_color,  pcol33, surf_pocket33 
set_color pcol34 = [0.702,0.659,0.278]
select surf_pocket34, protein and id [8417,8560,8555,8805,10518,8552,8561,10573,8804,8407] 
set surface_color,  pcol34, surf_pocket34 
set_color pcol35 = [0.902,0.902,0.361]
select surf_pocket35, protein and id [5241,10180,10182,10597,3481,3482,3531,10587,3530,10179,3065,3045,5243,5244,5245] 
set surface_color,  pcol35, surf_pocket35 
set_color pcol36 = [0.659,0.702,0.278]
select surf_pocket36, protein and id [10240,10242,10484,10486,10510,10649,8549] 
set surface_color,  pcol36, surf_pocket36 


deselect

orient
