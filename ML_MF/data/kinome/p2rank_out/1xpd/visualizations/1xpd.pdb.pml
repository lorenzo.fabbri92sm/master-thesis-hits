from pymol import cmd,stored

set depth_cue, 1
set fog_start, 0.4

set_color b_col, [36,36,85]
set_color t_col, [10,10,10]
set bg_rgb_bottom, b_col
set bg_rgb_top, t_col      
set bg_gradient

set  spec_power  =  200
set  spec_refl   =  0

load data/1xpd.pdb, protein
create ligands, protein and organic
select xlig, protein and organic
delete xlig

hide everything, all

color white, elem c
color bluewhite, protein
#show_as cartoon, protein
show surface, protein
#set transparency, 0.15

show sticks, ligands
set stick_color, magenta

load data/1xpd.pdb_points.pdb.gz, points
hide nonbonded, points
show nb_spheres, points
set sphere_scale, 0.2, points
cmd.spectrum("b", "green_red", selection="points", minimum=0, maximum=0.7)


stored.list=[]
cmd.iterate("(resn STP)","stored.list.append(resi)")    # read info about residues STP
lastSTP=stored.list[-1] # get the index of the last residue
hide lines, resn STP

cmd.select("rest", "resn STP and resi 0")

for my_index in range(1,int(lastSTP)+1): cmd.select("pocket"+str(my_index), "resn STP and resi "+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.show("spheres","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_scale","0.4","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_transparency","0.1","pocket"+str(my_index))



set_color pcol1 = [0.361,0.576,0.902]
select surf_pocket1, protein and id [1420,799,800,1344,1345,211,212,214,204,202,203,1417,792,180,183,188,768,781,773,795,798,782,210,383,233,235,209,626,623,625,743,384,517,1422,1423,622,1430,550,1439,1440,1441,176,234,173,174,175,179,753,754,765,368,759,177,761,163,821,822,817,1581,823] 
set surface_color,  pcol1, surf_pocket1 
set_color pcol2 = [0.278,0.298,0.702]
select surf_pocket2, protein and id [1292,1318,199,1563,1564,1546,1565,1452,1466,1467,1545,1429,1328,1290,1291,1329,1511,1512,1513,1514,1465,1277,200,1611,1610,1544,1649,1523,1524] 
set surface_color,  pcol2, surf_pocket2 
set_color pcol3 = [0.533,0.361,0.902]
select surf_pocket3, protein and id [17,24,664,711,80,676,63,39,560,525,527,523,500,502,501] 
set surface_color,  pcol3, surf_pocket3 
set_color pcol4 = [0.565,0.278,0.702]
select surf_pocket4, protein and id [2225,1923,2222,2223,1791,2059,2021,2045,2220,2221,2063,2053,1788,1646,1986,1821,1623] 
set surface_color,  pcol4, surf_pocket4 
set_color pcol5 = [0.902,0.361,0.878]
select surf_pocket5, protein and id [384,516,1451,400,408,409,459,456,1456,483,486,487,1458,217,220,380,382,206,207,208,454,491,493,488,199,1452,1426,211] 
set surface_color,  pcol5, surf_pocket5 
set_color pcol6 = [0.702,0.278,0.533]
select surf_pocket6, protein and id [1114,2398,2396,1113,1084,1082,1083,2399,2400,1151,1153,1141,1143] 
set surface_color,  pcol6, surf_pocket6 
set_color pcol7 = [0.902,0.361,0.490]
select surf_pocket7, protein and id [834,788,1375,789,1020,1022,1026,1377,1378,1379,1021,1045,1046,1050,832,1033,1042,1123,1125] 
set surface_color,  pcol7, surf_pocket7 
set_color pcol8 = [0.702,0.329,0.278]
select surf_pocket8, protein and id [752,1389,1390,319,654,747,756,1407,353,349,653,1357] 
set surface_color,  pcol8, surf_pocket8 
set_color pcol9 = [0.902,0.620,0.361]
select surf_pocket9, protein and id [1682,1688,1690,1693,1714,1995,1528,1996,1997,1685,1715] 
set surface_color,  pcol9, surf_pocket9 
set_color pcol10 = [0.702,0.631,0.278]
select surf_pocket10, protein and id [1860,1305,1319,1640,1593,816,1317,1561,1562,1316] 
set surface_color,  pcol10, surf_pocket10 


deselect

orient
