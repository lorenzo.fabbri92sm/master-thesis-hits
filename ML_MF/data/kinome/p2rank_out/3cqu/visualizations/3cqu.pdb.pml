from pymol import cmd,stored

set depth_cue, 1
set fog_start, 0.4

set_color b_col, [36,36,85]
set_color t_col, [10,10,10]
set bg_rgb_bottom, b_col
set bg_rgb_top, t_col      
set bg_gradient

set  spec_power  =  200
set  spec_refl   =  0

load data/3cqu.pdb, protein
create ligands, protein and organic
select xlig, protein and organic
delete xlig

hide everything, all

color white, elem c
color bluewhite, protein
#show_as cartoon, protein
show surface, protein
#set transparency, 0.15

show sticks, ligands
set stick_color, magenta

load data/3cqu.pdb_points.pdb.gz, points
hide nonbonded, points
show nb_spheres, points
set sphere_scale, 0.2, points
cmd.spectrum("b", "green_red", selection="points", minimum=0, maximum=0.7)


stored.list=[]
cmd.iterate("(resn STP)","stored.list.append(resi)")    # read info about residues STP
lastSTP=stored.list[-1] # get the index of the last residue
hide lines, resn STP

cmd.select("rest", "resn STP and resi 0")

for my_index in range(1,int(lastSTP)+1): cmd.select("pocket"+str(my_index), "resn STP and resi "+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.show("spheres","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_scale","0.4","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_transparency","0.1","pocket"+str(my_index))



set_color pcol1 = [0.361,0.576,0.902]
select surf_pocket1, protein and id [163,745,1125,1126,1154,743,744,746,2450,2452,2453,104,142,143,141,110,116,2490,2488,2680,2682,2684,1152,554,720,721,717,1235,1153,1233,1137,1244,286,288,443,123,124,126,130,138,140,139,1112,2706,1113,2690,2695,2701,162,105,107,269,699,711,695] 
set surface_color,  pcol1, surf_pocket1 
set_color pcol2 = [0.278,0.278,0.702]
select surf_pocket2, protein and id [2704,385,386,406,408,2718,2722,368,369,387,2708,1093,404,1257,1260,1264,1265,1267,402,306,307,410,418,416,341,347,442,129,132,134,2705,1240,1096,1242,1243,1244,290] 
set surface_color,  pcol2, surf_pocket2 
set_color pcol3 = [0.576,0.361,0.902]
select surf_pocket3, protein and id [2223,2224,2235,2225,2236,964,966,989,990,967,2231,2138,2139,1025,2111,2113,968,994,2272,2274,2271,2273,2270,2275,2276,991,1034] 
set surface_color,  pcol3, surf_pocket3 
set_color pcol4 = [0.616,0.278,0.702]
select surf_pocket4, protein and id [1271,1282,407,409,2726,399,400,397,433,1259,1263,2722,1347,1350,1380,1381,1368,1365,1086,1345] 
set surface_color,  pcol4, surf_pocket4 
set_color pcol5 = [0.902,0.361,0.792]
select surf_pocket5, protein and id [1315,1316,1321,1322,1324,1521,1522,2077,1525,1529,1499,1510,1511,1512,1514,1336,1493,2075,2073,2076,2036,2037] 
set surface_color,  pcol5, surf_pocket5 
set_color pcol6 = [0.702,0.278,0.447]
select surf_pocket6, protein and id [704,1178,1170,2293,2294,2295,1187,515,523,526,1218,1217,703,1167,701,702,708] 
set surface_color,  pcol6, surf_pocket6 
set_color pcol7 = [0.902,0.361,0.361]
select surf_pocket7, protein and id [2717,2713,2715,2731,1374,1375,1748,1746,1790,1791,1398,2716,2712,2714] 
set surface_color,  pcol7, surf_pocket7 
set_color pcol8 = [0.702,0.447,0.278]
select surf_pocket8, protein and id [68,69,70,71,72,73,74,22,23,25,653,171,155,299] 
set surface_color,  pcol8, surf_pocket8 
set_color pcol9 = [0.902,0.792,0.361]
select surf_pocket9, protein and id [2205,2213,901,902,875,915,911,912,913,2316,2331,2313,2309,2310,2311,2312] 
set surface_color,  pcol9, surf_pocket9 


deselect

orient
