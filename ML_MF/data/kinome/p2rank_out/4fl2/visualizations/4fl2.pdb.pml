from pymol import cmd,stored

set depth_cue, 1
set fog_start, 0.4

set_color b_col, [36,36,85]
set_color t_col, [10,10,10]
set bg_rgb_bottom, b_col
set bg_rgb_top, t_col      
set bg_gradient

set  spec_power  =  200
set  spec_refl   =  0

load data/4fl2.pdb, protein
create ligands, protein and organic
select xlig, protein and organic
delete xlig

hide everything, all

color white, elem c
color bluewhite, protein
#show_as cartoon, protein
show surface, protein
#set transparency, 0.15

show sticks, ligands
set stick_color, magenta

load data/4fl2.pdb_points.pdb.gz, points
hide nonbonded, points
show nb_spheres, points
set sphere_scale, 0.2, points
cmd.spectrum("b", "green_red", selection="points", minimum=0, maximum=0.7)


stored.list=[]
cmd.iterate("(resn STP)","stored.list.append(resi)")    # read info about residues STP
lastSTP=stored.list[-1] # get the index of the last residue
hide lines, resn STP

cmd.select("rest", "resn STP and resi 0")

for my_index in range(1,int(lastSTP)+1): cmd.select("pocket"+str(my_index), "resn STP and resi "+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.show("spheres","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_scale","0.4","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_transparency","0.1","pocket"+str(my_index))



set_color pcol1 = [0.361,0.576,0.902]
select surf_pocket1, protein and id [3011,2385,2386,2387,2433,2435,2392,2391,2395,2935,3457,3459,3460,2573,2574,2954,2952,2382,2384,2434,2379,2380,2381,2940,2558,2936,3610,3626,3628,3629,3561,3611,2416,3319,2414,2396,2397,2399,2400,2403,3613,2984,2985,3000,3002,3341,3342,3343,3344,2977,2986,3371,2957,3010,3370,3452,3355,3356,3678,3957,3345,3625,3316] 
set surface_color,  pcol1, surf_pocket1 
set_color pcol2 = [0.278,0.365,0.702]
select surf_pocket2, protein and id [1842,1844,1847,1876,1878,909,910,1833,1869,1870,1871,908,1867,1868,1872,1910,1873,1169,1198,1170,1174,1162,1163,1836,296,847,104,89,133,1920,1139,4463,4471,4470,1137,1164,1166,1167] 
set surface_color,  pcol2, surf_pocket2 
set_color pcol3 = [0.361,0.361,0.902]
select surf_pocket3, protein and id [1876,1781,1878,1799,1842,1843,1868,1872,1910,1873,1869,1871,126,128,164,166,1920,152,155,191,129,130,131,133,96,1908,132] 
set surface_color,  pcol3, surf_pocket3 
set_color pcol4 = [0.365,0.278,0.702]
select surf_pocket4, protein and id [3678,3679,3955,3956,3957,3965,3622,3625,3630,3000,3002,3342,3343,3902,3344,3940,3626] 
set surface_color,  pcol4, surf_pocket4 
set_color pcol5 = [0.576,0.361,0.902]
select surf_pocket5, protein and id [452,448,437,438,443,428,429,451,470,4168,4169,4172,542,545,548,544,4199,4374,4167,4205,4372,4376,4377,4378,4379,4197,4409,453,4413,4405,4415] 
set surface_color,  pcol5, surf_pocket5 
set_color pcol6 = [0.533,0.278,0.702]
select surf_pocket6, protein and id [1111,1088,1090,1110,1057,1058,1059,1077,3196,3197,3193,4360,4361,3218,4359,3219,3220,3258,1082,1089,3195,2783,3257,3227,3221,1087,1055,1056] 
set surface_color,  pcol6, surf_pocket6 
set_color pcol7 = [0.792,0.361,0.902]
select surf_pocket7, protein and id [3143,1148,1151,3118,3411,3410,3412,3112,3113,3116,3107,3105,3106,2068,3091,1147,2080,4465,4474,2063,2067] 
set surface_color,  pcol7, surf_pocket7 
set_color pcol8 = [0.702,0.278,0.702]
select surf_pocket8, protein and id [328,330,1,2,5,66,343,345,344,327,281,278,279,280,854,6,18] 
set surface_color,  pcol8, surf_pocket8 
set_color pcol9 = [0.902,0.361,0.792]
select surf_pocket9, protein and id [244,630,765,607,637,638,639,626,628,667,54,55,37,52,764] 
set surface_color,  pcol9, surf_pocket9 
set_color pcol10 = [0.702,0.278,0.533]
select surf_pocket10, protein and id [2918,2920,3482,2935,3454,3459,3471,3469,2740,2834,2934] 
set surface_color,  pcol10, surf_pocket10 
set_color pcol11 = [0.902,0.361,0.576]
select surf_pocket11, protein and id [1243,1253,1245,1240,1210,1211,1212,1192,931,935,959,970,972,997,1000,998,964,965,968] 
set surface_color,  pcol11, surf_pocket11 
set_color pcol12 = [0.702,0.278,0.365]
select surf_pocket12, protein and id [3752,3753,3754,3756,3755,3757,3758,3517,3575,3526,3572,3574,3541,3543,3308,3309,3491,3494,3495,3487,3513,3514,3776,3778,3751] 
set surface_color,  pcol12, surf_pocket12 
set_color pcol13 = [0.902,0.361,0.361]
select surf_pocket13, protein and id [1236,1519,1693,1692,1511,1550,1552,1225,1226,1227,1551,1737,1721,1235] 
set surface_color,  pcol13, surf_pocket13 
set_color pcol14 = [0.702,0.365,0.278]
select surf_pocket14, protein and id [1327,1489,1558,1329,1331,1639,1640,1328,1642,1502,1503,1504,1663] 
set surface_color,  pcol14, surf_pocket14 
set_color pcol15 = [0.902,0.576,0.361]
select surf_pocket15, protein and id [1109,1111,4397,4398,1102,1080,952,1131,1099,314,316,919,1132,302,4442] 
set surface_color,  pcol15, surf_pocket15 
set_color pcol16 = [0.702,0.533,0.278]
select surf_pocket16, protein and id [2485,2479,2317,2266,2282,2235,2232,2170,2172,2215,2211,2492,2495,2499,2237] 
set surface_color,  pcol16, surf_pocket16 
set_color pcol17 = [0.902,0.792,0.361]
select surf_pocket17, protein and id [1781,1880,1919,1779,1780,1932,1933,1920] 
set surface_color,  pcol17, surf_pocket17 
set_color pcol18 = [0.702,0.702,0.278]
select surf_pocket18, protein and id [3678,3679,3955,3956,3965,3622,3625,3630,3664,3666] 
set surface_color,  pcol18, surf_pocket18 


deselect

orient
