from pymol import cmd,stored

set depth_cue, 1
set fog_start, 0.4

set_color b_col, [36,36,85]
set_color t_col, [10,10,10]
set bg_rgb_bottom, b_col
set bg_rgb_top, t_col      
set bg_gradient

set  spec_power  =  200
set  spec_refl   =  0

load data/3l9p.pdb, protein
create ligands, protein and organic
select xlig, protein and organic
delete xlig

hide everything, all

color white, elem c
color bluewhite, protein
#show_as cartoon, protein
show surface, protein
#set transparency, 0.15

show sticks, ligands
set stick_color, magenta

load data/3l9p.pdb_points.pdb.gz, points
hide nonbonded, points
show nb_spheres, points
set sphere_scale, 0.2, points
cmd.spectrum("b", "green_red", selection="points", minimum=0, maximum=0.7)


stored.list=[]
cmd.iterate("(resn STP)","stored.list.append(resi)")    # read info about residues STP
lastSTP=stored.list[-1] # get the index of the last residue
hide lines, resn STP

cmd.select("rest", "resn STP and resi 0")

for my_index in range(1,int(lastSTP)+1): cmd.select("pocket"+str(my_index), "resn STP and resi "+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.show("spheres","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_scale","0.4","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_transparency","0.1","pocket"+str(my_index))



set_color pcol1 = [0.361,0.576,0.902]
select surf_pocket1, protein and id [244,1210,1225,1331,1332,1333,1334,1188,1213,1214,1239,282,375,789,225,281,786,227,228,795,232,243,265,266,224,1237,1238,1326,772,790,807,811,814,804,796,797,523,387,388,524,497,270,767,255,256,1463,1465,257,259,261,262,464,260,410,1347,1348,1350,525,1349,1371,1379,1380,1381,491,486,490,460,1357,1444,1495,1491] 
set surface_color,  pcol1, surf_pocket1 
set_color pcol2 = [0.365,0.278,0.702]
select surf_pocket2, protein and id [1441,1472,1473,1501,1502,1503,1471,1548,1630,1632,1387,1356,1438,1389,1423,1426,1429,1434,1437,1176,1178,1628,1388,1575,1611,1578,1580,1173,1355] 
set surface_color,  pcol2, surf_pocket2 
set_color pcol3 = [0.792,0.361,0.902]
select surf_pocket3, protein and id [36,40,1391,1394,1397,1399,1155,1150,1151,46,1148,1152,1361,1390,1134,1126,1177,1178,1388,1385] 
set surface_color,  pcol3, surf_pocket3 
set_color pcol4 = [0.702,0.278,0.533]
select surf_pocket4, protein and id [1061,1084,2235,601,614,616,627,1064,599,618,1030,1031,1032,2272,1085] 
set surface_color,  pcol4, surf_pocket4 
set_color pcol5 = [0.902,0.361,0.361]
select surf_pocket5, protein and id [1479,1481,1877,1902,1578,1580,1517,1502,1866] 
set surface_color,  pcol5, surf_pocket5 
set_color pcol6 = [0.702,0.533,0.278]
select surf_pocket6, protein and id [1778,1781,1780,1800,1801,1802,1803,1805,1804,1989,1995,1746,1839,1984,1999] 
set surface_color,  pcol6, surf_pocket6 


deselect

orient
