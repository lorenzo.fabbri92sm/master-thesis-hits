from pymol import cmd,stored

set depth_cue, 1
set fog_start, 0.4

set_color b_col, [36,36,85]
set_color t_col, [10,10,10]
set bg_rgb_bottom, b_col
set bg_rgb_top, t_col      
set bg_gradient

set  spec_power  =  200
set  spec_refl   =  0

load data/1qzj.pdb, protein
create ligands, protein and organic
select xlig, protein and organic
delete xlig

hide everything, all

color white, elem c
color bluewhite, protein
#show_as cartoon, protein
show surface, protein
#set transparency, 0.15

show sticks, ligands
set stick_color, magenta

load data/1qzj.pdb_points.pdb.gz, points
hide nonbonded, points
show nb_spheres, points
set sphere_scale, 0.2, points
cmd.spectrum("b", "green_red", selection="points", minimum=0, maximum=0.7)


stored.list=[]
cmd.iterate("(resn STP)","stored.list.append(resi)")    # read info about residues STP
lastSTP=stored.list[-1] # get the index of the last residue
hide lines, resn STP

cmd.select("rest", "resn STP and resi 0")

for my_index in range(1,int(lastSTP)+1): cmd.select("pocket"+str(my_index), "resn STP and resi "+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.show("spheres","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_scale","0.4","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_transparency","0.1","pocket"+str(my_index))



set_color pcol1 = [0.361,0.576,0.902]
select surf_pocket1, protein and id [3704,3703,727,728,729,1002,1003,1004,1005,3674,943,944,997,998,1001,1076,1075,988,989,1057,1058,1059,1125,3729,3730,3706,395,397,3733,3746,3747,394,396,726,746,956,951,767,954,942,950,408,1457,1232,1215,1233,3686,441,3492,3685,1506,340,3661,3684,3671,3675,3676,3677,3678,3385,3382,4022,3340,1127,1214,3238,3344,3346,3338,3362,3321,3318,3308,3311,3312,3437,3692,3739,3744,3967,3973,3975,3976,3997,3998,4002,4004,3665,3664,1212,3644] 
set surface_color,  pcol1, surf_pocket1 
set_color pcol2 = [0.278,0.380,0.702]
select surf_pocket2, protein and id [3689,3691,3717,1587,1609,3433,3821,3822,3823,3493,3693,3817,3820,3435,3781,1606,1607,1608,1527,1558,1546,3686,336,337,441,3492,323,1516,340,341,1588,1590,4054,3847,4079,4080,3794,3796,3816] 
set surface_color,  pcol2, surf_pocket2 
set_color pcol3 = [0.361,0.408,0.902]
select surf_pocket3, protein and id [2414,2207,2212,2213,2199,2208,2221,2227,2228,2200,2201,2598,2307,2277,2427,2426,2441,2444,2599,2363,2365,1752,2191,1753,1754,1756,2190,2176,2177,2178,2841,2278,1725,1728,1765,1755,1729,1732,1733,1681,1682,1704] 
set surface_color,  pcol3, surf_pocket3 
set_color pcol4 = [0.310,0.278,0.702]
select surf_pocket4, protein and id [2818,2819,2821,2837,2857,2858,2808,2809,1798,2852,2853,2866,1762,1772,2859,1792,1797,1799,2191,2189,2190,2188,2043,1765] 
set surface_color,  pcol4, surf_pocket4 
set_color pcol5 = [0.482,0.361,0.902]
select surf_pocket5, protein and id [1710,1711,1985,1986,1987,1989,1990,1988,2041,2042,1725,1728,1766,1920,1906,1908,1955,1704,1707,1708] 
set surface_color,  pcol5, surf_pocket5 
set_color pcol6 = [0.439,0.278,0.702]
select surf_pocket6, protein and id [1103,1104,1106,1108,1105,1107,1109,1053,1054,1058,1033,1034,3318,3274,3296,3308,3311,3312,3297,3321] 
set surface_color,  pcol6, surf_pocket6 
set_color pcol7 = [0.651,0.361,0.902]
select surf_pocket7, protein and id [1710,1711,1989,1990,2062,1988,3547,2060,1624,1626,1532,1534,2081,2083] 
set surface_color,  pcol7, surf_pocket7 
set_color pcol8 = [0.573,0.278,0.702]
select surf_pocket8, protein and id [3893,3894,4034,3925,3926,3927,3928,4099,4164,4098,4778,4779,4722,4780,4781,4782,4165,4003,4004,4725] 
set surface_color,  pcol8, surf_pocket8 
set_color pcol9 = [0.820,0.361,0.902]
select surf_pocket9, protein and id [2111,5010,5026,2128,2143,2156,5025,1730,1731,2109,2164,2173,2174,2177,2840,2841,2155,2154,2162,2163,2176,2167] 
set surface_color,  pcol9, surf_pocket9 
set_color pcol10 = [0.702,0.278,0.698]
select surf_pocket10, protein and id [1517,1491,1518,3542,3543,3545,1490,1530,1532,304,1515,1516,341,476,3547] 
set surface_color,  pcol10, surf_pocket10 
set_color pcol11 = [0.902,0.361,0.812]
select surf_pocket11, protein and id [2129,2092,2095,3066,2960,3018,4561,4565,1571,4509,4511,4513,4515,1643,2076,3068,3069,3070,3512,1642,2075,2938] 
set surface_color,  pcol11, surf_pocket11 
set_color pcol12 = [0.702,0.278,0.565]
select surf_pocket12, protein and id [5385,3205,5401,5405,5406,5407,5409,3202,3211,3254,3203,3204,3260,3261,3264,3265,3252,3253] 
set surface_color,  pcol12, surf_pocket12 
set_color pcol13 = [0.902,0.361,0.643]
select surf_pocket13, protein and id [2395,2294,2396,2397,2259,2260,2429,2430,2256,2258,2393] 
set surface_color,  pcol13, surf_pocket13 
set_color pcol14 = [0.702,0.278,0.435]
select surf_pocket14, protein and id [4247,4004,3924,4099,4151,4213,4216,4217,4249,4215,3925,3926,3927,3928] 
set surface_color,  pcol14, surf_pocket14 
set_color pcol15 = [0.902,0.361,0.475]
select surf_pocket15, protein and id [2111,1731,1732,1733,2173,2177] 
set surface_color,  pcol15, surf_pocket15 
set_color pcol16 = [0.702,0.278,0.302]
select surf_pocket16, protein and id [1178,1181,1182,1183,3120,3162,3163,1177,1200,1167,1168,1169] 
set surface_color,  pcol16, surf_pocket16 
set_color pcol17 = [0.902,0.416,0.361]
select surf_pocket17, protein and id [1093,1017,1037,1092,1019,1039,112,114,59,37,23,36,55,62] 
set surface_color,  pcol17, surf_pocket17 
set_color pcol18 = [0.702,0.388,0.278]
select surf_pocket18, protein and id [3555,3558,3556,1484,1470,3626,3627,1474,3528,3530,1184,3529,3591,3593] 
set surface_color,  pcol18, surf_pocket18 
set_color pcol19 = [0.902,0.584,0.361]
select surf_pocket19, protein and id [3797,3796,3819,3875,3877,3909,3910,3870,3795,3981,3854,3873,3855,3856,3853] 
set surface_color,  pcol19, surf_pocket19 
set_color pcol20 = [0.702,0.522,0.278]
select surf_pocket20, protein and id [3542,3543,1488,1490,3544,3555,669,1484,1485,668] 
set surface_color,  pcol20, surf_pocket20 
set_color pcol21 = [0.902,0.753,0.361]
select surf_pocket21, protein and id [5385,5381,3205,5401,5405,4301,5371,5373,3202,3254,3204,3264] 
set surface_color,  pcol21, surf_pocket21 
set_color pcol22 = [0.702,0.651,0.278]
select surf_pocket22, protein and id [3054,2992,3053,5587,5589,5591,2990,2991,5608,3100,3101,5586,5603,5604,3005] 
set surface_color,  pcol22, surf_pocket22 
set_color pcol23 = [0.878,0.902,0.361]
select surf_pocket23, protein and id [1964,1965,1966,1968,1892,1893,1894,1933,1934,1878,1879,1873,1880,2006] 
set surface_color,  pcol23, surf_pocket23 


deselect

orient
