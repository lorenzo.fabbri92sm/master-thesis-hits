from pymol import cmd,stored

set depth_cue, 1
set fog_start, 0.4

set_color b_col, [36,36,85]
set_color t_col, [10,10,10]
set bg_rgb_bottom, b_col
set bg_rgb_top, t_col      
set bg_gradient

set  spec_power  =  200
set  spec_refl   =  0

load data/2x8d.pdb, protein
create ligands, protein and organic
select xlig, protein and organic
delete xlig

hide everything, all

color white, elem c
color bluewhite, protein
#show_as cartoon, protein
show surface, protein
#set transparency, 0.15

show sticks, ligands
set stick_color, magenta

load data/2x8d.pdb_points.pdb.gz, points
hide nonbonded, points
show nb_spheres, points
set sphere_scale, 0.2, points
cmd.spectrum("b", "green_red", selection="points", minimum=0, maximum=0.7)


stored.list=[]
cmd.iterate("(resn STP)","stored.list.append(resi)")    # read info about residues STP
lastSTP=stored.list[-1] # get the index of the last residue
hide lines, resn STP

cmd.select("rest", "resn STP and resi 0")

for my_index in range(1,int(lastSTP)+1): cmd.select("pocket"+str(my_index), "resn STP and resi "+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.show("spheres","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_scale","0.4","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_transparency","0.1","pocket"+str(my_index))



set_color pcol1 = [0.361,0.576,0.902]
select surf_pocket1, protein and id [122,129,130,145,147,148,135,138,167,168,169,505,506,656,663,1158,266,651,433,652,282,378,401,402,371,372,375,139,142,296,136,323,345,346,432,1078,1077,111,701,117,118,120,110,112,116,668,674,677,114,115,137,1061,1159,1160,1163,1164,1165,1062,1166,1162,1179,1180,1181,365,366,367,370,1173,1175,1352,1018,1020,1188,1186,1187,1189,1049,1050,1037,1038,691] 
set surface_color,  pcol1, surf_pocket1 
set_color pcol2 = [0.278,0.278,0.702]
select surf_pocket2, protein and id [747,748,1593,1044,714,718,719,720,721,1634,1619,744,759,763,1617,1618,1054,1379,1628,1632,1633,1635,1620] 
set surface_color,  pcol2, surf_pocket2 
set_color pcol3 = [0.576,0.361,0.902]
select surf_pocket3, protein and id [272,620,621,644,560,561,562,622,623,624,583,82,83,84,290,273,41,61,58,59] 
set surface_color,  pcol3, surf_pocket3 
set_color pcol4 = [0.616,0.278,0.702]
select surf_pocket4, protein and id [532,533,535,638,639,63,66,641,640,643,531,248,249,642,644,34,206,534,530] 
set surface_color,  pcol4, surf_pocket4 
set_color pcol5 = [0.902,0.361,0.792]
select surf_pocket5, protein and id [448,449,450,419,415,416,418,388,390,423,425,985,392,1208,1239] 
set surface_color,  pcol5, surf_pocket5 
set_color pcol6 = [0.702,0.278,0.447]
select surf_pocket6, protein and id [552,553,613,409,382,410,380,383,384,572,442,569] 
set surface_color,  pcol6, surf_pocket6 
set_color pcol7 = [0.902,0.361,0.361]
select surf_pocket7, protein and id [1144,658,659,660,1093,1111,1126,1101,1102,1103,665] 
set surface_color,  pcol7, surf_pocket7 
set_color pcol8 = [0.702,0.447,0.278]
select surf_pocket8, protein and id [2013,1483,1466,1485,2003,1999,2007,2005,2006,1439,1459,2004,1476] 
set surface_color,  pcol8, surf_pocket8 
set_color pcol9 = [0.902,0.792,0.361]
select surf_pocket9, protein and id [1968,1974,1941,1979,1790,1757,1759,1754,1760,1762,1768,1788,1766,1791,1942,1945] 
set surface_color,  pcol9, surf_pocket9 


deselect

orient
