from pymol import cmd,stored

set depth_cue, 1
set fog_start, 0.4

set_color b_col, [36,36,85]
set_color t_col, [10,10,10]
set bg_rgb_bottom, b_col
set bg_rgb_top, t_col      
set bg_gradient

set  spec_power  =  200
set  spec_refl   =  0

load data/3brh.pdb, protein
create ligands, protein and organic
select xlig, protein and organic
delete xlig

hide everything, all

color white, elem c
color bluewhite, protein
#show_as cartoon, protein
show surface, protein
#set transparency, 0.15

show sticks, ligands
set stick_color, magenta

load data/3brh.pdb_points.pdb.gz, points
hide nonbonded, points
show nb_spheres, points
set sphere_scale, 0.2, points
cmd.spectrum("b", "green_red", selection="points", minimum=0, maximum=0.7)


stored.list=[]
cmd.iterate("(resn STP)","stored.list.append(resi)")    # read info about residues STP
lastSTP=stored.list[-1] # get the index of the last residue
hide lines, resn STP

cmd.select("rest", "resn STP and resi 0")

for my_index in range(1,int(lastSTP)+1): cmd.select("pocket"+str(my_index), "resn STP and resi "+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.show("spheres","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_scale","0.4","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_transparency","0.1","pocket"+str(my_index))



set_color pcol1 = [0.361,0.576,0.902]
select surf_pocket1, protein and id [1568,1053,3515,3641,1177,1179,1316,1178,4022,3639,3640,3853,3637,3661,3648,3647,1335,1380,1546,1317,1397,1378,1341,1567,1564,1565,1566,3645,1185,1186,1175,1183,1197,1199,3506,3625,4000,3511,3512,3507,3790,3791,3834,3836,4020,3772,1334,1333,3820,3797,3811,3995,4018,4019,3812,4014,1050,1163,1049,1045] 
set surface_color,  pcol1, surf_pocket1 
set_color pcol2 = [0.278,0.380,0.702]
select surf_pocket2, protein and id [3590,3566,3563,3562,3548,3310,3589,3331,3309,3701,3684,3687,3699,1052,1126,1127,1128,1129,1133,1124,1103,1104,1060,1062,1064,1100,1102,1096,1086,1132,3526,3524,1240,1242,1225,1228,847,848,846,869,3514,3595,3594,3680] 
set surface_color,  pcol2, surf_pocket2 
set_color pcol3 = [0.361,0.400,0.902]
select surf_pocket3, protein and id [2677,4740,2641,4548,4551,4553,4560,4564,4772,4774,4776,2653,2602,2604,4739,2603,2640,4559,4557,4539,2555,2557,2526,2559,2560,2554,2582,2553,2533,2529,2531] 
set surface_color,  pcol3, surf_pocket3 
set_color pcol4 = [0.318,0.278,0.702]
select surf_pocket4, protein and id [2316,133,165,166,61,63,65,2081,2095,2090,2093,86,88,89,90,162,197,198,163,164,200,2282,2101,2314,2102,2281,114,95,2318,91,92,58,2094,199,2106] 
set surface_color,  pcol4, surf_pocket4 
set_color pcol5 = [0.498,0.361,0.902]
select surf_pocket5, protein and id [357,363,529,341,301,304,305,308,351,267,268,272,269,300,289,292,293,295,277,531,290,291,362,390,391,510,511,512,2208,2205,2171,2213,4968,270,271,243,241,242,244,240,4970,4971,494,491,493] 
set surface_color,  pcol5, surf_pocket5 
set_color pcol6 = [0.455,0.278,0.702]
select surf_pocket6, protein and id [3021,3023,2746,2748,4663,2717,2747,2749,2769,2777,2778,2816,2719,2715,2718,4629,4666,4670,4671,2688,2720,4690,4692,2838,3001,3002,2782,2783,2837,2985,2989,5026,2998] 
set surface_color,  pcol6, surf_pocket6 
set_color pcol7 = [0.675,0.361,0.902]
select surf_pocket7, protein and id [4251,4252,4258,4261,4262,4263,4264,3217,4250,3215,4210,3244,3253,3254,3255,3207,4417,4456,3198,3199,3220,3223,3201,3208,3243,4292] 
set surface_color,  pcol7, surf_pocket7 
set_color pcol8 = [0.592,0.278,0.702]
select surf_pocket8, protein and id [4427,4428,4466,3204,4610,4611,4612,4583,4542,4581,4573,4578,4547,4550,4577,4575,4465,4544,4534,4499,4517,4518,4537,4541] 
set surface_color,  pcol8, surf_pocket8 
set_color pcol9 = [0.851,0.361,0.902]
select surf_pocket9, protein and id [2153,2154,2115,2116,2117,2119,2120,2011,2012,2063,2064,2086,2092,2084,2089,2045,1969,2125,1968,2123,742] 
set surface_color,  pcol9, surf_pocket9 
set_color pcol10 = [0.702,0.278,0.671]
select surf_pocket10, protein and id [245,4985,244,182,2261,2245,2249,2250,2253,183,208,4983,4984] 
set surface_color,  pcol10, surf_pocket10 
set_color pcol11 = [0.902,0.361,0.773]
select surf_pocket11, protein and id [1346,1388,1771,1772,1773,1774,1775,1776,1763,1764,1765,1767,1405,1406,1514,1515,1516,1740] 
set surface_color,  pcol11, surf_pocket11 
set_color pcol12 = [0.702,0.278,0.533]
select surf_pocket12, protein and id [736,781,782,739,745,746,750,753,755,1999,793,1965,1799,1798,1804,1808,1809,1811,1839,1757,2003,2001] 
set surface_color,  pcol12, surf_pocket12 
set_color pcol13 = [0.902,0.361,0.596]
select surf_pocket13, protein and id [4183,4185,4849,4847,4448,4445,4446,4861,4822,4851,4143,4169,4449,4171,4447,4141,4144,4147,4177,4142] 
set surface_color,  pcol13, surf_pocket13 
set_color pcol14 = [0.702,0.278,0.396]
select surf_pocket14, protein and id [477,1112,1113,4960,4962,4942,1602,1607,1609,4964,4957,4979,1092,1094,1069,1079,1080,1070,1073,1601,1081] 
set surface_color,  pcol14, surf_pocket14 
set_color pcol15 = [0.902,0.361,0.420]
select surf_pocket15, protein and id [4183,4185,4180,4182,4184,4448,4480,4481,4247,4248,4449,4218] 
set surface_color,  pcol15, surf_pocket15 
set_color pcol16 = [0.702,0.302,0.278]
select surf_pocket16, protein and id [1723,1760,1761,1765,1989,1725,1727,1729,1730,1731,1721,1732,2029,2027,1751,1752,1753,1985,1987] 
set surface_color,  pcol16, surf_pocket16 
set_color pcol17 = [0.902,0.478,0.361]
select surf_pocket17, protein and id [3542,4056,3531,3557,3574,3575,2953,4063,5029,5031,5011,5001] 
set surface_color,  pcol17, surf_pocket17 
set_color pcol18 = [0.702,0.439,0.278]
select surf_pocket18, protein and id [1868,1870,1869,1876,1880,1603,2236,4964,2271,4962,1902,1599] 
set surface_color,  pcol18, surf_pocket18 
set_color pcol19 = [0.902,0.655,0.361]
select surf_pocket19, protein and id [4170,4416,4200,4292,4386,4201,4291,4414,4415,3253,4304,4306] 
set surface_color,  pcol19, surf_pocket19 
set_color pcol20 = [0.702,0.576,0.278]
select surf_pocket20, protein and id [4226,4216,4224,4220,4222,3968,3970,4225,4227,4229,3969] 
set surface_color,  pcol20, surf_pocket20 
set_color pcol21 = [0.902,0.831,0.361]
select surf_pocket21, protein and id [1717,1930,1718,1956,1957,1838,1747,1839,1853] 
set surface_color,  pcol21, surf_pocket21 
set_color pcol22 = [0.686,0.702,0.278]
select surf_pocket22, protein and id [2218,2228,2161] 
set surface_color,  pcol22, surf_pocket22 


deselect

orient
