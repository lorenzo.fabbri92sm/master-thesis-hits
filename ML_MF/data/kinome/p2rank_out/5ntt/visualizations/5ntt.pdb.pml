from pymol import cmd,stored

set depth_cue, 1
set fog_start, 0.4

set_color b_col, [36,36,85]
set_color t_col, [10,10,10]
set bg_rgb_bottom, b_col
set bg_rgb_top, t_col      
set bg_gradient

set  spec_power  =  200
set  spec_refl   =  0

load data/5ntt.pdb, protein
create ligands, protein and organic
select xlig, protein and organic
delete xlig

hide everything, all

color white, elem c
color bluewhite, protein
#show_as cartoon, protein
show surface, protein
#set transparency, 0.15

show sticks, ligands
set stick_color, magenta

load data/5ntt.pdb_points.pdb.gz, points
hide nonbonded, points
show nb_spheres, points
set sphere_scale, 0.2, points
cmd.spectrum("b", "green_red", selection="points", minimum=0, maximum=0.7)


stored.list=[]
cmd.iterate("(resn STP)","stored.list.append(resi)")    # read info about residues STP
lastSTP=stored.list[-1] # get the index of the last residue
hide lines, resn STP

cmd.select("rest", "resn STP and resi 0")

for my_index in range(1,int(lastSTP)+1): cmd.select("pocket"+str(my_index), "resn STP and resi "+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.show("spheres","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_scale","0.4","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_transparency","0.1","pocket"+str(my_index))



set_color pcol1 = [0.361,0.576,0.902]
select surf_pocket1, protein and id [350,203,207,213,214,369,189,199,191,192,195,196,208,374,1186,1187,824,1214,1281,1283,1282,351,230,1215,1188,1441,1440,1436,628,759,760,775,186,229,180,182,184,334,804,799,800,803,809,797,1333,1334,1335,1318,1319,472,474,1323,1329,470,1337,1340,1342,405,409,398,1341,1196,1176,1290,1288,1289,1291,1304,1336,1157,1159,1175,1438,502,503,499,500,501,1156,1306] 
set surface_color,  pcol1, surf_pocket1 
set_color pcol2 = [0.302,0.278,0.702]
select surf_pocket2, protein and id [221,223,240,241,150,68,70,71,749,751,750,61,25,54,377,361,362,358,360,359,716,719,728,730,731] 
set surface_color,  pcol2, surf_pocket2 
set_color pcol3 = [0.631,0.361,0.902]
select surf_pocket3, protein and id [134,135,766,652,667,111,107,281,273,653,654,656,308,309,326,328,664] 
set surface_color,  pcol3, surf_pocket3 
set_color pcol4 = [0.678,0.278,0.702]
select surf_pocket4, protein and id [855,815,854,856,857,858,859,1222,1239,1240,991,993,1242,953,954,955,956] 
set surface_color,  pcol4, surf_pocket4 
set_color pcol5 = [0.902,0.361,0.682]
select surf_pocket5, protein and id [1595,1596,2148,2161,2162,2143,2146,1586,1587,1091,1094,1092,1069,1071,2152] 
set surface_color,  pcol5, surf_pocket5 
set_color pcol6 = [0.702,0.278,0.341]
select surf_pocket6, protein and id [974,2260,2253,2254,1017,972,940,942,945,937,977,978,981,982,1015,2225,2231,2232,2240,2241] 
set surface_color,  pcol6, surf_pocket6 
set_color pcol7 = [0.902,0.522,0.361]
select surf_pocket7, protein and id [876,1768,1770,1695,1434,839,1694,1423,1430,1427,1471,1769,1480,1482,1743] 
set surface_color,  pcol7, surf_pocket7 
set_color pcol8 = [0.702,0.596,0.278]
select surf_pocket8, protein and id [657,653,654,656,658,305,308,309,326,328,785,313,652,781,784,634] 
set surface_color,  pcol8, surf_pocket8 


deselect

orient
