from pymol import cmd,stored

set depth_cue, 1
set fog_start, 0.4

set_color b_col, [36,36,85]
set_color t_col, [10,10,10]
set bg_rgb_bottom, b_col
set bg_rgb_top, t_col      
set bg_gradient

set  spec_power  =  200
set  spec_refl   =  0

load data/2esm.pdb, protein
create ligands, protein and organic
select xlig, protein and organic
delete xlig

hide everything, all

color white, elem c
color bluewhite, protein
#show_as cartoon, protein
show surface, protein
#set transparency, 0.15

show sticks, ligands
set stick_color, magenta

load data/2esm.pdb_points.pdb.gz, points
hide nonbonded, points
show nb_spheres, points
set sphere_scale, 0.2, points
cmd.spectrum("b", "green_red", selection="points", minimum=0, maximum=0.7)


stored.list=[]
cmd.iterate("(resn STP)","stored.list.append(resi)")    # read info about residues STP
lastSTP=stored.list[-1] # get the index of the last residue
hide lines, resn STP

cmd.select("rest", "resn STP and resi 0")

for my_index in range(1,int(lastSTP)+1): cmd.select("pocket"+str(my_index), "resn STP and resi "+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.show("spheres","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_scale","0.4","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_transparency","0.1","pocket"+str(my_index))



set_color pcol1 = [0.361,0.576,0.902]
select surf_pocket1, protein and id [1733,1626,1636,1613,1614,837,820,821,1734,1735,1736,1728,1234,1235,804,1239,1257,1652,1653,691,692,693,1625,1286,643,640,641,1287,1629,1301,1307,2931,2936,2948,2941,2945,2925,2938,634,638,1251,1752,1753,1758,1597,1757,935,934,1751,937,936,938,1749,1732,670,672,658,661,662,655,677,645,646,2963,1876,910,939,906,908,673,923,924,1877,917] 
set surface_color,  pcol1, surf_pocket1 
set_color pcol2 = [0.278,0.376,0.702]
select surf_pocket2, protein and id [45,78,86,88,111,109,77,3362,51,53,3431,3445,3370,3390,3391,6452,6453,6454,6455,6457,4383,48,49,50,87,52,54,6473,6474,6464,118,44,40,41,3779,3799,3802,12,14,16,4385,35,37,36,15,25,3785,3789,3786,3795,6459,3796,3436,3437,3443,3444,3368,3372,3373,3371] 
set surface_color,  pcol2, surf_pocket2 
set_color pcol3 = [0.361,0.392,0.902]
select surf_pocket3, protein and id [4493,3945,3946,3947,4911,3888,3894,6206,4884,4883,4544,4545,4559,4561,6203,6207,6208,6210,4992,4894,4986,4993,4492,4887,6226,3900,3895,3897,6224,4994,4058,4910,6202,4497,3892,6198,6200,4565] 
set surface_color,  pcol3, surf_pocket3 
set_color pcol4 = [0.329,0.278,0.702]
select surf_pocket4, protein and id [4175,4193,3926,3927,5140,5138,5139,5128,3913,3917,3924,3909,3910,3912,4159,5146,4176,5124,5016,4854,4855,4895,4856,4871,4872,5006,5007,4194,4196,4241,4074,4075,4091,4994,3916,3899,3900,3908,3931,4990,4991,4894,4993,3947,5009,5008,5015] 
set surface_color,  pcol4, surf_pocket4 
set_color pcol5 = [0.514,0.361,0.902]
select surf_pocket5, protein and id [3303,3304,3306,3272,3279,778,779,780,3276,3267,3269,3307,3222,1126,1127,1109,1110,1113,1115,3305,796,798,736,760,3244,3246,3242,3243,3245,3308] 
set surface_color,  pcol5, surf_pocket5 
set_color pcol6 = [0.471,0.278,0.702]
select surf_pocket6, protein and id [3684,3130,3134,3132,3133,3681,3682,3647,3648,3649,3650,958,960,929,919,925,3626,3615,911,3683,951,877,888,3145,875,876,910,905,909] 
set surface_color,  pcol6, surf_pocket6 
set_color pcol7 = [0.698,0.361,0.902]
select surf_pocket7, protein and id [197,3150,957,959,956,159,162,3683,151,177,1001,135,140,141,142,143,144,145,3684,3685,961,963,3682,958,960,3157,3172,992,955,962,964,954,3182,191,3209] 
set surface_color,  pcol7, surf_pocket7 
set_color pcol8 = [0.616,0.278,0.702]
select surf_pocket8, protein and id [560,559,561,562,812,829,685,702,683,606,701,583,1211,1209,1161,1189,3001,3002,3008,3009,2999,1191,3010,3015,3016,3017,2993,678,3018,649,684,686,2986,2992,842,1190,674,675,1187] 
set surface_color,  pcol8, surf_pocket8 
set_color pcol9 = [0.886,0.361,0.902]
select surf_pocket9, protein and id [4217,4220,4221,4222,4212,4213,4214,4215,4216,4224,4230,4232,6411,4249,4250,4257,4258,4259,3389,429,431,3409,3413,3414,3415,3416,428,3394,3404,3405,3395,3396,3408] 
set surface_color,  pcol9, surf_pocket9 
set_color pcol10 = [0.702,0.278,0.639]
select surf_pocket10, protein and id [3562,3560,6294,6300,6303,3522,3524,4424,4427,4423,3519,3520,6380,3535,3547,6310,6299,6360,6372] 
set surface_color,  pcol10, surf_pocket10 
set_color pcol11 = [0.902,0.361,0.729]
select surf_pocket11, protein and id [268,265,306,308,270,293,294,1165,1166,1169,3034,281,3120,3039,3048,3049,3053,3054,3058,3038,3067] 
set surface_color,  pcol11, surf_pocket11 
set_color pcol12 = [0.702,0.278,0.494]
select surf_pocket12, protein and id [2041,2040,1527,2042,2604,2605,2583,2603,2607,2076,2612,1508] 
set surface_color,  pcol12, surf_pocket12 
set_color pcol13 = [0.902,0.361,0.545]
select surf_pocket13, protein and id [5286,5106,5107,5232,5121,5210,5208,5135,4846,5108,5109,5118,5122,5123,5124,5182,5128,5013,4842,4843,4845,4841,5014,5016] 
set surface_color,  pcol13, surf_pocket13 
set_color pcol14 = [0.702,0.278,0.353]
select surf_pocket14, protein and id [4419,4445,4447,4467,4083,3940,3956,3817,3818,3814,3815,3954,3955,3970,3837,3859,3860,3813,3816,4066,3824,3838,4436,6279] 
set surface_color,  pcol14, surf_pocket14 
set_color pcol15 = [0.902,0.361,0.361]
select surf_pocket15, protein and id [1889,1904,2201,1302,1326,1628,2158,1301,1299,1914,1915] 
set surface_color,  pcol15, surf_pocket15 
set_color pcol16 = [0.702,0.353,0.278]
select surf_pocket16, protein and id [6116,6108,6113,4609,6103,4569,4570,4567,4597,6189,4607,6190,4541,4573,4533,4574] 
set surface_color,  pcol16, surf_pocket16 
set_color pcol17 = [0.902,0.545,0.361]
select surf_pocket17, protein and id [5172,5173,4886,5458,5459,4560,4559,5416,5145,5160,5161,5162] 
set surface_color,  pcol17, surf_pocket17 
set_color pcol18 = [0.702,0.494,0.278]
select surf_pocket18, protein and id [4645,4646,6082,6095,6097,6091,6092,5991,6002,5992,6076,6077,4685,6004,4676,4687] 
set surface_color,  pcol18, surf_pocket18 
set_color pcol19 = [0.902,0.729,0.361]
select surf_pocket19, protein and id [2742,2740,2728,2729,2730,2732,1429,2814,2815,2820,1387,1427,2829,2835,1388,2833,1399,2830] 
set surface_color,  pcol19, surf_pocket19 
set_color pcol20 = [0.702,0.639,0.278]
select surf_pocket20, protein and id [1660,1349,1275,2928,2929,2856,2845,2846,2850,2851,2854,2841] 
set surface_color,  pcol20, surf_pocket20 
set_color pcol21 = [0.886,0.902,0.361]
select surf_pocket21, protein and id [2799,1683,1096,1055,1058,1243,1244,1714,1715,1264,1666,1667,1669,1668] 
set surface_color,  pcol21, surf_pocket21 


deselect

orient
