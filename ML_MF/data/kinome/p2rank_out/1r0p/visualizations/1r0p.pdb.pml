from pymol import cmd,stored

set depth_cue, 1
set fog_start, 0.4

set_color b_col, [36,36,85]
set_color t_col, [10,10,10]
set bg_rgb_bottom, b_col
set bg_rgb_top, t_col      
set bg_gradient

set  spec_power  =  200
set  spec_refl   =  0

load data/1r0p.pdb, protein
create ligands, protein and organic
select xlig, protein and organic
delete xlig

hide everything, all

color white, elem c
color bluewhite, protein
#show_as cartoon, protein
show surface, protein
#set transparency, 0.15

show sticks, ligands
set stick_color, magenta

load data/1r0p.pdb_points.pdb.gz, points
hide nonbonded, points
show nb_spheres, points
set sphere_scale, 0.2, points
cmd.spectrum("b", "green_red", selection="points", minimum=0, maximum=0.7)


stored.list=[]
cmd.iterate("(resn STP)","stored.list.append(resi)")    # read info about residues STP
lastSTP=stored.list[-1] # get the index of the last residue
hide lines, resn STP

cmd.select("rest", "resn STP and resi 0")

for my_index in range(1,int(lastSTP)+1): cmd.select("pocket"+str(my_index), "resn STP and resi "+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.show("spheres","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_scale","0.4","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_transparency","0.1","pocket"+str(my_index))



set_color pcol1 = [0.361,0.576,0.902]
select surf_pocket1, protein and id [1369,1370,1379,1381,1378,1380,1371,1372,1383,1226,1227,796,791,419,801,1347,306,307,324,433,263,269,272,275,266,322,323,808,806,1342,843,839,840,812,810,815,823,849] 
set surface_color,  pcol1, surf_pocket1 
set_color pcol2 = [0.278,0.349,0.702]
select surf_pocket2, protein and id [1445,1446,1447,1448,1449,569,1327,1328,567,564,566,589,590,591,594,588,1176,1151,1450,1317,1136,1138,1140,623,1322,625,1433,1432,1131,1124,1115,1348,1350,1351,1352,1311] 
set surface_color,  pcol2, surf_pocket2 
set_color pcol3 = [0.404,0.361,0.902]
select surf_pocket3, protein and id [888,1266,1267,1268,1269,1270,1271,1272,936,942,956,950,951,884,887,915,930,937,939,940,941,928,933,838,954,1016,1017,1018,984,985,987,986,1265,944,945,830,831,832,823,836,827,828,837,872,873,874] 
set surface_color,  pcol3, surf_pocket3 
set_color pcol4 = [0.416,0.278,0.702]
select surf_pocket4, protein and id [1535,1796,1799,1808,2391,867,2393,866,1750,865,1418,1417,1419,1407,1408,1201,1536,1520,1805,1521,1522,1523,1496,1502,1508,1514] 
set surface_color,  pcol4, surf_pocket4 
set_color pcol5 = [0.663,0.361,0.902]
select surf_pocket5, protein and id [1648,1649,2142,2145,2146,1640,2147,1076,1105,2158,2162,2163,1104,1106,1107,1638,1639] 
set surface_color,  pcol5, surf_pocket5 
set_color pcol6 = [0.616,0.278,0.702]
select surf_pocket6, protein and id [1006,1005,1030,2239,2240,2241,2242,1008,1007,1036,1058,1060,978,1017,981,983,984,985,1039,986,1265,1266,1037] 
set surface_color,  pcol6, surf_pocket6 
set_color pcol7 = [0.902,0.361,0.878]
select surf_pocket7, protein and id [511,507,508,536,539,541,1349,1350,1361,1352,1338,300,453,447,448,474,476,298,301,461,1362,435,568,534,531] 
set surface_color,  pcol7, surf_pocket7 
set_color pcol8 = [0.702,0.278,0.584]
select surf_pocket8, protein and id [1295,1294,1243,1251,803,819,797,798,398,693,660,676,677,635,643,646] 
set surface_color,  pcol8, surf_pocket8 
set_color pcol9 = [0.902,0.361,0.620]
select surf_pocket9, protein and id [47,719,715,45,44,164,712,716,159,160,552,553,126,705,136,106,105,66] 
set surface_color,  pcol9, surf_pocket9 
set_color pcol10 = [0.702,0.278,0.380]
select surf_pocket10, protein and id [1412,1428,1458,1426,1427,1386,291,292,294,1348,1350,1355,1361,1356,1387,1433,1432] 
set surface_color,  pcol10, surf_pocket10 
set_color pcol11 = [0.902,0.361,0.361]
select surf_pocket11, protein and id [1145,1619,1165,1141,1166,1447,1449,1144,1138] 
set surface_color,  pcol11, surf_pocket11 
set_color pcol12 = [0.702,0.380,0.278]
select surf_pocket12, protein and id [221,250,251,223,356,344,249,404,342,343,405,406,407,408,403,345,346,809,810] 
set surface_color,  pcol12, surf_pocket12 
set_color pcol13 = [0.902,0.620,0.361]
select surf_pocket13, protein and id [333,335,441,768,770,234,235,726,728,232,754,753] 
set surface_color,  pcol13, surf_pocket13 
set_color pcol14 = [0.702,0.584,0.278]
select surf_pocket14, protein and id [1679,2066,2068,2069,2070,1518,1819,1712,1919] 
set surface_color,  pcol14, surf_pocket14 
set_color pcol15 = [0.902,0.878,0.361]
select surf_pocket15, protein and id [1513,1545,1499,1462,1437,1454,1460,1571,1575,1603,1604,1606,1581,1576,1577,1626] 
set surface_color,  pcol15, surf_pocket15 


deselect

orient
