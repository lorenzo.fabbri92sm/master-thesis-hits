from pymol import cmd,stored

set depth_cue, 1
set fog_start, 0.4

set_color b_col, [36,36,85]
set_color t_col, [10,10,10]
set bg_rgb_bottom, b_col
set bg_rgb_top, t_col      
set bg_gradient

set  spec_power  =  200
set  spec_refl   =  0

load data/4qrc.pdb, protein
create ligands, protein and organic
select xlig, protein and organic
delete xlig

hide everything, all

color white, elem c
color bluewhite, protein
#show_as cartoon, protein
show surface, protein
#set transparency, 0.15

show sticks, ligands
set stick_color, magenta

load data/4qrc.pdb_points.pdb.gz, points
hide nonbonded, points
show nb_spheres, points
set sphere_scale, 0.2, points
cmd.spectrum("b", "green_red", selection="points", minimum=0, maximum=0.7)


stored.list=[]
cmd.iterate("(resn STP)","stored.list.append(resi)")    # read info about residues STP
lastSTP=stored.list[-1] # get the index of the last residue
hide lines, resn STP

cmd.select("rest", "resn STP and resi 0")

for my_index in range(1,int(lastSTP)+1): cmd.select("pocket"+str(my_index), "resn STP and resi "+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.show("spheres","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_scale","0.4","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_transparency","0.1","pocket"+str(my_index))



set_color pcol1 = [0.361,0.576,0.902]
select surf_pocket1, protein and id [356,608,732,1260,605,727,728,224,225,742,743,1336,1334,1337,1335,1338,1341,1344,1345,555,523,530,532,1236,1352,1354,1261,1356,164,227,163,226,371,159,160,166,210,211,368,161,500,1362,1366,1367,764,767,752,757,747,750,744,1328,1130,1188,1331,598,1131,556,601,774,1176,1184,1172,1364,1368,524] 
set surface_color,  pcol1, surf_pocket1 
set_color pcol2 = [0.302,0.278,0.702]
select surf_pocket2, protein and id [1235,1523,1463,1464,1480,1462,1505,1506,1507,186,1236,1245,1246,1349,1209,1194,1206,1207,1208,1377,1421,1423,1522,1417,1422,1424,1427,1560,1561,1520,1521,1525,1526,1529,1433,1380,1382,1375,1378,1379] 
set surface_color,  pcol2, surf_pocket2 
set_color pcol3 = [0.631,0.361,0.902]
select surf_pocket3, protein and id [1820,1552,1771,1553,1819,1827,1505,1504,789,790,791,1232,1234,1500,1503,1498,1510,1490,1491,1538,1824,1540] 
set surface_color,  pcol3, surf_pocket3 
set_color pcol4 = [0.678,0.278,0.702]
select surf_pocket4, protein and id [1820,1819,1827,1504,1500,1473,1498,1510,1847,1848,1849,1850,1853,1854,1860,1863,1866,1538,1824,1540,1514,1515,1516] 
set surface_color,  pcol4, surf_pocket4 
set_color pcol5 = [0.902,0.361,0.682]
select surf_pocket5, protein and id [1034,2299,1035,1054,1056,2301,1064,1065,1066,584,585,1067] 
set surface_color,  pcol5, surf_pocket5 
set_color pcol6 = [0.702,0.278,0.341]
select surf_pocket6, protein and id [1021,991,994,990,979,980,1784,1787,996,2021,1804,1805,1806,865,866,2022] 
set surface_color,  pcol6, surf_pocket6 
set_color pcol7 = [0.902,0.522,0.361]
select surf_pocket7, protein and id [1368,524,1199,1364,1173,1171,1172,1160,1161,1168,1150,1176,523] 
set surface_color,  pcol7, surf_pocket7 
set_color pcol8 = [0.702,0.596,0.278]
select surf_pocket8, protein and id [1013,1035,1067,1296,1300,1288,1297] 
set surface_color,  pcol8, surf_pocket8 


deselect

orient
