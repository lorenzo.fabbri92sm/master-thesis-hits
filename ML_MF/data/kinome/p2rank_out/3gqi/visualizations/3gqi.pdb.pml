from pymol import cmd,stored

set depth_cue, 1
set fog_start, 0.4

set_color b_col, [36,36,85]
set_color t_col, [10,10,10]
set bg_rgb_bottom, b_col
set bg_rgb_top, t_col      
set bg_gradient

set  spec_power  =  200
set  spec_refl   =  0

load data/3gqi.pdb, protein
create ligands, protein and organic
select xlig, protein and organic
delete xlig

hide everything, all

color white, elem c
color bluewhite, protein
#show_as cartoon, protein
show surface, protein
#set transparency, 0.15

show sticks, ligands
set stick_color, magenta

load data/3gqi.pdb_points.pdb.gz, points
hide nonbonded, points
show nb_spheres, points
set sphere_scale, 0.2, points
cmd.spectrum("b", "green_red", selection="points", minimum=0, maximum=0.7)


stored.list=[]
cmd.iterate("(resn STP)","stored.list.append(resi)")    # read info about residues STP
lastSTP=stored.list[-1] # get the index of the last residue
hide lines, resn STP

cmd.select("rest", "resn STP and resi 0")

for my_index in range(1,int(lastSTP)+1): cmd.select("pocket"+str(my_index), "resn STP and resi "+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.show("spheres","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_scale","0.4","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_transparency","0.1","pocket"+str(my_index))



set_color pcol1 = [0.361,0.576,0.902]
select surf_pocket1, protein and id [200,466,468,491,199,192,197,201,203,205,1552,1396,1397,487,1398,486,488,492,489,490,1405,1406,1551,1548,390,391,498,392,406,408,212,396,209,210,211,402,520,521,460,465,1269,180,181,182,188,189,1272,1273,178,227,1246,1283,1380,1381,1382,1383,1274,1247,1375,1376,1377,1565] 
set surface_color,  pcol1, surf_pocket1 
set_color pcol2 = [0.278,0.341,0.702]
select surf_pocket2, protein and id [2828,2975,2978,2984,2325,2326,2829,2958,2960,2977,2979,2980,2324,2292,2323,2299,2300,2302,2988,2994,3005,3014,2995,1154,3072,3078,3079,3067,3068,3084,3088,2808,2810,2356,2357,2421,2952,2953,2956,2847,2848,2948,2939,2940,2941,2424,2398,2399,2390,2783,2784,2797,2788,2798,2395,2577,2578,2766,1131,1132] 
set surface_color,  pcol2, surf_pocket2 
set_color pcol3 = [0.424,0.361,0.902]
select surf_pocket3, protein and id [170,171,176,177,178,226,227,801,1269,1375,1376,1377,1299,1298,1278,1283,1380,1381,1383,376,752,748,773,774,770,764,766,173,174,390,392,554,555,522,790,791,828] 
set surface_color,  pcol3, surf_pocket3 
set_color pcol4 = [0.435,0.278,0.702]
select surf_pocket4, protein and id [1335,1336,1337,1338,2555,998,1005,1085,1029,1055,1057,1056,1058,1007,2554,1111,1334,1326,2787,1079,2817,2790,2791,2788,1047,1050,2792] 
set surface_color,  pcol4, surf_pocket4 
set_color pcol5 = [0.698,0.361,0.902]
select surf_pocket5, protein and id [1235,1236,1237,1405,1418,1419,1420,1551,1398,490,1684,1516,1517,1525,1522,1560,1524,461,1530,1537,460,1509,1510,1507,1416,1417,1218,1437,1681,1682] 
set surface_color,  pcol5, surf_pocket5 
set_color pcol6 = [0.651,0.278,0.702]
select surf_pocket6, protein and id [3401,3402,3460,3728,3729,3669,3459,3667,3668,3411,3408,3409,3410,3706,3711,3705,3374,3376,3386,3377,3378,3379,2654,2655,2653,3852,3853,3854,3710,3712,3851,3951,3945,3954] 
set surface_color,  pcol6, surf_pocket6 
set_color pcol7 = [0.902,0.361,0.824]
select surf_pocket7, protein and id [1704,1712,1178,1179,1181,1182,1152,2261,2265,2244,2247,2248,2249,2250,1699,1700,1707,1708,1701,1702,1180] 
set surface_color,  pcol7, surf_pocket7 
set_color pcol8 = [0.702,0.278,0.533]
select surf_pocket8, protein and id [3502,3505,3506,3658,3809,3498,3503,3820,3735,3677,3826,3827,3674,3678,3697,3815,3819,3807,3816] 
set surface_color,  pcol8, surf_pocket8 
set_color pcol9 = [0.902,0.361,0.545]
select surf_pocket9, protein and id [4054,4064,4065,4067,3760,3806,3818,4066,4068,3758,4069,3802] 
set surface_color,  pcol9, surf_pocket9 
set_color pcol10 = [0.702,0.278,0.318]
select surf_pocket10, protein and id [2821,2840,2819,3006,2986,3035,3057,2462,3056,3146,2838] 
set surface_color,  pcol10, surf_pocket10 
set_color pcol11 = [0.902,0.451,0.361]
select surf_pocket11, protein and id [2240,2274,2278,2160,2308,2309,2310,2133,2127,2130,2131,2126,2158,2092,2241] 
set surface_color,  pcol11, surf_pocket11 
set_color pcol12 = [0.702,0.459,0.278]
select surf_pocket12, protein and id [2062,2063,2069,1037,1039,1040,1832,1834,2371,2372,1073,2104,2080] 
set surface_color,  pcol12, surf_pocket12 
set_color pcol13 = [0.902,0.729,0.361]
select surf_pocket13, protein and id [607,608,2818,2819,2781,2534,2776,2825,2778,2816,2817,2773,1326,1345,2532,1328,1330] 
set surface_color,  pcol13, surf_pocket13 
set_color pcol14 = [0.702,0.675,0.278]
select surf_pocket14, protein and id [2444,3249,2873,3305,3248,2894,2935,2896,3268,3269] 
set surface_color,  pcol14, surf_pocket14 


deselect

orient
