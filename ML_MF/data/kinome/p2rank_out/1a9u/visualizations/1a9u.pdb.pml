from pymol import cmd,stored

set depth_cue, 1
set fog_start, 0.4

set_color b_col, [36,36,85]
set_color t_col, [10,10,10]
set bg_rgb_bottom, b_col
set bg_rgb_top, t_col      
set bg_gradient

set  spec_power  =  200
set  spec_refl   =  0

load data/1a9u.pdb, protein
create ligands, protein and organic
select xlig, protein and organic
delete xlig

hide everything, all

color white, elem c
color bluewhite, protein
#show_as cartoon, protein
show surface, protein
#set transparency, 0.15

show sticks, ligands
set stick_color, magenta

load data/1a9u.pdb_points.pdb.gz, points
hide nonbonded, points
show nb_spheres, points
set sphere_scale, 0.2, points
cmd.spectrum("b", "green_red", selection="points", minimum=0, maximum=0.7)


stored.list=[]
cmd.iterate("(resn STP)","stored.list.append(resi)")    # read info about residues STP
lastSTP=stored.list[-1] # get the index of the last residue
hide lines, resn STP

cmd.select("rest", "resn STP and resi 0")

for my_index in range(1,int(lastSTP)+1): cmd.select("pocket"+str(my_index), "resn STP and resi "+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.show("spheres","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_scale","0.4","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_transparency","0.1","pocket"+str(my_index))



set_color pcol1 = [0.361,0.576,0.902]
select surf_pocket1, protein and id [269,286,287,288,237,239,395,397,393,396,825,826,865,873,380,381,268,270,1338,1342,1344,1334,1336,1337,667,1343,862,866,867,1341,1345,1348,666,588,589,843,841,551,552,554] 
set surface_color,  pcol1, surf_pocket1 
set_color pcol2 = [0.278,0.278,0.702]
select surf_pocket2, protein and id [849,851,852,853,675,2809,687,2808,357,358,2814,669,672,1316,1319,1320,857,863,868,848,637,639,640,656,1307,1312,641,652,1302,1303,612,611,629,626,618,621,623,1264,1268,1271,1299,1301,1254,1255,1257,1298,870,1256,856] 
set surface_color,  pcol2, surf_pocket2 
set_color pcol3 = [0.576,0.361,0.902]
select surf_pocket3, protein and id [1468,1229,1509,1511,900,902,901,1497,1499,1734,1778,927,931,1454,1773,1234,1235,1230] 
set surface_color,  pcol3, surf_pocket3 
set_color pcol4 = [0.616,0.278,0.702]
select surf_pocket4, protein and id [1138,2379,2384,1104,1108,1644,1627,1136,1637,1134,1135,1106,1142,1143,1146,2542,2376,2378,2377,2388,1140] 
set surface_color,  pcol4, surf_pocket4 
set_color pcol5 = [0.902,0.361,0.792]
select surf_pocket5, protein and id [643,651,1086,1087,1088,652,1302,2517,1052,2471,2494,2520,2475,2477,2462,1024,1026,1060,1062] 
set surface_color,  pcol5, surf_pocket5 
set_color pcol6 = [0.702,0.278,0.447]
select surf_pocket6, protein and id [693,694,695,162,161,2,6,9,25,26,27,179,181,337,339,2801,2802,683] 
set surface_color,  pcol6, surf_pocket6 
set_color pcol7 = [0.902,0.361,0.361]
select surf_pocket7, protein and id [1786,1918,2194,2180,2182,2183,2184,1757,1794,1796,1909,1880,1908,1916,2185,2145,2146,2174,1765,1776,1781,1766,1783,1756,1881,1884] 
set surface_color,  pcol7, surf_pocket7 
set_color pcol8 = [0.702,0.447,0.278]
select surf_pocket8, protein and id [488,490,260,261,264,512,513,511,1372,509,510,1377,1379,1378] 
set surface_color,  pcol8, surf_pocket8 
set_color pcol9 = [0.902,0.792,0.361]
select surf_pocket9, protein and id [914,965,966,995,998,997,999,916,917,1294,996] 
set surface_color,  pcol9, surf_pocket9 


deselect

orient
