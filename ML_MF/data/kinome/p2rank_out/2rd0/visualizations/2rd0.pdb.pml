from pymol import cmd,stored

set depth_cue, 1
set fog_start, 0.4

set_color b_col, [36,36,85]
set_color t_col, [10,10,10]
set bg_rgb_bottom, b_col
set bg_rgb_top, t_col      
set bg_gradient

set  spec_power  =  200
set  spec_refl   =  0

load data/2rd0.pdb, protein
create ligands, protein and organic
select xlig, protein and organic
delete xlig

hide everything, all

color white, elem c
color bluewhite, protein
#show_as cartoon, protein
show surface, protein
#set transparency, 0.15

show sticks, ligands
set stick_color, magenta

load data/2rd0.pdb_points.pdb.gz, points
hide nonbonded, points
show nb_spheres, points
set sphere_scale, 0.2, points
cmd.spectrum("b", "green_red", selection="points", minimum=0, maximum=0.7)


stored.list=[]
cmd.iterate("(resn STP)","stored.list.append(resi)")    # read info about residues STP
lastSTP=stored.list[-1] # get the index of the last residue
hide lines, resn STP

cmd.select("rest", "resn STP and resi 0")

for my_index in range(1,int(lastSTP)+1): cmd.select("pocket"+str(my_index), "resn STP and resi "+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.show("spheres","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_scale","0.4","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_transparency","0.1","pocket"+str(my_index))



set_color pcol1 = [0.361,0.576,0.902]
select surf_pocket1, protein and id [5857,5858,5881,14,15,17,7,16,2181,2178,13,2288,2317,2325,2326,2329,2330,2316,2359,2358,2360,2349,2350,2351,5866,5865,2172,5829,5830,5862,5863,5864,5859,5867,5868,5871,2168,2169,5890,5057,5062,1432,1434,5063,5066,5067,5068,1435,1449,1452,5069,5070,1464,1465,1466,1467,2289,2291,2292,2293,1460,1481,1482,1483,2320,1553,2318,2319,1525,5373,5375,2517,5400,5061,5401,5402,5403,5813,5810,5811,5815,4762,1441,1443,1444,2502,2489,2490,2492,2493,5103,5104,5105,5847,5853,5856,5,6,6616,1,48,50,6397,6398,6399,6400,47,49,6490,6491,46,69,7230,41,42,70,6437,6439,6532,6535,6537,6538,6531,6372,6373,6515,6522,6517,6499,7231,6547,6314,5099,5150,5151,5100,5101,5102,4784,4785,4782,4783,4786,4787,4789,4791,4793,6370,6371,6368,6407,6408,6441,6435,6440,6436,6444,4820,4822,6345,6340,6341,6344,6312,2508,2509,5427,5428,5361,5362] 
set surface_color,  pcol1, surf_pocket1 
set_color pcol2 = [0.278,0.412,0.702]
select surf_pocket2, protein and id [5225,3487,3489,3490,3485,3488,3491,3494,8421,8416,8418,8420,3497,8412,8413,9093,9127,9128,9129,9130,9126,5200,5203,5207,5208,5209,5224,5219,5199,5201,3512,3513,3514,8383,7857,7817,7818,6289,7813,7814,7819,7821,7854,7860,7820,7834,7835,4879,5182,6287,7809,7810,6288,8453,6573,6271,6580,6270,6272,6269,3658,9054,9056,9057,9091,9092,9094,9064,8445,2738,5210,5214,5216,5217,5218,3646,3650,9009,3638,8478,8480,8506,8451,8444,8443,8476,8470,8471,9032,3472,3464,3465,3657,3296,3473,3466,3663,3664,3665,2715,2727,2730,2745,3477,3480,3633,3634,3482,3653,3635,3636,3637,3236,3238,3239,3232,2812,3503,3307,3308,3566,3567,3525,3582,3524,3496,3240,3241,3245,3532,3246,3533,3535,3249,3251,3592,3578,3583,3584,3587,3581,3579,7802,7808] 
set surface_color,  pcol2, surf_pocket2 
set_color pcol3 = [0.361,0.482,0.902]
select surf_pocket3, protein and id [700,701,718,721,836,837,785,816,817,668,683,1027,1031,994,1002,1017,1018,1026,760,784,757,782,783,778,777,781,774,773,788,772,793,794,796,776,722,749,751,727,728,729,705,714,1054,5451,5452,5453,5455,5454,5780,5781,5786,5790,820,5754,5782,5783,5784,1050,1051,1052,1053,1055,1057,5485,818,1034,5425,5426,5391,5392,5393,1046,1044,1045,1047,1048,1072,1029,685,686,1032,1019,1021,997,1084,1090,1093,1101,1102,1087] 
set surface_color,  pcol3, surf_pocket3 
set_color pcol4 = [0.278,0.337,0.702]
select surf_pocket4, protein and id [6051,6224,5985,5986,5987,7266,7267,6697,6653,6696,7185,7186,6279,6305,6527,6529,6605,6606,6607,7265,6030,6243,6244,7273,5998,6005,6013,6264,7274,7276,5984,6307,6306,6308,7270,7268,6671,6672,7269,6031,6618,6052,6053,6622,6624,6627,6655,6611,6629] 
set surface_color,  pcol4, surf_pocket4 
set_color pcol5 = [0.361,0.388,0.902]
select surf_pocket5, protein and id [8103,8104,7057,7367,7388,8135,8105,7597,7055,7620,7619,7587,7590,7593,7594,8098,8099,8091,7356,7082,7083,7085,7081,7630,7631,8070,8071,8069] 
set surface_color,  pcol5, surf_pocket5 
set_color pcol6 = [0.298,0.278,0.702]
select surf_pocket6, protein and id [1050,5475,5477,5476,1072,5167,5168,685,686,687,1074,5193,5188,6562,6558,6564,5501,5502,5499,6567,5140,5166,5192,5163,5198,5196] 
set surface_color,  pcol6, surf_pocket6 
set_color pcol7 = [0.427,0.361,0.902]
select surf_pocket7, protein and id [4904,3608,3612,3606,3609,3610,3611,3317,3617,3452,4929,1177,1156,1160,1161,1162,1174,3624,3625,3337,3330,3333,3334,3323,3326,3327,3328,3329,3349,4917,4919,3365,3372,3374,3439,3440,3438,3412,3358,3362,3363,3364,5246,5256] 
set surface_color,  pcol7, surf_pocket7 
set_color pcol8 = [0.369,0.278,0.702]
select surf_pocket8, protein and id [1539,1579,1580,1581,1582,116,117,91,97,102,118,2348,65,87,74,2362,1540,1543,6473,6474,6980,6981,7016,6476,6493,6979,6959,6988,89,6960,6978,6957,67] 
set surface_color,  pcol8, surf_pocket8 
set_color pcol9 = [0.522,0.361,0.902]
select surf_pocket9, protein and id [7874,4589,7876,3580,7868,7872,4060,3575,3571,3576,3582,3550,3551,3573,7863,7866,4578,4580,4581,4587,4588,4579,2886,2887,2888,2889,2890,4302,3201] 
set surface_color,  pcol9, surf_pocket9 
set_color pcol10 = [0.443,0.278,0.702]
select surf_pocket10, protein and id [4509,4510,4739,4803,4501,4503,4504,4520,7706,7708,7728,4517,6382,6410,6414,6416,6379,6405,6408,6447,4768,4805,4769,4794,7729,4770] 
set surface_color,  pcol10, surf_pocket10 
set_color pcol11 = [0.620,0.361,0.902]
select surf_pocket11, protein and id [2171,2494,2140,6122,6123,6124,1443,1444,2492,2493,2413,2414,1686,1683,1684,2429,2430,2458,2459,2428,2418,2424,2431,5889,5890,2172] 
set surface_color,  pcol11, surf_pocket11 
set_color pcol12 = [0.518,0.278,0.702]
select surf_pocket12, protein and id [1361,1375,1376,1379,1380,1381,1382,1383,1384,2208,2228,2229,2230,1856,1373,2123,2124,2207,2443,2444,1854,1855,2108,1853,2478,2476,2093,1857,2214,2221,2213] 
set surface_color,  pcol12, surf_pocket12 
set_color pcol13 = [0.714,0.361,0.902]
select surf_pocket13, protein and id [1459,1458,1412,1413,2218,2219,2220,2221,1409,1410,1415,1390,1388,1411,1381,1383,4747,4751,4752,4754,5052,4755,1450,1456,1472,1473,5041,1322,1326,4753,5023,5024,5044] 
set surface_color,  pcol13, surf_pocket13 
set_color pcol14 = [0.592,0.278,0.702]
select surf_pocket14, protein and id [2075,2079,2113,2114,2115,2078,2080,2081,1678,1937,1935,1943,1906,1907,2100,2102,2421,1679,1683] 
set surface_color,  pcol14, surf_pocket14 
set_color pcol15 = [0.808,0.361,0.902]
select surf_pocket15, protein and id [21,58,60,20,23,26,2373,79,80,6167,35,31,55,6649,38,6637,6638,6639] 
set surface_color,  pcol15, surf_pocket15 
set_color pcol16 = [0.667,0.278,0.702]
select surf_pocket16, protein and id [4621,4630,4631,3180,4617,4622,3178,3179,4939,4942,4618,4620,4643,4645,4946,4950,4952,4951,4905,4908,4897,4898,4900,4895,3391,3368,4916,4918,3390,3389,4933] 
set surface_color,  pcol16, surf_pocket16 
set_color pcol17 = [0.902,0.361,0.898]
select surf_pocket17, protein and id [5467,5843,5494,5820,5822,6599,6216,5897,6082] 
set surface_color,  pcol17, surf_pocket17 
set_color pcol18 = [0.702,0.278,0.663]
select surf_pocket18, protein and id [1050,1053,1055,5474,5475,5477,5476,136,138,137,139,5472,5507,5473,1072,685,686,687,679,683,833,834,675,678,835,836,837] 
set surface_color,  pcol18, surf_pocket18 
set_color pcol19 = [0.902,0.361,0.804]
select surf_pocket19, protein and id [8313,9209,9210,9208,8344,9150,9152,9153,8343,9184,8311,8352,9176,9182,9174,9175,3516,3517,3519,3508,9221] 
set surface_color,  pcol19, surf_pocket19 
set_color pcol20 = [0.702,0.278,0.588]
select surf_pocket20, protein and id [26,27,28,29,2371,2373,78,79,80,2369,2336,6158,6159,6157,2398,2379,2396,2397,2376,2380] 
set surface_color,  pcol20, surf_pocket20 
set_color pcol21 = [0.902,0.361,0.706]
select surf_pocket21, protein and id [8553,8924,8923,8925,866,865,867,8870,163,173,154,156,157,159,8965] 
set surface_color,  pcol21, surf_pocket21 
set_color pcol22 = [0.702,0.278,0.514]
select surf_pocket22, protein and id [6665,6897,6664,7209,6648,6697,6660,6661,6662,6695,6725,6726,6693,6727,6703,6699,6728,6858] 
set surface_color,  pcol22, surf_pocket22 
set_color pcol23 = [0.902,0.361,0.612]
select surf_pocket23, protein and id [158,159,160,164,165,169,143,162,163,8483,8489,8490,8494,8496,678,8485,8487,8488,8497,139] 
set surface_color,  pcol23, surf_pocket23 
set_color pcol24 = [0.702,0.278,0.439]
select surf_pocket24, protein and id [6880,7468,7469,7470,6803,7439,6849,6807,6810,6813,6815,6804,6802,6806,6779,7440] 
set surface_color,  pcol24, surf_pocket24 
set_color pcol25 = [0.902,0.361,0.518]
select surf_pocket25, protein and id [3892,3893,4189,4187,4427,3866,3864,3889,3890,4424,4425,4429,4435,4458,4459,4215] 
set surface_color,  pcol25, surf_pocket25 
set_color pcol26 = [0.702,0.278,0.365]
select surf_pocket26, protein and id [5180,5182,5174,6287,6288,6289,5172,5200,5208,5201,6271,6580,6270,6272,6573,6566] 
set surface_color,  pcol26, surf_pocket26 
set_color pcol27 = [0.902,0.361,0.424]
select surf_pocket27, protein and id [1489,1490,1494,6446,4762,1482,1483,2293,6403,6404,6405,6406,6407,6408,6441,6435,6440,6444,4769] 
set surface_color,  pcol27, surf_pocket27 
set_color pcol28 = [0.702,0.278,0.294]
select surf_pocket28, protein and id [3355,3140,3356,3767,2595,3137,3160,3794,2598] 
set surface_color,  pcol28, surf_pocket28 
set_color pcol29 = [0.902,0.392,0.361]
select surf_pocket29, protein and id [5143,5145,4855,5173,5174,6287,6286,6316,7798,4821,5148] 
set surface_color,  pcol29, surf_pocket29 
set_color pcol30 = [0.702,0.341,0.278]
select surf_pocket30, protein and id [7776,7310,7100,7103,7308,6329] 
set surface_color,  pcol30, surf_pocket30 
set_color pcol31 = [0.902,0.486,0.361]
select surf_pocket31, protein and id [2,6615,6203,6198,6200,39,40,6613,6620,22,6183,3,4,5878,5880] 
set surface_color,  pcol31, surf_pocket31 
set_color pcol32 = [0.702,0.416,0.278]
select surf_pocket32, protein and id [4596,4598,4302,4608,2878,4585,2889,2892,4333,4289,4290,4295,4296,4298,2882,2884,4331,4334,4336,4613] 
set surface_color,  pcol32, surf_pocket32 
set_color pcol33 = [0.902,0.580,0.361]
select surf_pocket33, protein and id [5848,5856,2,6614,6616,6203,6198,5850,6613,5836,4,5878,5880,5877] 
set surface_color,  pcol33, surf_pocket33 
set_color pcol34 = [0.702,0.490,0.278]
select surf_pocket34, protein and id [7944,8087,8088,8097,8021,8053,8057,8055,7974,7992,7991] 
set surface_color,  pcol34, surf_pocket34 
set_color pcol35 = [0.902,0.675,0.361]
select surf_pocket35, protein and id [7510,7509,7557,7431,7432,8105,8131,8134,8148,8150] 
set surface_color,  pcol35, surf_pocket35 
set_color pcol36 = [0.702,0.561,0.278]
select surf_pocket36, protein and id [3830,3784,4386,4388,3152,3159,3160,2606,4357,4354,3826,4389,4392,3793,3166] 
set surface_color,  pcol36, surf_pocket36 
set_color pcol37 = [0.902,0.773,0.361]
select surf_pocket37, protein and id [5508,5509,127,128,130,5472,5481,5507,5511,5541,190,5486,5767,5769] 
set surface_color,  pcol37, surf_pocket37 
set_color pcol38 = [0.702,0.635,0.278]
select surf_pocket38, protein and id [3497,8413,9093,9129,3485,3488,3491,9094] 
set surface_color,  pcol38, surf_pocket38 
set_color pcol39 = [0.902,0.867,0.361]
select surf_pocket39, protein and id [7344,7346,7347,7953,8059,7917,7918,7085,7340,7350] 
set surface_color,  pcol39, surf_pocket39 
set_color pcol40 = [0.690,0.702,0.278]
select surf_pocket40, protein and id [4615,4638,4419,4407,4337,4417] 
set surface_color,  pcol40, surf_pocket40 
set_color pcol41 = [0.839,0.902,0.361]
select surf_pocket41, protein and id [7389,7418,7386,7432,7388,7030,7417,7455] 
set surface_color,  pcol41, surf_pocket41 


deselect

orient
