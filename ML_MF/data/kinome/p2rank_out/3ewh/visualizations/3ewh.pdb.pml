from pymol import cmd,stored

set depth_cue, 1
set fog_start, 0.4

set_color b_col, [36,36,85]
set_color t_col, [10,10,10]
set bg_rgb_bottom, b_col
set bg_rgb_top, t_col      
set bg_gradient

set  spec_power  =  200
set  spec_refl   =  0

load data/3ewh.pdb, protein
create ligands, protein and organic
select xlig, protein and organic
delete xlig

hide everything, all

color white, elem c
color bluewhite, protein
#show_as cartoon, protein
show surface, protein
#set transparency, 0.15

show sticks, ligands
set stick_color, magenta

load data/3ewh.pdb_points.pdb.gz, points
hide nonbonded, points
show nb_spheres, points
set sphere_scale, 0.2, points
cmd.spectrum("b", "green_red", selection="points", minimum=0, maximum=0.7)


stored.list=[]
cmd.iterate("(resn STP)","stored.list.append(resi)")    # read info about residues STP
lastSTP=stored.list[-1] # get the index of the last residue
hide lines, resn STP

cmd.select("rest", "resn STP and resi 0")

for my_index in range(1,int(lastSTP)+1): cmd.select("pocket"+str(my_index), "resn STP and resi "+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.show("spheres","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_scale","0.4","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_transparency","0.1","pocket"+str(my_index))



set_color pcol1 = [0.361,0.576,0.902]
select surf_pocket1, protein and id [648,762,1335,567,568,647,763,402,387,745,385,386,767,1409,1413,1423,1424,1425,1428,1431,1412,1430,1414,1416,1417,1418,403,535,278,276,277,399,231,226,228,258,1432,821,814,225,1336,787,793,802,811,784,208,219,220,222,223,779,781,783] 
set surface_color,  pcol1, surf_pocket1 
set_color pcol2 = [0.278,0.341,0.702]
select surf_pocket2, protein and id [559,594,558,560,566,1250,532,1414,1417,1418,535,1262,1264,1260,1420,1257,1258,1409,1410,1403,567,568,647,1246,1224,592,584,586,1245,1247,1235,1208,1209,640,1249,1256] 
set surface_color,  pcol2, surf_pocket2 
set_color pcol3 = [0.424,0.361,0.902]
select surf_pocket3, protein and id [1308,1483,1481,1482,1557,1778,1444,1467,1468,1475,1473,1474,820,821,1306,834,835,1815,1819,1821,1822,870,871,1825,1826,841] 
set surface_color,  pcol3, surf_pocket3 
set_color pcol4 = [0.435,0.278,0.702]
select surf_pocket4, protein and id [1086,1374,1375,1376,1031,1054,1088,1087,1034,1029,1032,1033,1035,853,886,805,806,851,807,808,809,887,888,1348,1360,1361,1369] 
set surface_color,  pcol4, surf_pocket4 
set_color pcol5 = [0.698,0.361,0.902]
select surf_pocket5, protein and id [1136,2318,2319,2320,2322,1078,2347,1138,1167,1169,1147,1145,1146,1148,1373,1110,1363,1383,624,626,633,1109] 
set surface_color,  pcol5, surf_pocket5 
set_color pcol6 = [0.651,0.278,0.702]
select surf_pocket6, protein and id [1375,1088,1060,1087,1061,1034,1029,960,961,962,1020,1028,959,986,1077] 
set surface_color,  pcol6, surf_pocket6 
set_color pcol7 = [0.902,0.361,0.824]
select surf_pocket7, protein and id [634,619,623,624,632,617,626,633,1145,610,614,616,618,602,606,1167,1168,1169] 
set surface_color,  pcol7, surf_pocket7 
set_color pcol8 = [0.702,0.278,0.533]
select surf_pocket8, protein and id [1942,1978,1979,1980,1982,1941,1952,1840,1842,1986,1901,1907,1908,1900,1903,1913,1862,1943,1863] 
set surface_color,  pcol8, surf_pocket8 
set_color pcol9 = [0.902,0.361,0.545]
select surf_pocket9, protein and id [1612,1923,1535,1536,1600,1602,1610,1599,1520,1882,1883,1889,1890] 
set surface_color,  pcol9, surf_pocket9 
set_color pcol10 = [0.702,0.278,0.318]
select surf_pocket10, protein and id [245,248,249,254,474,475,1453,247,1454,252,504,473,471,243,256,1452,1417,403,533,534,1433,1434,1436,1425,503,531] 
set surface_color,  pcol10, surf_pocket10 
set_color pcol11 = [0.902,0.451,0.361]
select surf_pocket11, protein and id [2279,2246,2215,2216,2258,2280,2281,2283,2284,2109,2110,2282,2100,2134] 
set surface_color,  pcol11, surf_pocket11 
set_color pcol12 = [0.702,0.459,0.278]
select surf_pocket12, protein and id [1595,1521,1650,1651,1268,1449,1269,1280,1281,1527,1528,1565,1566,1271] 
set surface_color,  pcol12, surf_pocket12 
set_color pcol13 = [0.902,0.729,0.361]
select surf_pocket13, protein and id [1844,1952,2148,2150,1989,1706,1953,1709] 
set surface_color,  pcol13, surf_pocket13 
set_color pcol14 = [0.702,0.675,0.278]
select surf_pocket14, protein and id [2225,2236,2241,2222,2224,2229,1214,1665,1675,1666] 
set surface_color,  pcol14, surf_pocket14 


deselect

orient
