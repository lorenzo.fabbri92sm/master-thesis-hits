from pymol import cmd,stored

set depth_cue, 1
set fog_start, 0.4

set_color b_col, [36,36,85]
set_color t_col, [10,10,10]
set bg_rgb_bottom, b_col
set bg_rgb_top, t_col      
set bg_gradient

set  spec_power  =  200
set  spec_refl   =  0

load data/4hct.pdb, protein
create ligands, protein and organic
select xlig, protein and organic
delete xlig

hide everything, all

color white, elem c
color bluewhite, protein
#show_as cartoon, protein
show surface, protein
#set transparency, 0.15

show sticks, ligands
set stick_color, magenta

load data/4hct.pdb_points.pdb.gz, points
hide nonbonded, points
show nb_spheres, points
set sphere_scale, 0.2, points
cmd.spectrum("b", "green_red", selection="points", minimum=0, maximum=0.7)


stored.list=[]
cmd.iterate("(resn STP)","stored.list.append(resi)")    # read info about residues STP
lastSTP=stored.list[-1] # get the index of the last residue
hide lines, resn STP

cmd.select("rest", "resn STP and resi 0")

for my_index in range(1,int(lastSTP)+1): cmd.select("pocket"+str(my_index), "resn STP and resi "+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.show("spheres","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_scale","0.4","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_transparency","0.1","pocket"+str(my_index))



set_color pcol1 = [0.361,0.576,0.902]
select surf_pocket1, protein and id [1051,1142,1148,1150,1039,127,130,1067,702,705,706,673,697,698,701,667,681,1048,1068,720,725,726,728,150,161,146,149,303,318,143,300,301,182,183,184,153,1209,1174,1180,134,135,139,132,133,151,1149,644,645,646,647,648,649,286,643,1141,524,522,670,674,535,539,1143,1144,1154,451,452,449,1159,1160,1161,299,355,1168,1169,1170,1173,631] 
set surface_color,  pcol1, surf_pocket1 
set_color pcol2 = [0.302,0.278,0.702]
select surf_pocket2, protein and id [1042,1043,1614,1333,1334,1346,1347,1623,1562,757,719,720,1616,1044,1040,1041,1036,1295,1313,1316,1304,1676,1677] 
set surface_color,  pcol2, surf_pocket2 
set_color pcol3 = [0.631,0.361,0.902]
select surf_pocket3, protein and id [606,608,609,56,594,597,55,96,98,310,623,92,93,94,95,97,52,54,571,193,194] 
set surface_color,  pcol3, surf_pocket3 
set_color pcol4 = [0.678,0.278,0.702]
select surf_pocket4, protein and id [979,980,423,1189,1154,451,447,477,441,442,443,444,446] 
set surface_color,  pcol4, surf_pocket4 
set_color pcol5 = [0.902,0.361,0.682]
select surf_pocket5, protein and id [509,883,884,885,1112,510,907,2121,854,855,853,874,876,2112,832,830,823] 
set surface_color,  pcol5, surf_pocket5 
set_color pcol6 = [0.702,0.278,0.341]
select surf_pocket6, protein and id [1177,1179,1437,1438,1246,1256,1318,1258,1309,1311,1259,1261,1002,1317,1315,1415,1416,1178,1180,1005,1007,1182] 
set surface_color,  pcol6, surf_pocket6 
set_color pcol7 = [0.902,0.522,0.361]
select surf_pocket7, protein and id [851,817,818,820,821,848,2103,1821,819,1831,1835,1836,1860,2106] 
set surface_color,  pcol7, surf_pocket7 
set_color pcol8 = [0.702,0.596,0.278]
select surf_pocket8, protein and id [2006,2008,1944,1986,1912,1914,1915,2077,1880,1879,1881,2038,2039] 
set surface_color,  pcol8, surf_pocket8 


deselect

orient
