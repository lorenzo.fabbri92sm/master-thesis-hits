from pymol import cmd,stored

set depth_cue, 1
set fog_start, 0.4

set_color b_col, [36,36,85]
set_color t_col, [10,10,10]
set bg_rgb_bottom, b_col
set bg_rgb_top, t_col      
set bg_gradient

set  spec_power  =  200
set  spec_refl   =  0

load data/4at5.pdb, protein
create ligands, protein and organic
select xlig, protein and organic
delete xlig

hide everything, all

color white, elem c
color bluewhite, protein
#show_as cartoon, protein
show surface, protein
#set transparency, 0.15

show sticks, ligands
set stick_color, magenta

load data/4at5.pdb_points.pdb.gz, points
hide nonbonded, points
show nb_spheres, points
set sphere_scale, 0.2, points
cmd.spectrum("b", "green_red", selection="points", minimum=0, maximum=0.7)


stored.list=[]
cmd.iterate("(resn STP)","stored.list.append(resi)")    # read info about residues STP
lastSTP=stored.list[-1] # get the index of the last residue
hide lines, resn STP

cmd.select("rest", "resn STP and resi 0")

for my_index in range(1,int(lastSTP)+1): cmd.select("pocket"+str(my_index), "resn STP and resi "+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.show("spheres","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_scale","0.4","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_transparency","0.1","pocket"+str(my_index))



set_color pcol1 = [0.361,0.576,0.902]
select surf_pocket1, protein and id [1310,1314,1315,348,1305,1311,1322,1323,1320,194,195,197,1324,1328,1203,1214,1331,1332,1371,1373,1374,1375,1346,1347,1367,1368,1369,1345,1370,1372,1151,1155,1173,1306,1307,1309,1138,1139,1140,1144,1201,1317,1319,227,174,770,773,797,798,1229,766,729,731,762,724,735,732,733,738,746,763,171,172,175,1210,1211,1300,1301,585,586,1293,1299,1302,1227,1228,1200,579,1098,1099,481,480,347,205,207,198,202,203,511,510,503,533,601,710,711,332,707,708,709,717,584,530,1397,1395,1396,1387] 
set surface_color,  pcol1, surf_pocket1 
set_color pcol2 = [0.278,0.278,0.702]
select surf_pocket2, protein and id [1078,2232,2234,2235,2264,2266,2267,1080,1081,1084,561,572,1031,1032,1033,1057,1059,1060,551,552,553] 
set surface_color,  pcol2, surf_pocket2 
set_color pcol3 = [0.576,0.361,0.902]
select surf_pocket3, protein and id [757,759,761,751,758,760,755,1251,1258,1242,1250,807,809,842,843,2355,2357,2367,2370,1264,2358,2354,2351,800,805,833] 
set surface_color,  pcol3, surf_pocket3 
set_color pcol4 = [0.616,0.278,0.702]
select surf_pocket4, protein and id [1041,1272,561,562,571,572,1010,1011,1031,1032,1033,1039,1040,1060,2264,2267,1253,1255,1256,1257,2288,1008,2287,2289,2310,2313,2316,2319,2290,2293,2262] 
set surface_color,  pcol4, surf_pocket4 
set_color pcol5 = [0.902,0.361,0.792]
select surf_pocket5, protein and id [1910,1912,1935,1940,1949,1953,1955,1816,1669,1670,1672,1673,1915,1913,1914,2104,1699,2106,2109,2110,2105,2108] 
set surface_color,  pcol5, surf_pocket5 
set_color pcol6 = [0.702,0.278,0.447]
select surf_pocket6, protein and id [48,50,68,69,458,460,495,66,643,457,459,461] 
set surface_color,  pcol6, surf_pocket6 
set_color pcol7 = [0.902,0.361,0.361]
select surf_pocket7, protein and id [1970,1815,1817,1837,1838,1846,1908,1845,1847,1941,1943,1956,1951] 
set surface_color,  pcol7, surf_pocket7 
set_color pcol8 = [0.702,0.447,0.278]
select surf_pocket8, protein and id [1438,1564,1893,1892,1561,1419,1420,1440,1441,1567,1568,1569,1570,1571,1572,1923,1922,1594] 
set surface_color,  pcol8, surf_pocket8 
set_color pcol9 = [0.902,0.792,0.361]
select surf_pocket9, protein and id [1215,1214,1176,1332,1314,1321,1325,1200] 
set surface_color,  pcol9, surf_pocket9 


deselect

orient
