from pymol import cmd,stored

set depth_cue, 1
set fog_start, 0.4

set_color b_col, [36,36,85]
set_color t_col, [10,10,10]
set bg_rgb_bottom, b_col
set bg_rgb_top, t_col      
set bg_gradient

set  spec_power  =  200
set  spec_refl   =  0

load data/1opl.pdb, protein
create ligands, protein and organic
select xlig, protein and organic
delete xlig

hide everything, all

color white, elem c
color bluewhite, protein
#show_as cartoon, protein
show surface, protein
#set transparency, 0.15

show sticks, ligands
set stick_color, magenta

load data/1opl.pdb_points.pdb.gz, points
hide nonbonded, points
show nb_spheres, points
set sphere_scale, 0.2, points
cmd.spectrum("b", "green_red", selection="points", minimum=0, maximum=0.7)


stored.list=[]
cmd.iterate("(resn STP)","stored.list.append(resi)")    # read info about residues STP
lastSTP=stored.list[-1] # get the index of the last residue
hide lines, resn STP

cmd.select("rest", "resn STP and resi 0")

for my_index in range(1,int(lastSTP)+1): cmd.select("pocket"+str(my_index), "resn STP and resi "+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.show("spheres","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_scale","0.4","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_transparency","0.1","pocket"+str(my_index))



set_color pcol1 = [0.361,0.576,0.902]
select surf_pocket1, protein and id [4550,4548,4549,4551,4575,4577,4570,4573,4574,4544,4576,4578,5533,5534,5626,5627,5608,5624,5099,5105,5102,4950,4951,4547,5101,4546,4572,4725,4599,4600,4717,4721,4724,4852,5062,5065,5622,5621,4876,4877,5615,4712,4707,4708,4709,4710,5080,5081,5140,5164,5113,5119,5120,5129,5130] 
set surface_color,  pcol1, surf_pocket1 
set_color pcol2 = [0.278,0.388,0.702]
select surf_pocket2, protein and id [1491,1493,1522,1546,1656,1547,1654,1657,1659,1664,2009,1494,2046,2048,1495,1496,1481,2087,2111,1524,2080,2077,2480,2481,1515,2052,2060,2076,2108,1523,2571,1668,1671,1672,1520,1521,2565,2568,2027,2049,2053,2054,1899,1898,2032,1655,2028,2556,2564,2562,1897,1824,1823,2012] 
set surface_color,  pcol2, surf_pocket2 
set_color pcol3 = [0.361,0.420,0.902]
select surf_pocket3, protein and id [1260,1263,1266,1267,1270,1271,1273,2526,1275,1316,1318,1616,1619,112,98,100,106,110,111,1284,1904,1289,1290,1293,1312,1924,1314,1288,2542,2543,1296,299,300,89,91,93,84,94,92,97,138,137,424,1279,423,78,1294,1295,2063,2041,1637,1641,1636,1615,2055,2030,2033,2037,2038,1923,2056,2062,2490,2493,2497,2500] 
set surface_color,  pcol3, surf_pocket3 
set_color pcol4 = [0.294,0.278,0.702]
select surf_pocket4, protein and id [570,590,594,565,554,559,564,540,627,1240,543,1244,555,2251,2282,2283,2284,2253,2255,1252,1265,628,1884,2527,1873,1874,1875,1263,1883,2303,2304,2305,3449,3451,3452,1869,1867,1876,2275,2274,2276,3485,3486,739,737,735,736,2277,2327,2325,2326] 
set surface_color,  pcol4, surf_pocket4 
set_color pcol5 = [0.455,0.361,0.902]
select surf_pocket5, protein and id [3578,3579,2214,3550,3580,2210,2211,2213,2235,2233,2232,2238,2203,2206,2198,3221,3222,3223,3224,3235,3259,3260,2939,2969,2269,3475,2172,2990,2992,2993,2169,2170,3213,3220,2972,2974,2975,2971,3589,3614] 
set surface_color,  pcol5, surf_pocket5 
set_color pcol6 = [0.416,0.278,0.702]
select surf_pocket6, protein and id [6287,6288,6313,5266,5267,6275,6312,6274,6276,6277,6025,6027,6028,5288,5285,5286,6022,5290,5291,5294,5296,6551,6553,5297,6024,5992,5322] 
set surface_color,  pcol6, surf_pocket6 
set_color pcol7 = [0.612,0.361,0.902]
select surf_pocket7, protein and id [1856,1858,1828,1857,1859,274,1381,265,266,1347,1349,1353,1377,1379,1378,1354,264,1841,1320,1323,1324,1336,1313,1380,1382,1361,1363,1364,1300,1310,1908] 
set surface_color,  pcol7, surf_pocket7 
set_color pcol8 = [0.537,0.278,0.702]
select surf_pocket8, protein and id [4773,4777,4739,4812,4816,5629,4747,4745,4741,5625,4582,4572,4725,4722,4723,4724,5622,5623,4851,5621,4589,4585,4729,4822,4817,4734,5624] 
set surface_color,  pcol8, surf_pocket8 
set_color pcol9 = [0.765,0.361,0.902]
select surf_pocket9, protein and id [687,689,1201,1225,1091,1128,517,519,1100,1102,510,1193,1199,1127] 
set surface_color,  pcol9, surf_pocket9 
set_color pcol10 = [0.655,0.278,0.702]
select surf_pocket10, protein and id [1316,1621,1611,1622,1610,112,113,109,1319,1364,1365,1387,116,138,114,130,1321,1324,1325,1339,1332,1591,1397] 
set surface_color,  pcol10, surf_pocket10 
set_color pcol11 = [0.902,0.361,0.878]
select surf_pocket11, protein and id [5468,5469,5473,5482,5483,5750,5757,5773,5763,5765,5746,5891,4815,5722,5729,5731,5708,5709,4813,5629,5631,5645] 
set surface_color,  pcol11, surf_pocket11 
set_color pcol12 = [0.702,0.278,0.624]
select surf_pocket12, protein and id [575,576,577,579,881,882,561,566,569,571,711,713,787,788,907,880,710,718,719,725,737,726,729,730,738,3516,3518,3520,3481,3482,739] 
set surface_color,  pcol12, surf_pocket12 
set_color pcol13 = [0.902,0.361,0.722]
select surf_pocket13, protein and id [2428,2430,2569,2570,2572,2574,2575,2576,1728,1798,1759,2429,1760,1762,1672,1517,1518,1519,1510] 
set surface_color,  pcol13, surf_pocket13 
set_color pcol14 = [0.702,0.278,0.502]
select surf_pocket14, protein and id [446,447,41,42,40,245,246,247,222,223,22,403,429,404,278] 
set surface_color,  pcol14, surf_pocket14 
set_color pcol15 = [0.902,0.361,0.569]
select surf_pocket15, protein and id [1262,71,73,409,1281,543,1243,1244,1245,1246,540,528,1247,481,482,59,60,61] 
set surface_color,  pcol15, surf_pocket15 
set_color pcol16 = [0.702,0.278,0.380]
select surf_pocket16, protein and id [6004,6035,6257,3058,6263,3081,6091,3055,3072,6267,6239,6248,6236,6240,6058,6059,6233] 
set surface_color,  pcol16, surf_pocket16 
set_color pcol17 = [0.902,0.361,0.412]
select surf_pocket17, protein and id [845,846,603,638,641,632,844,640,819,3555,3556,604,607,2221,858,864,2209] 
set surface_color,  pcol17, surf_pocket17 
set_color pcol18 = [0.702,0.302,0.278]
select surf_pocket18, protein and id [2071,2073,2118,2120,2122,2150,2145,2146,2153,2067,2068,2193,2194] 
set surface_color,  pcol18, surf_pocket18 
set_color pcol19 = [0.902,0.463,0.361]
select surf_pocket19, protein and id [2416,2837,2420,2710,2712,2693,2697,2669,2430,2578,2720,2429,2414,2415] 
set surface_color,  pcol19, surf_pocket19 
set_color pcol20 = [0.702,0.420,0.278]
select surf_pocket20, protein and id [4294,4267,4295,4269,4268,4360,3684,3686,4392,3672,3668,4368,3677] 
set surface_color,  pcol20, surf_pocket20 
set_color pcol21 = [0.902,0.620,0.361]
select surf_pocket21, protein and id [5168,5173,5175,5209,5247,5254,5205,5206,5200,5203,5198,5199,5120,5171,5124,5126,5166] 
set surface_color,  pcol21, surf_pocket21 
set_color pcol22 = [0.702,0.541,0.278]
select surf_pocket22, protein and id [476,1068,503,504,465,466,493,494,1054,1056,1101,1050,1053,1055] 
set surface_color,  pcol22, surf_pocket22 
set_color pcol23 = [0.902,0.776,0.361]
select surf_pocket23, protein and id [4999,5000,4834,4412,4445,4857,5001,4886,4830,4862,4832] 
set surface_color,  pcol23, surf_pocket23 
set_color pcol24 = [0.702,0.663,0.278]
select surf_pocket24, protein and id [6083,6380,5947,6177,5944,6228,6223] 
set surface_color,  pcol24, surf_pocket24 
set_color pcol25 = [0.871,0.902,0.361]
select surf_pocket25, protein and id [780,474,475,476,762,1061,972,1056,1060,971,1050,1044] 
set surface_color,  pcol25, surf_pocket25 


deselect

orient
