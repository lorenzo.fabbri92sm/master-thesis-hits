from pymol import cmd,stored

set depth_cue, 1
set fog_start, 0.4

set_color b_col, [36,36,85]
set_color t_col, [10,10,10]
set bg_rgb_bottom, b_col
set bg_rgb_top, t_col      
set bg_gradient

set  spec_power  =  200
set  spec_refl   =  0

load data/1vyw.pdb, protein
create ligands, protein and organic
select xlig, protein and organic
delete xlig

hide everything, all

color white, elem c
color bluewhite, protein
#show_as cartoon, protein
show surface, protein
#set transparency, 0.15

show sticks, ligands
set stick_color, magenta

load data/1vyw.pdb_points.pdb.gz, points
hide nonbonded, points
show nb_spheres, points
set sphere_scale, 0.2, points
cmd.spectrum("b", "green_red", selection="points", minimum=0, maximum=0.7)


stored.list=[]
cmd.iterate("(resn STP)","stored.list.append(resi)")    # read info about residues STP
lastSTP=stored.list[-1] # get the index of the last residue
hide lines, resn STP

cmd.select("rest", "resn STP and resi 0")

for my_index in range(1,int(lastSTP)+1): cmd.select("pocket"+str(my_index), "resn STP and resi "+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.show("spheres","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_scale","0.4","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_transparency","0.1","pocket"+str(my_index))



set_color pcol1 = [0.361,0.576,0.902]
select surf_pocket1, protein and id [116,119,1087,113,664,117,272,674,687,725,1082,718,751,1084,691,1110,1111,694,710,711,665,666,667,668,669,533,532,1182,289,1183,1094,1184,1190,285,152,288,171,151,125,170] 
set surface_color,  pcol1, surf_pocket1 
set_color pcol2 = [0.278,0.392,0.702]
select surf_pocket2, protein and id [5180,4607,4608,5216,4603,4604,4609,4610,4660,4661,4662,4616,4642,4643,5023,5155,5156,5157,5158,5159,5160,5161,5673,4763,4780,5674,5678,5679,5680,5580,5585,5681,5573,5575,5601,5602,5201,5213,5185,5193] 
set surface_color,  pcol2, surf_pocket2 
set_color pcol3 = [0.361,0.435,0.902]
select surf_pocket3, protein and id [3027,4743,5045,5046,5054,5055,5149,3028,4707,3437,5057,3461,3462,4521,4517,4546,3018,4557,4556,4543] 
set surface_color,  pcol3, surf_pocket3 
set_color pcol4 = [0.278,0.286,0.702]
select surf_pocket4, protein and id [1238,3585,3594,3593,3624,1262,1464,1466,2436,2437,2440,2441,2442,2470,2218,1239,1242,1244,1493,1472,1482,1485,1024,1487,1461,2497,1486,2208,2213,1492,1257,1263,1258,1261,3625] 
set surface_color,  pcol4, surf_pocket4 
set_color pcol5 = [0.424,0.361,0.902]
select surf_pocket5, protein and id [1202,1094,1188,1189,1190,1052,1330,1054,1071,289,428,1203,1204,1206,1323,1211,1212,1213,1053,1070,1095,1087,149,151,135,139,152,393,394,395,396,1326] 
set surface_color,  pcol5, surf_pocket5 
set_color pcol6 = [0.380,0.278,0.702]
select surf_pocket6, protein and id [7496,566,7497,216,563,658,66,7906,30,7930,7931,55,65,554,52,7487,7491,555,252,253,7938] 
set surface_color,  pcol6, surf_pocket6 
set_color pcol7 = [0.561,0.361,0.902]
select surf_pocket7, protein and id [5729,8062,8063,6939,5753,5748,5749,5752,5754,8093,8094,5730,5733,5735,5973,5978,5515,5963,5755,5976,5977,6700,5731,6966,6920,6924,5984,5983,6743,5985,6709,6699,6704,5989,6932,6933,6936] 
set surface_color,  pcol7, surf_pocket7 
set_color pcol8 = [0.490,0.278,0.702]
select surf_pocket8, protein and id [3583,3584,3580,3616,3614,3698,3699,3700,3702,3703,3729,3705,3706,3710,3677,3611,3704,4392,4403,4405,3550,3730,2881,2885,2886,2887,2888,3731,4401,4389,1260,1261,1467,1256,1255,1263,2889,2893,2879,3585,1262,2876] 
set surface_color,  pcol8, surf_pocket8 
set_color pcol9 = [0.698,0.361,0.902]
select surf_pocket9, protein and id [8085,8198,8169,8172,8872,8858,8859,8874,8861,8080,8083,8173,8054,5746,8019,5754,8053,7350,7354,7348,7355,7356,7357,8200,8051,7362] 
set surface_color,  pcol9, surf_pocket9 
set_color pcol10 = [0.596,0.278,0.702]
select surf_pocket10, protein and id [913,1127,1139,1152,1140,1142,675,678,1168,1169,683,696,7150,7151,2368,2369,680,542,540,541,504,505,507,508,518] 
set surface_color,  pcol10, surf_pocket10 
set_color pcol11 = [0.839,0.361,0.902]
select surf_pocket11, protein and id [5166,5168,5169,5170,5660,5171,5187,5188,5618,5630,5631,5174,5198,5615,5632,5633,4998,5659,5643,6860,5029,5031,4996,4999,3034,2662,2681,2682] 
set surface_color,  pcol11, surf_pocket11 
set_color pcol12 = [0.702,0.278,0.694]
select surf_pocket12, protein and id [4885,4886,4887,5694,5695,4640,5679,5680,5585,5681,5545,5562,5702,5704,5703,4780,4919,4779,4797,5142,4889,4892,4637,4638,4642,4643] 
set surface_color,  pcol12, surf_pocket12 
set_color pcol13 = [0.902,0.361,0.824]
select surf_pocket13, protein and id [7419,7070,7994,8345,8346,7416,7412,7413,7447,8309,7384,7385,7409,7410,8304,8306,8294,8323,7388,8254,7999,8002,8367,8011,8244,8246,7060,7077,7078] 
set surface_color,  pcol13, surf_pocket13 
set_color pcol14 = [0.702,0.278,0.584]
select surf_pocket14, protein and id [3785,3876,3877,3530,3536,3542,2601,2600,2608,2609,3835,3837,3840,3854,3775,3777,3779,3898,3825,2978,2941,3781,2737,3803,2950,2947,2943,2944,2590,2591,2594,2595,3525,2566] 
set surface_color,  pcol14, surf_pocket14 
set_color pcol15 = [0.902,0.361,0.682]
select surf_pocket15, protein and id [1896,1852,1894,1895,1486,2210,1410,1412,1460,1443,1446,1448,1457,1409,1903,1485,2219,1861,1866,1867,1868,1452] 
set surface_color,  pcol15, surf_pocket15 
set_color pcol16 = [0.702,0.278,0.478]
select surf_pocket16, protein and id [5558,5560,5836,5837,5870,5871,5838,5851,5852,5854,5856,5857,5858,5859,5847,5855,5839,5230,5231,5232,5233,5569,6083] 
set surface_color,  pcol16, surf_pocket16 
set_color pcol17 = [0.902,0.361,0.545]
select surf_pocket17, protein and id [6343,6385,6387,6381,6394,6352,6357,5976,5977,6701,5937,5938,5951,5903,5900,5901,5934,5939,5952,5943,5948,5955,6709,6710,6694,6702,6341] 
set surface_color,  pcol17, surf_pocket17 
set_color pcol18 = [0.702,0.278,0.369]
select surf_pocket18, protein and id [298,624,35,598,60,59,81,83,34,84,642,643,36] 
set surface_color,  pcol18, surf_pocket18 
set_color pcol19 = [0.902,0.361,0.408]
select surf_pocket19, protein and id [590,591,3388,3425,3427,3389,3391,356,3359,367,321,322,324,332,602,609,3361,3365] 
set surface_color,  pcol19, surf_pocket19 
set_color pcol20 = [0.702,0.298,0.278]
select surf_pocket20, protein and id [491,492,542,7132,7133,7134,7135,7136,493,540,541,7087,7131,7085,7481] 
set surface_color,  pcol20, surf_pocket20 
set_color pcol21 = [0.902,0.451,0.361]
select surf_pocket21, protein and id [3460,4514,4520,4517,4546,585,3428,3430,3425,3426,3427,3429,3398,3461,3462,4519,4521,600,602,3437,3396,3432,3397,4543,3401,4537] 
set surface_color,  pcol21, surf_pocket21 
set_color pcol22 = [0.702,0.408,0.278]
select surf_pocket22, protein and id [4526,5133,5132,4789,5115,5134,4551,4770,4528,4525,4550,4574,4575] 
set surface_color,  pcol22, surf_pocket22 
set_color pcol23 = [0.902,0.592,0.361]
select surf_pocket23, protein and id [5056,5058,5061,5073,1,7,7899,5076,7928,7927,7929,29,7897,5062,7960,7962,5077,7918,7926,7951,7953,7923,7924] 
set surface_color,  pcol23, surf_pocket23 
set_color pcol24 = [0.702,0.514,0.278]
select surf_pocket24, protein and id [4881,4887,5696,4870,4875,4876,4879,4856,4869,4880,4908,4909,5789,5790,7672,7694,7695,5699,5698,5700,5704,4872] 
set surface_color,  pcol24, surf_pocket24 
set_color pcol25 = [0.902,0.729,0.361]
select surf_pocket25, protein and id [1361,1362,1363,1366,1367,1368,1086,740,741,742,1380,1379,1077,1078,1592,1069,1346,1347,1352,1360] 
set surface_color,  pcol25, surf_pocket25 
set_color pcol26 = [0.702,0.624,0.278]
select surf_pocket26, protein and id [571,565,570,582,572,3491,3493,586,3455,3479,3480,3482,3458,3459] 
set surface_color,  pcol26, surf_pocket26 
set_color pcol27 = [0.902,0.871,0.361]
select surf_pocket27, protein and id [2147,2000,1790,1998,2192,2194,2170,2185,2176,1842,1843,2180,2182,2183,2184] 
set surface_color,  pcol27, surf_pocket27 
set_color pcol28 = [0.671,0.702,0.278]
select surf_pocket28, protein and id [7888,7894,7856,7857,7863,4861,5081,7896,4813,4826,4831,4859,4858,4814,4815,5093] 
set surface_color,  pcol28, surf_pocket28 


deselect

orient
