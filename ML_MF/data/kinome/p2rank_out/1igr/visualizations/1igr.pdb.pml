from pymol import cmd,stored

set depth_cue, 1
set fog_start, 0.4

set_color b_col, [36,36,85]
set_color t_col, [10,10,10]
set bg_rgb_bottom, b_col
set bg_rgb_top, t_col      
set bg_gradient

set  spec_power  =  200
set  spec_refl   =  0

load data/1igr.pdb, protein
create ligands, protein and organic
select xlig, protein and organic
delete xlig

hide everything, all

color white, elem c
color bluewhite, protein
#show_as cartoon, protein
show surface, protein
#set transparency, 0.15

show sticks, ligands
set stick_color, magenta

load data/1igr.pdb_points.pdb.gz, points
hide nonbonded, points
show nb_spheres, points
set sphere_scale, 0.2, points
cmd.spectrum("b", "green_red", selection="points", minimum=0, maximum=0.7)


stored.list=[]
cmd.iterate("(resn STP)","stored.list.append(resi)")    # read info about residues STP
lastSTP=stored.list[-1] # get the index of the last residue
hide lines, resn STP

cmd.select("rest", "resn STP and resi 0")

for my_index in range(1,int(lastSTP)+1): cmd.select("pocket"+str(my_index), "resn STP and resi "+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.show("spheres","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_scale","0.4","pocket"+str(my_index))
for my_index in range(1,int(lastSTP)+1): cmd.set("sphere_transparency","0.1","pocket"+str(my_index))



set_color pcol1 = [0.361,0.576,0.902]
select surf_pocket1, protein and id [1688,1694,1695,1696,1794,1705,1708,192,398,183,184,1919,1921,15,17,1769,1728,1768,1802,1770,1771,1772,1792,1793,1719,1714,620,1723,1807,1709,619,405,406] 
set surface_color,  pcol1, surf_pocket1 
set_color pcol2 = [0.278,0.298,0.702]
select surf_pocket2, protein and id [668,667,457,712,472,474,476,727,673,728,730,688,690,906,907,908] 
set surface_color,  pcol2, surf_pocket2 
set_color pcol3 = [0.533,0.361,0.902]
select surf_pocket3, protein and id [3066,3067,3267,3268,3681,3665,3049,3050,3051,2987,2994,3001,3020,3264,3266,3686,3496,3685,3656,3022,3249,3486,3488,3487,3476,2787,2998,2802,2990] 
set surface_color,  pcol3, surf_pocket3 
set_color pcol4 = [0.565,0.278,0.702]
select surf_pocket4, protein and id [1904,2088,2089,2157,1902,1894,1895,1999,2005,2007,2009,2010,1982,1984,2019,1903,25,26,27,28,29,32,33,34,204,205,206,207,16,208,1931,2154] 
set surface_color,  pcol4, surf_pocket4 
set_color pcol5 = [0.902,0.361,0.878]
select surf_pocket5, protein and id [537,538,540,817,1442,539,526,527,546,547,516,788,789,1434,1418,1419,799,801] 
set surface_color,  pcol5, surf_pocket5 
set_color pcol6 = [0.702,0.278,0.533]
select surf_pocket6, protein and id [1636,1517,1737,1534,1535,1513,1525,1526,1515,1516,856,841,842,1043,1478,1482,1483,1471,845,846,1464,1481,1044,1046,1047,1638,1641] 
set surface_color,  pcol6, surf_pocket6 
set_color pcol7 = [0.902,0.361,0.490]
select surf_pocket7, protein and id [1322,1323,1325,1329,1081,1082,1024,1034,1038,1026,1008,1018,1020,1102,1123,1124,1125,1105,1039,991,993,1324,995,1080,1083] 
set surface_color,  pcol7, surf_pocket7 
set_color pcol8 = [0.702,0.329,0.278]
select surf_pocket8, protein and id [1097,1076,678,680,1094,1095,891,1068,1071,877,879,880,867,868,873,883,884,872,885,886,675,656,677,887] 
set surface_color,  pcol8, surf_pocket8 
set_color pcol9 = [0.902,0.620,0.361]
select surf_pocket9, protein and id [54,258,473,239,240,242,243,666,668,458,457,472,474,476,471] 
set surface_color,  pcol9, surf_pocket9 
set_color pcol10 = [0.702,0.631,0.278]
select surf_pocket10, protein and id [1904,1902,1903,28,29,30,32,33,35,204,649,639,430] 
set surface_color,  pcol10, surf_pocket10 


deselect

orient
