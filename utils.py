"""
Python script containing some useful functions for my thesis.
"""

__author__    = "Lorenzo Fabbri"
__mantainer__ = "Lorenzo Fabbri"
__email__     = "lorenzo.fabbri@h-its.org"

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

def check_data(data, features, pair_plot=False, corr_plot=False, corr=False):
	"""
	Function to check if data is ready for further analysis or not.

	Parameters
	----------
	data : dataset
		Pandas dataset with rows being data points and columns 
		being features.
	features : list
		List of strings corresponding to columns with features NOT of interest.
	pair_plot : boolean
		Whether to plot `pairplot` or not. Default to false.
	corr_plot : boolean
		Whether to plot correlation matrix or not. Default to false.
	corr : boolean
		Whether to drop correlated features. Default to false, else 0.9.

	Returns
	-------
	None
	"""

	# Describe dataset
	print('Describing data...')
	print(f'{data.describe()}')

	# Check for missing values
	print(f'\nChecking for NaNs...')
	nan = np.sum(np.sum(data.isna()))
	if (nan>0):
		print(f'{np.sum(data.isna())}')

		# Drop columns with NaNs
		data = data.iloc[:, list(data.isna().any())]
	else:
		print(f'\t{nan}')

	# Drop features not desired for further analyses
	data = data.drop(features, axis=1)

	# Check which columns contain only zeros
	print(f'\nChecking for zeros...')
	print(f'\t{data.loc[:, ~(data!=0).any(axis=0)].columns}.')

	# Drop correlated features
	if corr:
		print(f'\nDropping correlated columns...')
		corr_matrix = data.corr().abs()
		upper = corr_matrix.where(np.triu(np.ones(corr_matrix.shape), k=1).astype(np.bool))
		to_drop = [coln for coln in upper.columns if any(upper[coln]>0.9)]
		print(f'\tColumns to drop: {to_drop}.')

		data = data.drop(to_drop, axis=1)

	# Plot distribution features
	if pair_plot:
		print(f'\nPlotting features...')
		sns.pairplot(data)

	# Plot correlation matrix
	if corr_plot:
		print(f'\nPlotting correlation matrix...')
		fig = plt.figure(figsize=(15, 15))
		ax = fig.add_subplot(111)
		cax = ax.matshow(data.corr())
		fig.colorbar(cax)
		ax.set_xticklabels(data.columns, rotation=90)
		ax.set_yticklabels(data.columns, rotation=20)
		plt.show()